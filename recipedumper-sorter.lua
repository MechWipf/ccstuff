
local function matchType ( str )
  return str:match( "^recipedumper:([a-z]+)!(.+)" )
end

local function matchItem ( str )
  return str:match( "(.+),(%d+)" )
end

local function matchRecipe ( str )
  local resultItem, resultQty
  local items = {}
  local tmp

  -- 1: all recipes 2: result
  tmp = { str:match("(.+)%->%((.+)%)") }
  resultItem, resultQty = matchItem( tmp[2] )

  local i = 0
  for item in tmp[1]:gmatch( "%((.-)%)" ) do
    i = i + 1
    if item ~= "None" then
      local key, _ = matchItem( item )
      items[i] = key
    end
  end

  return items, resultItem, resultQty
end

local function matchDimensions ( str )
  local w, h, s = str:match( "%(w=(%d+),h=(%d+)%)(.+)" )
  return tonumber(w), tonumber(h), s
end

-- counting some stuff
local shaped       = 0
local shapeless    = 0
local shapedore    = 0
local shapelessore = 0
local other        = 0

local function processFile ( writer, reader, form )
  local recipes = {}

  for line in reader:lines() do
    local _line = line
    local isShapeless
    local isOreDict
    local recipeType
    local w, h

    recipeType, line = matchType( line )
    isShapeless = ( recipeType == "shapeless" or recipeType == "shapelessore" )
    isOreDict   = ( recipeType == "shapedore" or recipeType == "shapelessore" )

    -- Just for counting stuff
    if recipeType == "shaped" then
      shaped = shaped + 1
    elseif recipeType == "shapeless" then
      shapeless = shapeless + 1
    elseif recipeType == "shapedore" then
      shapedore = shapedore + 1
    elseif recipeType == "shapelessore" then
      shapelessore = shapelessore + 1
    else
      other = other + 1

      -- early exit here... we can only do recipes for the first 4
      return
    end

    if not isShapeless then
      w, h, line = matchDimensions( line )
    end

    local pattern = { " ", " ", " ", " ", " ", " ", " ", " ", " " }
    local cType = "3x3"
    if w and w == 2 and h and h == 2 then
      pattern = { " ", " ", " ", " " }
      cType = "2x2"
    end

    local items = {}
    local grid, resultItem, resultQty = matchRecipe( line )

    local id = 0
    for i = 1, #pattern, 1 do
      local _id
      local item = grid[i]

      if item then
        if not items[item] then
          _id = id + 1; id = _id
          items[item] = _id
        else
          _id = items[item]
        end
        pattern[i] = _id
      end
    end

    local list = {}
    for k, i in pairs( items ) do
      list[i] = string.format( "%q", k )
    end

    pattern = table.concat( pattern, "" )

    if not recipes[resultItem] then
      recipes[resultItem] = {}
    end

    recipes[resultItem][#recipes[resultItem]+1] = {
      type = cType,
      qty = resultQty,
      pattern = pattern,
      items = list,
    }


  end

  if form == "lua" then
    writer:write( "return {\n" )

    local first = true
    for item, itemRecipes in pairs( recipes ) do
      if first then
        first = false
      else
        writer:write( "," )
      end

      writer:write( ("[%q]={\n"):format( item ) )
      for i, recipe in pairs( itemRecipes ) do
        writer:write( i ~= 1 and "\t,{" or "\t{" )
        writer:write( ("type=%q,"):format( recipe.type ) )
        writer:write( ("qty=%d,"):format( recipe.qty ) )
        writer:write( ("pattern=%q,"):format( recipe.pattern ) )
        writer:write( ("items={%s}"):format( table.concat(recipe.items, ",") ):sub( 1, -1 ) )
        writer:write( "}\n" )
      end
      writer:write( "}\n" )
      writer:flush()
    end

    writer:write( "}" )
  elseif form == "json" then
    writer:write( "{\n" )

    local first = true
    for item, itemRecipes in pairs( recipes ) do
      if first then
        first = false
      else
        writer:write( "," )
      end

      writer:write( ("%q:[\n"):format( item ) )
      for i, recipe in pairs( itemRecipes ) do
        writer:write( i ~= 1 and "\t,{" or "\t{" )
        writer:write( ("\"type\":%q,"):format( recipe.type ) )
        writer:write( ("\"qty\":%d,"):format( recipe.qty ) )
        writer:write( ("\"pattern\":%q,"):format( recipe.pattern ) )
        writer:write( ("\"items\":[%s]"):format( table.concat(recipe.items, ",") ):sub( 1, -1 ) )
        writer:write( "}\n" )
      end
      writer:write( "]\n" )
      writer:flush()
    end

    writer:write("}")
  end

end

local function main (...)
  local form = select(1, ...) or "lua"

  local reader = io.input [[C:\Users\mechwipf\Documents\Curse\Minecraft\Instances\FTB Infinity Evolved (1)\dumps\vsdump-2016-11-30T151546-recipes.log]]
  processFile( io.stdout, reader, form )

  -- print( "Shapeless recipes:    " .. shapeless )
  -- print( "Shaped recipes:       " .. shaped )
  -- print( "ShapelessOre recipes: " .. shapelessore )
  -- print( "ShapedOre recipes:    " .. shapedore )
  -- print( "Other recipes:        " .. other )
end


return main(...)