local utils, App, colors, fs, modularUI, term = 
      utils, App, colors, fs, modularUI, term

local app = App( "Explorer" )

function app:MainMenu ( session, parent, explorer )
    local app = self
    local pnMain = modularUI.widgets.panel( 0, 1, parent.w, 1 )
    pnMain:setColor( "background", colors.black )

    local btns = {
        -- new Folder
        function ( sDir )
            if fs.isReadOnly( sDir ) then return nil end

            return "new folder", function ()
                local tb = modularUI.widgets.textbox( 0, 0, parent.w - 1, 1 )
                tb.focus = true
                explorer:add( tb )
                explorer:invalidate()

                function tb.finish ( _self )
                    local path = fs.combine( "/" .. sDir, _self:getText() )

                    pcall( function ()
                        fs.makeDir( path )
                        app:drawDir( sDir )
                    end )

                    _self:kill()
                end

                function tb.update ( _self )
                    if not _self.focus then
                        _self:finish()
                    end
                end

                function tb.onEnter ( _self )
                    _self:finish()
                end
            end
        end,

        -- new File
        function ( sDir, _ )
            if fs.isReadOnly( sDir ) then return nil end

            return "new file", function ()
                local tb = modularUI.widgets.textbox( 0, 0, parent.w - 1, 1 )
                tb.focus = true
                explorer:add( tb )
                explorer:invalidate()

                function tb.finish ( _self )
                    local path = fs.combine( "/" .. sDir, _self:getText() )

                    pcall( function ()
                      local f = fs.open( path, "w" )
                      f.write( "" )
                      f.close()

                      app:drawDir( sDir )
                    end )
                    _self:kill()
                end

                function tb.update ( _self )
                    if not _self.focus then
                        _self:finish()
                    end
                end

                function tb.onEnter ( _self )
                    _self:finish()
                end
            end
        end,

        -- run file
        function ( _, sPath )
            if not fs.exists( sPath ) or fs.isDir( sPath ) then return nil end

            return "run", function ()
                session:runLegacy( sPath )
            end
        end,

        -- edit file
        function ( _, sPath )
            if not fs.exists( sPath ) or fs.isDir( sPath ) then return nil end

            local sTitle = fs.isReadOnly( sPath ) and "view" or "edit"

            return sTitle, function ()
                session:runLegacy( "/rom/programs/edit", sPath )
            end
        end,

        -- delete file
        function ( sDir, sPath )
          if not fs.exists( sPath ) or sDir == sPath then return nil end

          return "delete", function ()
            pcall( function ()
              fs.delete( sPath )
              app:drawDir( sDir )
            end )
          end
        end,
    }


    function pnMain.updateButtons ( _self, sPath )
        _self:clearChilds()

        local x = 1
        local y = 0
        for _, v in pairs( btns ) do
            local sDir = session.dir

            local sName, fFunc = v( sDir, sPath )

            if sName then
                if sName:len() + 1 + x > _self.w then
                    x = 1
                    y = y + 1
                end

                local btn = modularUI.widgets.button( x, y, sName:len(), 1, sName )
                btn.onClickLeft = fFunc
                btn:setColor( "button", colors.green )
                btn:setColor( "text", colors.black )
                btn:setParent( _self )

                x = x + sName:len() + 1
            end
        end

        _self:setSize( nil, y + 1 )
        _self:invalidate()
        --print( "Done" )
    end

    return pnMain
end

function app:loadUI ()
    local cw, ch = term.getSize()

    local session = self.session

    local pnBg = modularUI.widgets.panel( 1, 1, cw, ch )
    pnBg:setColors( { background = colors.orange } )

    local pnMain = modularUI.widgets.panel( 0, 1, cw, ch - 1 )
    pnMain:setColors( { background = colors.gray } )
    pnMain:setParent( pnBg )

    local text = ""
    local lbCurDir = modularUI.widgets.label( 0, pnMain.h - 1, text )
    lbCurDir:setColors( { background = colors.gray } )
    lbCurDir:setParent( pnMain )

    text = "Explorer"
    local lbTitle = modularUI.widgets.label( math.floor(cw / 2 - text:len() / 2), 0, text )
    lbTitle:setColors( { background = colors.orange } )
    lbTitle:setParent( pnBg )

    text = "X"
    local btnExit = modularUI.widgets.button( cw - 1, 0, text:len(), 1, text )
    btnExit:setColors( { button = colors.red } )
    btnExit:setParent( pnBg )

    function btnExit.onClickLeft ()
        session:switchApp( "Startmenu" )
    end


    local vlExplorer = modularUI.widgets.viewlist( 0, 1, pnMain.w, pnMain.h - 2, "item" )
    vlExplorer:setColor( "background", colors.black )
    vlExplorer:setParent( pnMain )


    local pnMenuMain = self:MainMenu( session, pnMain, vlExplorer )


    text = "Menu"
    local btnMenu = modularUI.widgets.button( 1, 0, text:len(), 1, text )
    btnMenu:setColor( "button", colors.gray )
    btnMenu:setColor( "text", colors.lightGray )
    btnMenu:setParent( pnMain )
    btnMenu.active = false

    btnMenu.e_y = vlExplorer.y
    btnMenu.e_h = vlExplorer.h

    function btnMenu.onClickLeft ( _self )
        _self.active = not _self.active
        _self:render()

        if _self.active == true then
            pnMenuMain:setParent( _self.parent )
        else
            pnMenuMain:delParent()
        end
    end

    function btnMenu.render ( _self )
        local m_h = pnMenuMain.h
        local e_y = _self.e_y
        local e_h = _self.e_h

        if _self.active == true then
            vlExplorer:setPos( nil, e_y + m_h )
            vlExplorer:setSize( nil, e_h - m_h )

            _self:setColor( "button", colors.lightGray )
            _self:setColor( "text", colors.white )
        elseif _self.active == false then
            vlExplorer:setPos( nil, 1 )
            vlExplorer:setSize( nil, e_h )

            _self:setColor( "button", colors.gray )
            _self:setColor( "text", colors.lightGray )
        end

        _self.parent:invalidate()
    end


    self.lbCurDir = lbCurDir
    self.vlExplorer = vlExplorer
    self.pnMenuMain = pnMenuMain
    self.btnMenu = btnMenu

    return pnBg
end

function app:drawDir ( dir )
    local app = self
    local session = self.session
    session.dir = dir

    if not fs.exists( dir ) then
        session:showMessageBox( utils.format("Directory %q does not exist.", dir) )
        app:drawDir( "/" )
        return
    end

    local list = fs.list( dir )
    if dir ~= "/" then
        table.insert( list, 1, ".." )
    end

    self.vlExplorer:clear()
    app.pnMenuMain:updateButtons( dir )
    app.btnMenu:render()

    local buttons = {}

    for i = 1, #list do
        local text = list[i]
        local lb = modularUI.widgets.button( 0, i - 1, text:len(), 1, text )
        buttons[#buttons+1] = lb
        lb.isDir = fs.isDir( "/" .. fs.combine( dir, text ) )
        lb:setColor( "button", colors.black )
        lb:setColor( "text", lb.isDir and colors.orange or colors.white )
        self.vlExplorer:add( lb )

        function lb.onClickLeft ( _self )
            local path = "/" .. fs.combine( dir, _self.text )

            app.pnMenuMain:updateButtons( path )
            app.btnMenu:render()

            if _self.selected then
                if _self.isDir then
                    app:drawDir( path )
                end
            else
                for iBtn = 1, #buttons, 1 do
                    local btn = buttons[iBtn]

                    if btn ~= _self then
                        btn.selected = false
                    else
                        btn.selected = true
                    end

                    btn:setColor( "button", btn.selected and colors.cyan or colors.black )
                    btn:invalidate()
                end
            end
        end

    end

    self.lbCurDir:setText( utils.format("Dir: %s", dir) )
    self.vlExplorer:setScroll( 1 )
    self.vlExplorer:invalidate()
end

function app:load ( session )
    self.session = session

    if not self.form then
        self.form = self:loadUI()
    end

    session.form = self.form

    if not session.dir then
        app:drawDir( "/" )
    end

    self.form:invalidate()
end

return app
