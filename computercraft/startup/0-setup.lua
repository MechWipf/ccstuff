local dbPath = "http://sw.kiwigaming.de/minecraft/cc/"

local _R = getfenv()
local    http,    loadstring,    fs =
      _R.http, _R.loadstring, _R.fs

local function loadfileOnline ( _sPath )
    local _s = http.get( _sPath ).readAll()

    if not _s or #_s == 0 then
        return nil, "No such File: " .. _sPath
    end

    return loadstring( _s )
end

local _G = getfenv(0)
_G.von = loadfileOnline( dbPath .. 'apis/vendor/von.lua' )()
loadfileOnline( dbPath .. 'apis/vendor/vfs.lua' )()
loadfileOnline( dbPath .. 'apis/vfs_httpx.lua' )()

if not fs.exists( "/db" )        then  vfs.mount("/db/"       , "httpx", { tree = dbPath .. "base.von"     , base = dbPath                 } ) end
if not fs.exists( "/apis" )      then  vfs.mount("/apis/"     , "httpx", { tree = dbPath .. "apis.von"     , base = dbPath .. "apis/"      } ) end
if not fs.exists( "/programs" )  then  vfs.mount("/programs/" , "httpx", { tree = dbPath .. "programs.von" , base = dbPath .. "programs/"  } ) end
if not fs.exists( "/installer" ) then  vfs.mount("/installer/", "httpx", { tree = dbPath .. "installer.von", base = dbPath .. "installer/" } ) end
