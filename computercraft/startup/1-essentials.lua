rawset( _G, "APPDATA_PATH", "/appdata/" )
rawset( _G, "API_PATH", "/apis/" )

os.loadAPI "/apis/legacy/utils"
os.loadAPI "/apis/legacy/easyFiles"

local require = _G.easyFiles.require
rawset( _G, "require", require)

local remoteDebug = require "remoteDebug"
local sha256 = require "vendor.sha256"

rawset( _G, "remoteDebug", remoteDebug )
rawset( _G, "sha256", sha256 )