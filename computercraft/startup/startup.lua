local _R = getfenv()
local shell = _R.shell
local fs = _R.fs

if fs.exists( "startup.d" ) then
    local ls = fs.list( "startup.d" )
    table.sort(ls)
    for _, file in pairs( ls ) do
        print( "Loading " .. file )
        local ok, err = pcall( shell.run, "/startup.d/" .. file )
        if not ok then
            print( "Error: ", err )
        end
    end
end