local function dl ( url )
  return function ( path )
    local h = http.get( url )
    local file = h:readAll() h:close()

    h = io.open( path, "w" )
    h:write( file ) h:close()
  end
end

dl "http://sw.kiwigaming.de/minecraft/cc/startup/0-setup.lua" "/startup/01-setup.lua"
dl "http://sw.kiwigaming.de/minecraft/cc/startup/1-essentials.lua" "/startup/02-essentials.lua"
dl "http://sw.kiwigaming.de/minecraft/cc/startup/2-aliases.lua" "/startup/03-aliases.lua"

os.reboot()