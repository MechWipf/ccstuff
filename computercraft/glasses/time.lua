local floor = math.floor

local Day = {
	"Sundas",
	"Morndas",
	"Tirdas",
	"Middas",
	"Turdas",
	"Fredas",
	"Loredas"
}

local Moon = {
	"Full Moon",
	"Waning Gibbous",
	"Last Quarter",
	"Waning Crescent",
	"New Moon",
	"Waxing Crescent",
	"First Quarter",
	"Waxing Gibbous"
}

local Max = 0
local function getTime()
	local t = os.time()
	if t > Max then Max = t end
	return {
		string.format("%.2d:%.2d",t,(t*100)%100/100*60),
		Day[floor(os.day()%6)+1]
	}
end

local function main()
	local GlassBridge = Devices["terminal_glasses_bridge"][1]
	if not GlassBridge then return end

	GlassBridge.clear()

	local Pos = {10,180}
	local obj = {}
	
	local Time, Day = unpack(getTime())
	local CanSpeak = true
	
	obj.Day = GlassBridge.addText(Pos[1],Pos[2],Day,0xFF0000)
	obj.Time = GlassBridge.addText(Pos[1],Pos[2]+8,Time,0xFF0000)	

	while true do
		sleep(0.1)
		local Time, Day = unpack(getTime())
		
		obj.Time.setText(Time)
		obj.Day.setText(Day)
	end
end

goroutine.spawnBackground("glasses_time",main)