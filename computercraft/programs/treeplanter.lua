if not util then error("Util API not loaded!") end

local extract = peripheral.wrap("bottom").extract
local select = turtle.select
local place = turtle.place
local getItemCount = turtle.getItemCount

function plantTrees()
	select(1)

	while true do
		if getItemCount(1) == 0 then
			extract(0, 6, 1, 64)
		end

		if getItemCount(2) == 0 then
			extract(0, util.getUUID(351,15), 1, 64)
		end

		if place() then
			select(2)
			while place() do sleep(0.1) end
			select(1)
		else
			sleep(0.2)
		end
	end
end

plantTrees()
