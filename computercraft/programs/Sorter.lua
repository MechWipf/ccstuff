if not util then
	if not os.loadAPI( "apis/util" ) then
		error( "Failed while loading 'apis/util'" )
	end
end

local getUUID = util.getUUID

local Sorter

for _,sSide in pairs( rs.getSides() ) do
	if peripheral.getType( sSide ) == "interactiveSorter" then
		Sorter = peripheral.wrap( sSide )
		break
	end
end

if not Sorter then
	error( "No sorter attached!" )
end


local supply = {}

supply[4] =
{
	15,					32,
	14,					32,
	getUUID(2001,1),	32,
	2001,				32,
	326,				8,
}

while true do

	for nSide = 0, 5, 1 do
		if supply[nSide] then
			local tList = Sorter.list( nSide )
			local cSupply = supply[nSide]

			for i = 1, #cSupply, 2 do
				if not tList or not tList[cSupply[i]] or tList[cSupply[i]] < cSupply[i+1] then
					for j = 0, 5, 1 do
						sleep(0)
						if j ~= nSide then
							if Sorter.extract( j, cSupply[i], nSide, cSupply[i+1] ) > 0 then
								print( string.format( "Sorting %d from %d to %d", cSupply[i], j, nSide ) )
							end
						end
					end
				end
			end
		end
	end
end
