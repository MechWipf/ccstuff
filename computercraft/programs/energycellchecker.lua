if not fs.exists("/appdata/ecc/config") then
	if not fs.exists( "/appdata/ecc" ) then fs.makeDir( "/appdata/ecc" ) end

	local file = fs.open("/appdata/ecc/config", "w")
	file:write [[return {
		rsbundleSide = "bottom",
		cells = {
			{
				peripheral	= "none",
				rsbundleIn	= "red",
				rsbundleOut	= "white",
				loadStart	=  5,		#percent
				loadEnd		= 95,		#percent
			},
		}
	}
]]
	file:flush()
	file:close()
end

local config = dofile "/appdata/ecc/config"

function main ()
	print( "Starting EnergycellChecker" )
	print( "Opening rsbundle .. \"" .. config.rsbundleSide .. "\"" )

	bundle.setSide( config.rsbundleSide )

	for k,v in pairs( config.cells ) do
		print( string.format( "Initialising cell %q", k ) )
		v.loading = true
	end

	while true do
		for name, cell in pairs( config.cells ) do
			local ren = peripheral.wrap( cell.peripheral )

			if ren then
				local f = ren.getEnergyStored() / ren.getMaxEnergyStored()

				if bundle.getIn( cell.rsbundleIn ) then
					cell.loading = false
				elseif f > cell.loadEnd then
					cell.loading = false
				elseif f < cell.loadStart then
					cell.loading = true
				end

				bundle.set( cell.rsbundleOut, cell.loading )

				print( string.format( "%s loading cell: %q", cell.loading and "started" or "stopped", name ) )

				sleep(0.5)
			end
		end

		sleep(5)
	end
end

while true do
	ok, err = pcall( main )

	if err then
		print( err )
		sleep( 20 )
		print( "restarting..." )
	end
end