-- ****************************** --
-- User: MechWipf
-- Date: 27.06.2015
-- Time: 23:00
-- ****************************** --

local    term,    colors,    os,    pairs,    tostring,    sleep,    setmetatable,    math,    parallel,    peripheral,    utils,    von,    error,    table,    tonumber,    select,    multishell,    package,    fs,    print,    textutils =
      _G.term, _G.colors, _G.os, _G.pairs, _G.tostring, _G.sleep, _G.setmetatable, _G.math, _G.parallel, _G.peripheral, _G.utils, _G.von, _G.error, _G.table, _G.tonumber, _G.select, _G.multishell, _G.package, _G.fs, _G.print, _G.textutils

local event_handler = require "vendor.event"()
local app = { items = {} }
local appID = "me_monitor_" .. math.random(100000, 999999)
local config = _G.config or require "config"
local Queue = _G.Queue or require "queue"
local args = {...}
local modularUI
if not package then package = require( "vendor.package" ) end

do
    modularUI = require "modularUI"()

    local path = _G.API_PATH .. "modularUI/widgets/"
    modularUI.loadWidget( path .. "base" )
    modularUI.loadWidget( path .. "label" )
    modularUI.loadWidget( path .. "panel" )
    modularUI.loadWidget( path .. "viewlist" )
    modularUI.loadWidget( path .. "tbutton" )
    modularUI.loadWidget( path .. "button" )
    modularUI.loadWidget( path .. "progressbar" )
    modularUI.loadWidget( path .. "textbox" )
end

local function loadForm ()

  if multishell then
    local c = multishell.getCurrent()
    multishell.setTitle( c, "ME Monitor" )
  end

    local c_w, c_h = term.getSize()

    local pnBg = modularUI.widgets.panel( 1, 1, c_w, c_h )
    pnBg:setColor( "background", colors.lime )

    local text = "ME Monitor"
    local lbTitle = modularUI.widgets.label( math.floor( c_w / 2 - text:len() / 2 ), 0, text )
    lbTitle:setColors( { background = colors.lime } )
    lbTitle:setParent( pnBg )

    text = "- am"
    local lbTime = modularUI.widgets.label( 0, 0, text )
    lbTime:setColors( { background = colors.lime, text = colors.cyan } )
    lbTime:setParent( pnBg )
    function lbTime:update ()
      local _text = textutils.formatTime( os.time(), false ) .. " "
      if self.text ~= _text then
        self.text = _text
        self:invalidate()
      end
    end

    text = "X"
    local btnExit = modularUI.widgets.button( pnBg.w - 1, 0, 1, 1, text )
    btnExit.onClickLeft = function () os.queueEvent( "_exit" .. appID ) end
    btnExit:setColor( "button", colors.red )
    btnExit:setParent( pnBg )

    local pnMain = modularUI.widgets.panel( 0, 1, pnBg.w, pnBg.h - 1 )
    pnMain:setColor( "background", colors.gray )
    pnMain:setParent( pnBg )

    text = "Changelog"
    local btnLog = modularUI.widgets.tbutton( 1, 0, text:len(), 1, text )
    btnLog.deactivateOnClick = false
    btnLog:setColor( "button_active", colors.black )
    btnLog:setColor( "button", colors.green )
    btnLog.onClickLeft = function () app:changeTab( "log" ) end
    btnLog:setParent( pnMain )

    text = "Crafting"
    local btnCraft = modularUI.widgets.tbutton( btnLog.x + btnLog.w + 1, 0, text:len(), 1, text )
    btnCraft.deactivateOnClick = false
    btnCraft:setColor( "button_active", colors.black )
    btnCraft:setColor( "button", colors.green )
    btnCraft.onClickLeft = function () app:changeTab( "craft" ) end
    btnCraft:setParent( pnMain )

    text = "Info"
    local btnInfo = modularUI.widgets.tbutton( btnCraft.x + btnCraft.w + 1, 0, text:len(), 1, text )
    btnInfo.deactivateOnClick = false
    btnInfo:setColor( "button_active", colors.black )
    btnInfo:setColor( "button", colors.green )
    btnInfo.onClickLeft = function () app:changeTab( "info" ) end
    btnInfo:setParent( pnMain )

    text = "Config"
    local btnConfig = modularUI.widgets.tbutton( btnInfo.x + btnInfo.w + 1, 0, text:len(), 1, text )
    btnConfig.deactivateOnClick = false
    btnConfig:setColor( "button_active", colors.black )
    btnConfig:setColor( "button", colors.green )
    btnConfig.onClickLeft = function () app:changeTab( "config" ) end
    btnConfig:setParent( pnMain )

    -- Tab: Changelog --
    local vlItemsLog = modularUI.widgets.viewlist( 0, 1, pnMain.w, c_h - 3 )
    vlItemsLog.itemHeight = 1
    vlItemsLog:setColor( "background", colors.black )
    app.vlItemsLog = vlItemsLog

    app.ItemLog = {}

    vlItemsLog:setContainer( setmetatable( {
        len = function ()
            return #app.ItemLog
        end
    }, {
        __index = function ( _, k )
            if app.ItemLog[k] then
                local item = app.ItemLog[k]
                return item
            end

            return nil
        end,
    } ) )
    -- -------------- --

    -- Tab: Info --
    local pnInfo = modularUI.widgets.panel( 0, 1, pnMain.w, c_h - 3 )
    pnInfo:setColor( "background", colors.black )

    text = "Stored Energy"
    local lbStoredEnergy = modularUI.widgets.label( 1, 1, text )
    lbStoredEnergy:setColors( { background = colors.black } )
    lbStoredEnergy:setParent( pnInfo )

    text = "%d %sRF/t of %d %sRF/t"
    local pbStoredEnergy = modularUI.widgets.progressbar( 2, lbStoredEnergy.y + 1, pnInfo.w - 4, text )
    pbStoredEnergy:setParent( pnInfo )

    function pbStoredEnergy:update ()
        local _text = self.text
        local n1, n2, p1, p2

        n1 = ( app.inventory and app.inventory.getStoredPower() or -1 ) * 2 -- 2 ae = 1 rf
        n2 = ( app.inventory and app.inventory.getMaxStoredPower() or -1 ) * 2

        self:setPercent( math.min( n1 / n2, 1 ) )

        n1, p1 = utils.reduceNum( n1 )
        n2, p2 = utils.reduceNum( n2 )
        self:setText( _text:format( n1, p1, n2, p2 ) )
    end

    text = "Energy Usage"
    local lbEnergyUsage = modularUI.widgets.label( 1, lbStoredEnergy.y + 4, text )
    lbEnergyUsage:setColors{ background = colors.black }
    lbEnergyUsage:setParent( pnInfo )

    local energy = {
        { "Idle  :", function () return app.inventory and app.inventory.getIdlePowerUsage() or -1 end },
        { "Usage :", function () return app.inventory and app.inventory.getAvgPowerUsage()  or -1 end },
        { "In    :", function () return app.inventory and app.inventory.getAvgPowerInjection() or -1 end }
    }

    for i, item in pairs( energy ) do
        local _text = item[1]
        local func = item[2]

        local lbText = modularUI.widgets.label( 2, lbEnergyUsage.y + i * 2, _text )
        lbText:setColors{ text = colors.orange, background = colors.black }
        lbText:setParent( pnInfo )

        energy[i] = lbText

        _text = "%d %sRF/t"
        local lbNum = modularUI.widgets.label( lbText.w + 3, lbEnergyUsage.y + i * 2, _text )
        lbNum:setColors{ text = colors.red, background = colors.black }
        lbNum:setParent( pnInfo )

        function lbText.update ()
            lbNum:setText( _text:format( utils.reduceNum( func() * 2 ) ) )
        end
    end
    -- --------- --


    -- Tab: Crafting --
    local vlItemsCrafting = modularUI.widgets.viewlist( 0, 1, pnMain.w, c_h - 3 )
    vlItemsCrafting:setColor( "background", colors.black )
    vlItemsCrafting.mode = "semi_item"
    app.vlItemsCrafting = vlItemsCrafting

    app.ItemCrafting = {}

    vlItemsCrafting:setContainer( setmetatable( {
        len = function ()
            return #app.ItemCrafting
        end
    }, {
        __index = function ( _, k )
            if app.ItemCrafting[k] then
                local item = app.ItemCrafting[k]
                return item
            end

            return nil
        end,
    } ) )

    vlItemsCrafting.wrapper = function ()
        local pnCraftPanel = modularUI.widgets.panel( 1, 0, vlItemsCrafting.w - 1, 1 )
        pnCraftPanel:setColors{ background = colors.black }

        local lbCraftLabel = modularUI.widgets.label( 0, 0, "" )
        lbCraftLabel:setColors{ background = colors.black }
        lbCraftLabel:setParent( pnCraftPanel )

        if app.inventory and app.inventory.requestCrafting then
            local _text = "C"
            local btnCraftOpen = modularUI.widgets.button( 0, 0, _text:len(), 1, _text )
            btnCraftOpen:setParent( pnCraftPanel )

            _text = "S"
            local btnStockOpen = modularUI.widgets.button( 1, 0, _text:len(), 1, _text )
            --btnStockOpen:setParent( pnCraftPanel )
            btnStockOpen:setColors{ text = colors.orange, button = colors.brown }

            function btnCraftOpen.onClickLeft ()
                app:showCraftForm ( pnCraftPanel.fingerprint )
            end

            function btnStockOpen.onClickLeft ()
                app:showCraftForm ( pnCraftPanel.fingerprint, true )
            end

            lbCraftLabel:setPos( 2 )
        end

        function pnCraftPanel:apply ( data )
            self.fingerprint = data[2]
            lbCraftLabel:setText( data[1] )
        end

        return pnCraftPanel
    end
    -- ------------- --

    -- Config --
    local pnConfig = modularUI.widgets.panel( 0, 1, pnMain.w, c_h - 3 )
    pnConfig:setColor( "background", colors.black )

    --text = "Monitor"
    --local lbMonitor = modularUI.widgets.label(  )
    -- ------ --


    text = "Status: ready"
    local lbStatus = modularUI.widgets.label( 0, pnMain.h-1, text )
    lbStatus:setParent( pnMain )

    function app.setStatus ( _, _text )
        lbStatus:setText( "Status: " .. _text )
    end

    local tabs = {
        ["parent"] = pnMain,
        ["tabs"] = {
            {
                ["name"] = "log",
                ["button"] = btnLog,
                ["widgets"] = { vlItemsLog }
            },
            {
                ["name"] = "craft",
                ["button"] = btnCraft,
                ["widgets"] = { vlItemsCrafting }
            },
            {
                ["name"] = "info",
                ["button"] = btnInfo,
                ["widgets"] = { pnInfo }
            },
            {
                ["name"] = "config",
                ["button"] = btnConfig,
                ["widgets"] = { pnConfig }
            }
        }
    }

    function app.changeTab ( _, tab )
        for _, t in pairs( tabs.tabs ) do
            local active = ( t.name == tab )

            t.button.isActive = active

            for _, w in pairs( t.widgets ) do
                if active then
                    w:setParent( tabs.parent )
                elseif w.parent then
                    w:delParent()
                end
            end
        end

        app.activeTab = tab
        tabs.parent:invalidate()
    end

    function app.showCraftForm ( _, fingerprint, stock )
        local details = app.inventory.getItemDetail( fingerprint, true )
        local basic = details.basic()

        local w = 30
        local h = 9

        local pnCraftForm = modularUI.widgets.panel( math.floor( c_w / 2 - w / 2 ), math.floor( c_h / 2 - h / 2 ), w, h )
        pnCraftForm:setColors{ background = colors.lime }

        local pnCraftMain = modularUI.widgets.panel( 1, 1, pnCraftForm.w - 2, pnCraftForm.h - 2 )
        pnCraftMain:setColors{ background = colors.gray }
        pnCraftMain:setParent( pnCraftForm )

        local _text = stock and "Set Resupply Limit" or "Crafting"
        local lbCFTitle = modularUI.widgets.label( math.floor( pnCraftForm.w / 2 - _text:len() / 2 ), 0, _text )
        lbCFTitle:setColors{ background = colors.lime }
        lbCFTitle:setParent( pnCraftForm )

        _text = "X"
        local btnCFClose = modularUI.widgets.button( pnCraftForm.w - 1, 0, _text:len(), 1, _text )
        btnCFClose:setColors{ button = colors.red }
        btnCFClose:setParent( pnCraftForm )

        _text = basic.display_name:sub(1,28)
        local lbItemName = modularUI.widgets.label( 0, 0 , _text )
        lbItemName:setColors{ background = colors.brown }
        lbItemName:setParent( pnCraftMain )

        _text = "Current "
        local lbItemCount = modularUI.widgets.label( 0, 2, _text )
        lbItemCount:setParent( pnCraftMain )

        _text = tostring( basic.qty )
        local lbItemCountValue = modularUI.widgets.label( lbItemCount.x + lbItemCount.w, lbItemCount.y, _text )
        lbItemCountValue:setColors{ _text = colors.orange }
        lbItemCountValue:setParent( pnCraftMain )

        _text = stock and "Supply  " or "Craft   "
        local lbCraftCount = modularUI.widgets.label( 0, 4, _text )
        lbCraftCount:setParent( pnCraftMain )

        local tbCraftCountInput = modularUI.widgets.textbox( lbCraftCount.x + lbCraftCount.w + 3, lbCraftCount.y, pnCraftMain.w - (lbCraftCount.x + lbCraftCount.w + 1) - 6 )
        tbCraftCountInput:setText( "0" )
        tbCraftCountInput.cursorX = 1
        tbCraftCountInput:setParent( pnCraftMain )

        for i = 1, 6 do
          local _btn
          if i < 4 then
            _btn = modularUI.widgets.button( tbCraftCountInput.x - i, tbCraftCountInput.y, 1, 1, "-" )
            _btn:setColors{ button = select( i, colors.yellow, colors.orange, colors.red ) }
            _btn.changeCount = select( i, -1, -64, -512 )
          else
            _btn = modularUI.widgets.button( tbCraftCountInput.x + tbCraftCountInput.w + ( i - 4 ), tbCraftCountInput.y, 1, 1, "+" )
            _btn:setColors{ button = select( i - 3, colors.yellow, colors.orange, colors.red ) }
            _btn.changeCount = select( i - 3, 1, 64, 512 )
          end

          _btn.onClickLeft = function ( self )
            tbCraftCountInput:setText( tostring( math.clamp( tonumber( tbCraftCountInput:getText() ) + self.changeCount, 0, 1e999 ) ) )
          end
          _btn:setParent( pnCraftMain )
        end

        _text = "Start"
        local btnCraftStart = modularUI.widgets.button( math.floor( pnCraftMain.w / 2 - _text:len() / 2 ), pnCraftMain.h - 1, _text:len(), 1, _text )
        btnCraftStart:setColors{ button = colors.green }
        btnCraftStart:setParent( pnCraftMain )

        btnCraftStart.onClickLeft = function ()
            if stock then
                error( "Not Implemented" )
            else
                app.inventory.requestCrafting( fingerprint, tonumber( tbCraftCountInput:getText() ) )
            end

            btnCFClose:_onClick( 1 )
        end

        btnCFClose.onClickLeft = function ()
            pnCraftForm:kill()
            app.form:enable( true )
        end

        app.form:enable( false )
        modularUI.startWatching( pnCraftForm, term )
        tbCraftCountInput.focus = true
    end

    app.form = pnBg

    local startTab = args[1] or ""

    for _, tab in pairs( tabs.tabs ) do
      if tab.name == startTab then
        app:changeTab( tab.name )
      end
    end

    modularUI.startWatching( app.form, term )
end

function app.saveTimestamp ( item, size )
    if not item.logLastQty then item.logLastQty = 0 end
    if not item.logTime    then item.logTime    = 0 end
    if not item.rate       then item.rate       = 0 end

    local delta = ( utils.unixTime() - item.logTime ) / 20
    if delta < 800 then
        local qtyDelta = ( size - item.logLastQty ) / delta
        item.rate = (item.rate + qtyDelta) / 2
        -- if item.fingerprint.id == "minecraft:cobblestone"
        -- and item.fingerprint.dmg == 0 then
        --     debug.log( delta, item.rate, size, item.logLastQty )
        -- end
    end
    item.logTime = utils.unixTime()

    item.logLastQty = size
end

local function program ()
    local appdata = "/appdata/me_monitor/config.von"
    app.changesPath = "/appdata/me_monitor/changes.zip"
    app.config = config.load( appdata, { ["inventory"] = "unknown" } )

    if not app.config then error "How?" end

    local pType = peripheral.getType( app.config.inventory )
    if pType and pType:match( "(tileinterface|aemultipart)" ) then
      app.inventory = peripheral.wrap( app.config.inventory )
    else
      local devices = utils.getDevices()

      if devices.tileinterface and #devices.tileinterface > 0 then
        app.inventory = devices.tileinterface[1]
      elseif devices.aemultipart and #devices.aemultipart > 0 then
        app.inventory = devices.aemultipart[1]
      end
    end

    if not app.inventory then
        app:setStatus( "incorrect config" )

        while not app.inventory do
            sleep(1)
        end
    end

    while true do
        sleep(10)
    end
end

local function checkItems ()
    while not app.inventory do sleep(1) end

    local function compare ( a, b )
        if not a then return false end
        if not b then return true end

        return a.value > b.value
    end

    if not app.saved_items then
      if fs.exists( app.changesPath ) then
        app:setStatus( "load changlog history" )
        local binary = utils.binaryRead( app.changesPath )
        local str = package.decompress( binary, true )
        app.saved_items = von.deserialize( str )
      else
        app.saved_items = {}
      end
    end
    local saved_items = app.saved_items

    while true do
        app:setStatus( "calculating changelog..." )
        -- debug.log( "Info: Calculating changelog" )
        local s = os.clock()

        local items = app.inventory.getAvailableItems()
        local craftable = {}

        local sorted = Queue.new()
        sorted.compare = compare

        local item_count = #items

        app:setStatus( ("calculating changelog... (%d/%d)"):format( 0, item_count ) )

        local looked = {}
        -- debug.log "-----------"
        for _, item in pairs( items ) do
            local key = ("%s:%d:%s"):format( item.fingerprint.id, item.fingerprint.dmg, item.fingerprint.nbt_hash or "" )
            local itemData

            if not saved_items[key] then
                local details = app.inventory.getItemDetail( item.fingerprint, true )
                local name = item.fingerprint.id

                if details then
                    utils.pcallLog(
                        "Warning: Error while accessing item details for " .. key .. ": %s",
                        function()
                            local _name = details.all().display_name
                            while _name:byte(1) and _name:byte(1) > 127 do
                                _name = _name:sub( 2 )
                            end
                            name = _name
                        end
                    )
                end

                itemData = {
                    fingerprint = item.fingerprint,
                    name = name
                }
            else
                itemData = saved_items[key]
            end

            app.saveTimestamp( itemData, item.size )

            local c = math.abs( itemData.rate )
            if c > 0 then
                sorted:insert( { key, value = c } )
            end


            if item.is_craftable then
                craftable[#craftable+1] = { tostring( itemData.name ), itemData.fingerprint }
            end

            saved_items[key] = itemData

            looked[key] = true

            if os.clock() - s > 6 then
                app:setStatus( ("calculating changelog... (%d/%d)"):format( _, item_count ) )
                sleep()
                s = os.clock()
            end
        end

        app:setStatus( "calculating changelog... sorting" )
        -- debug.log( "Info: Sorting" )

        for key, item in pairs( saved_items ) do
            if not looked[key] and (item.qty and item.qty > 0) then
                app.saveTimestamp( item, 0 )

                local c = math.abs( item.qty )
                if c > 0 then
                    sorted:insert( { key, value = c } )
                end
            end
        end

        if not app.pendingFile then
          local data = von.serialize( saved_items )
          if not data then
            error( "Something went wrong...?!" )
          end
          app.pendingFile = data
        end

        app:setStatus( "calculating changelog... get top items" )
        -- debug.log( "Info: Top items" )

        local t = {}
        s = os.clock()

        for _ = 1, 50, 1 do
            local item = sorted:deleteTop()
            if not item then break end
            local _item = app.saved_items[item[1]]

            local x, suffix = utils.reduceNumX( _item.rate * 3600 or 0, { { "h" }, { "m", 60 }, { "s", 60 }, { "t", 20 } } )

            t[#t+1] = {
                { "background", colors.black },
                { "text", x >= 0 and colors.green or colors.red },
                ("%.2f/%s "):format( math.floor( x*100 )/100, suffix ),
                { "text", colors.white },
                _item.name,
            }

            if os.clock() - s > 6 then
                sleep()
                s = os.clock()
            end
        end
        app.ItemLog = t
        app.vlItemsLog:invalidate()

        app.ItemCrafting = craftable
        app.vlItemsCrafting:invalidate()

        -- debug.log( "Info: Ready" )
        app:setStatus( "ready" )
        sleep(10)
    end
end

local function storeFile ()
  while true do
    if app.pendingFile then
      local binary = package.compress( app.pendingFile )
      utils.binaryWrite( app.changesPath, binary )
      app.pendingFile = false
    end
    sleep(1)
  end
end

(function ()
    local ok = utils.pcallLog(
      "There was an error loading the form: %s",
      loadForm
    )
    if not ok then return end

    parallel.waitForAny(
        function ()
          utils.pcallLog(
            "EventHandler MainLoop exits with error: %s",
            event_handler.mainLoop,
            event_handler
          )
        end,
        function ()
          while modularUI.inLoop do sleep(10) end
          utils.pcallLog(
            "ModularUI MainLoop exits with error: %s",
            modularUI.mainLoop,
            event_handler
          )
        end,
        function ()
          utils.pcallLog(
            "ProgrammLoop exits with error: %s",
            program
          )
        end,
        function ()
          utils.pcallLog(
            "CheckItemsLoop exits with error: %s",
            checkItems
          )
        end,
        function ()
          utils.pcallLog(
            "StoreFilesLoop exits with error: %s",
            storeFile
          )
        end,
        function ()
            os.pullEvent( "_exit" .. appID )
            app.cleanShutdown = true
        end
    )

    if not app.cleanShutdown then
      term.setBackgroundColor( colors.blue )
      term.setTextColor( colors.white )
      term.clear()
      term.setCursorPos( 1, 1 )

      print(
        "Programm crashed.\n",
        utils.getLastError(),
        "\n\nPress any key to exit."
      )
      os.pullEvent( "key" )
    end

    term.setBackgroundColor( colors.black )
    term.setTextColor( colors.white )
    term.clear()
    term.setCursorPos( 1, 1 )
end)()
