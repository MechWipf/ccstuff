--[[
Config
    out       : Extractor output side
    rsbSide   : Redbundle Side
    rsbColor  : Redbundle Color for energy control
    extractor : Side or Name of the to controling extractor
]]

local config = dofile "/appdata/extractor/config.lua"
local extractor = peripheral.wrap( config.extractor )
rsbundle.setSide( config.rsbSide )

function main ()
    while true do
        local inventory = extractor.getAllStacks()

        if next(inventory) then
            rsbundle.set( config.rsbColor, true )
            
            if inventory[8] then
                extractor.pushItem( config.out, 8, 64 )
            end

            if inventory[9] then
                extractor.pushItem( config.out, 9, 64 )
            end

        else
            rsbundle.set( config.rsbColor, false )
        end

        sleep(1)
    end
end

while true do
    pcall( main )
    sleep(2)
end