local request = require "internet".request

local function dl ( url )
  return function ( path )
    local sb = {}
    for chunk in request( url ) do
      sb[#sb+1] = chunk
    end
    local file = table.concat( sb )

    local h = io.open( path, "w" )
    h:write( file ) h:close()
  end
end

dl "http://mechwipf.dnshome.de/apis/oc/vfs.lua"     "/home/lib/vfs.lua"
dl "http://mechwipf.dnshome.de/apis/vendor/von.lua" "/home/lib/von.lua"