-- Global imports
local _R = getfenv()

-- Required libraries
local config = require "config"
local console = require "console"

-- Other Stuff
local configPath = "/appdata/filter/config.von"
local app = {}


function app.loadConfig()
  local _default = {
    ["inventory"] = {
      ["unknown"] = {
        ["direction"] = {
          { "itemID:string", "itemMeta?:string", "filter?(itemID, itemMeta): boolean" }
        }
      }
    }
  }

  local _loadedConfig = config.load( configPath, _default )
  local _config = {}

  for inventoryName, inventory in pairs( _loadedConfig.inventory ) do
    for direction, items in pairs( inventory ) do
      for _, item in pairs( items ) do
        if not _config[inventoryName] then
          _config[inventoryName] = {}
        end

        _config[inventoryName][item[1]] = { direction, item = { id = item[1], meta = item[2], filter = item[3] } }
      end
    end
  end

  app.config = _config
end

function app.main ( args )
  debug.log "----"

  local con = console.NewConsole()
  local interactive = false

  con:registerCommand( { "i", "interactive" }, function ( self )
    interactive = true
    self:exit()
  end )

  con:registerCommand( { "r", "run" }, function ( self, args ) end)

  con:run( { table.concat(args, " ") } )
  if interactive then con:run() end
end

local ok, ret = pcall( app.main, {...} )

if not ok then
  print( ret )
  io.read()
end
