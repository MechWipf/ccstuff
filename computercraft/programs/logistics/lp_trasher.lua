--[[
    Logistics Pipes API
        .getCraftableItems
        .setTurtleConnect
        .getUnlocalizedName
        .getRouterId
        .getItemIdentifierIDFor
        .canAccess
        .getItemAmount
        .getAvailableItems
        .getItemDamage
        .makeRequest
        .getItemID
        .getType
        .getNBTTagCompound
]]
local floor,      dofile, term, colors, tostring, ipairs, sleep, utils =
      math.floor, dofile, term, colors, tostring, ipairs, sleep, utils

dofile "db/apis/util.lua"
local modularUI, Events, eventHandler, app
local configDefault = [[return {
    ["filter"] = {
        ["298"] = true, --Leather
        ["299"] = true,
        ["300"] = true,
        ["301"] = true,


        ["314"] = false, --Gold
        ["315"] = false,
        ["316"] = false,
        ["317"] = false,

        ["283"] = false,
        ["284"] = false,
        ["285"] = false,
        ["286"] = false,
        ["294"] = false,


        ["306"] = true, --Iron
        ["307"] = true,
        ["308"] = true,
        ["309"] = true,

        ["256"] = true,
        ["257"] = true,
        ["258"] = true,
        ["267"] = true,
        ["292"] = true,


        ["302"] = true, --Chain (Steel)
        ["303"] = true,
        ["304"] = true,
        ["305"] = true,


        ["272"] = true, --Stone
        ["273"] = true,
        ["274"] = true,
        ["275"] = true,
        ["291"] = true,


        ["268"] = true, --Wood
        ["269"] = true,
        ["270"] = true,
        ["271"] = true,
        ["290"] = true,

        ["261"] = true,

        ["259"] = true,
        ["346"] = true,

    }
}
]]

if not getDevices then error "util.lua not installed" end
if not getDevices( 'LogisticsPipes:Request' ) then error( 'No "LogisticsPipes:Request" connected' ) end

if not fs.exists( "/appdata/logistic/config" ) then

    if not fs.exists( "/appdata/logistic/" ) then fs.makeDir( "/appdata/logistic/" ) end

    local file = fs.open( "/appdata/logistic/config", "w" )
    file:write( configDefault )
    file:flush()
    file:close()

    print "Write Config"
    print( configDefault )
end

local config = dofile "/appdata/logistic/config"
local requestPipe = getDevices( "LogisticsPipes:Request" )[1]


local function loadUI ()
    modularUI = dofile "db/apis/modularUI/init.lua"

    local path = "db/apis/modularUI/widgets/"
    modularUI.loadWidget( path .. "base" )
    modularUI.loadWidget( path .. "label" )
    modularUI.loadWidget( path .. "panel" )
    modularUI.loadWidget( path .. "viewlist" )
end

local function loadMain ()
    Events = dofile "db/apis/event.lua"

    eventHandler = Events()
    app = {}

    local items = {}
    local items_count = {}
    local itemCounter = 0

    local c_w, c_h = term.getSize()

    local pnBg = modularUI.widgets.panel( 1, 1, c_w, c_w )
    pnBg:setColor( "background", colors.orange )

    local text = "LogisticPipes Trash Manager"
    local lbTitle = modularUI.widgets.label( floor( c_w / 2 - text:len() / 2 ), 0, text )
    lbTitle:setColors( { background = colors.orange } )
    lbTitle:setParent( pnBg )

    local pnMain = modularUI.widgets.panel( 0, 1, pnBg.w, pnBg.h - 1 )
    pnMain:setColor( "background", colors.gray )
    pnMain:setParent( pnBg )

    local text = "Trashed items: 0"
    local lbDumped = modularUI.widgets.label( 0, 1, text )
    lbDumped:setColor( "background", colors.gray )
    lbDumped:setParent( pnMain )

    local vlTrash = modularUI.widgets.viewlist( 0, 2, pnMain.w, c_h - 4 )
    vlTrash:setColor( "background", colors.black )
    vlTrash:setParent( pnMain )

    function app.addItem ( sName )
        itemCounter = itemCounter + 1

        lbDumped:setText( utils.format( "Trashed items: %s", itemCounter ) )

        if not items[sName] then
            items[sName] = modularUI.widgets.label( 0, 0, sName )
            items[sName]:setColors( { background = colors.black, text = colors.brown } )
            items_count[sName] = 0
            vlTrash:add( items[sName] )
        end

        items_count[sName] = items_count[sName] + 1

        local count = tostring( items_count[sName] )
        local count = "0" * ( 3 - count:len() ) .. count

        items[sName]:setText( utils.format( " %s x %s", count, sName ) )

        vlTrash:invalidate()
    end

    modularUI.startWatching( pnBg )
end

local function main ()
    while true do
        local items = requestPipe.getAvailableItems()

        for _, item in ipairs( items ) do
            local itemName = requestPipe.getUnlocalizedName( item[1] )
            local itemId   = requestPipe.getItemID( item[1] )

            if config.filter[ tostring( itemId ) ] then
                local ok, _ = requestPipe.makeRequest( item[1], 1 )

                if ok == "DONE" then
                    app.addItem( itemName )
                end

                sleep( .25 )
            end

            sleep( .1 )
        end
    end
end

local ok, err = pcall( loadUI )

if not ok then
    error( utils.format( "Failed to load UI -> %s", err ) )
end

local ok, err = pcall( loadMain )

if not ok then
    error( utils.format( "Failed to load Main -> %s", err ) )
end

parallel.waitForAny(
    function ()
        eventHandler:mainLoop()
    end,
    function ()
        modularUI.mainLoop( eventHandler )
    end,
    function ()
        main()
    end
)
