-- Setup --
local function fromHex ( str )
    local s = str:gsub( "..", function (c)
        return string.char( tonumber( c, 16 ) )
    end )
    return s
end
local function loadHex ( str )
    local str = fromHex( str )
    return loadstring( str )
end

local shell = getfenv(2).shell
local von = loadHex "%s" ()
local data = ([[%s]]):gsub( "`%%[`", "[[" ):gsub( "`%%]`", "]]" )
local runner = "local fn = loadstring(({(%s):gsub('..',function (c) return string.char(tonumber(c,16)) end)})[1]) setfenv(fn, getfenv()) fn(...)"
local meta = {}

print "Installing: "
for path, item in pairs( von.deserialize( data ) ) do
    local ty, file = item[1], item[2]

    if ty == "help" then
        meta.help = loadHex( file )
    else
        local directory = path:match("(.+)/.-$")
        if not fs.exists( directory ) then fs.makeDir( directory ) end

        print( path )

        if ty == "file" then
            local f = io.open( path, "w" )
            f:write( file ) f:close()
        elseif ty == "func" then
            local f = io.open( path, "w" )
            f:write( runner:format( file ) ) f:close()
        end
    end
end

print( "Setup complete.\n" )
if meta.help then
    meta.help( ... )
end