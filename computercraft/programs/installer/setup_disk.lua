-- Setup --
local shell = getfenv(2).shell
os.loadAPI( shell.resolve "./apis/package" )
os.loadAPI( shell.resolve "./apis/utils"  )
local von = dofile( shell.resolve "./apis/von.lua" )
local data = utils.binaryRead( shell.resolve "./data.pack"  )
data = package.decompress( data, true )
local meta = {}

local runner = "local fn = loadstring(({(%q):gsub('..',function (c) return string.char(tonumber(c,16)) end)})[1]) setfenv(fn,getfenv()) fn(...)"

print "Installing: "
local paths = von.deserialize( data )
for path, item in pairs( paths ) do
    local ty, file = item[1], item[2]

    if ty == "help" then
        meta.help = loadHex( file )
    else
        local directory = path:match("(.+)/.-$")
        if not fs.exists( directory ) then fs.makeDir( directory ) end

        print( path )

        if ty == "file" then
            local f = io.open( path, "w" )
            f:write( file ) f:close()
        elseif ty == "func" then
            local f = io.open( path, "w" )
            f:write( runner:format( file ) ) f:close()
        end
    end
end

print( "Setup complete.\n" )
meta.help()
