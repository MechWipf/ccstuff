-- rDebug v0.0.10

local cmd = select( 1, ... )

if cmd == "receiver" then
  if not remoteDebug then error( "No remoteDebug session found" ) end

  local _, err = remoteDebug:setReceiver( select( 2, ... ) )
  if err then print(err) return end

  print( "Set receiver to " .. select( 2, ... ) )
elseif cmd == "enable" then
  if not remoteDebug then error( "No remoteDebug session found" ) end

  local _, err = remoteDebug:registerDebugger()
  if err then print(err) return end

  debug.log "Debug enabled."
  print( "Enabled remote debugging. Receiver: " .. remoteDebug.config.receiver )
elseif cmd == "enable" then
  if not remoteDebug then error( "No remoteDebug session found" ) end

  debug.log "Debug disabled."
  debug.disable()

  print( "Disabled remote debugging." )
elseif cmd == "connect" then
  if not remoteDebug then error( "No remoteDebug session found" ) end

  remoteDebug.serviceGetDeviceUrl = select( 2, ... ) .. "/v1/devices"
  remoteDebug.serviceSendDebugUrl = select( 2, ... ) .. "/v1/msgs"

  print( ("Debug server is %s"):format( select( 2, ... ) ) )
  debug.send( ("Debug server is %s"):format( select( 2, ... ) ) )
elseif cmd == "new_device" then
  if not remoteDebug then error( "No remoteDebug session found" ) end

  if remoteDebug.config then
    remoteDebug.config.deviceKey = nil
  end

  local ok = remoteDebug:getDevice()
  if ok then
    print( "Got new Device key from server." )
  else
    print( "Something went wrong." )
  end
elseif cmd == "info" then
  if not remoteDebug then error( "No remoteDebug session found" ) end

  local s = [[-- Info --
DeviceKey: %s
DevicePhon: %s
Receiver: %s
ServiceUrls:
  GetDevice: %s
  SendDebug: %s
Enabled: %s]]

  if not remoteDebug.config then
    remoteDebug:getConfig()
  end

  local deviceKey = remoteDebug.config and tostring( remoteDebug.config.deviceKey ) or "nil"
  local devicePhon = remoteDebug.config and tostring( remoteDebug.config.devicePhon ) or "nil"
  local receiver = remoteDebug.config and tostring( remoteDebug.config.receiver ) or "nil"
  local serviceGetDeviceUrl = remoteDebug.serviceGetDeviceUrl
  local serviceSendDebugUrl = remoteDebug.serviceSendDebugUrl
  local enabled = debug.isEnabled() and "yes" or "no"

  print( s:format( deviceKey, devicePhon, receiver, serviceGetDeviceUrl, serviceSendDebugUrl, enabled ) )
elseif cmd == "howtouse" then
  local s = [[
1. Open kiwigaming.de/cc
2. Navigate to 'Debugging Interface'
3. Copy the session name
4. Do rdebug receiver <session name> to set it to your session
5. Now enable it with rdebug enable (it should print you a message on the website)
6. Use debug.log(...) in your code]]
  print(s)
else
  local s = [[Usage:
rdebug receiver <name>
:This should match the session code shown on the website
rdebug enable
:Enables the sending of debug messages
rdebug disable
:Disables the sending of debug messages
rdebug connect <url>
:Full URL of alternative service
rdebug new_device
:Get a new device id from the service
rdebug help|nothing
:Displays this help message
rdebug howtouse
:how to use it]]
    print(s)
end