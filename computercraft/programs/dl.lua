local fs = require "filesystem"
local internet = require "internet"
local shell = require "shell"

local args, options = shell.parse(...)

if #args < 2 then
  io.write( "Usage: dl [-f] <from_url> <to>\n" )
  io.write( "-f: overwrite file if it already exists." )
end

local url = shell.resolve(args[1])
local to = shell.resolve(args[2])

if fs.isDirectory( to ) then
  to = to .. "/" .. fs.name( from )
end

if fs.exists( to ) then
  if not options.f then
    io.stderr:write( "target file exists" )
    return
  end
end

local file = io.open( to, "w" )

local req = internet.request( from )
local sb

for chunk in req do
  file:write( chunk )
end

file:close()
