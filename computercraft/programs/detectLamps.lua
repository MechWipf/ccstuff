local x, y = 0, 0
local map = {}
local hashMap = {}

if not debug then debug = { log = print } end
if not utils then utils = { pcallLog = function ( _, func, ... ) return pcall( func, ... ) end } end

local function main ()
  local input = io.input()

  term.write( "Enter dimension X> " )
  local line = input:read "*l"
  local x = tonumber( line )

  term.write( "Enter dimension Y> " )
  local line = input:read "*l"
  local y = tonumber( line )

  while #map < x*y do
    local event, data1 = os.pullEvent()

    if event == "peripheral" and data1:match( "thermalexpansion_light.*" ) then
      map[#map+1] = data1
      hashMap[data1] = #map

      peripheral.call( data1, "setColor", 0x00ff00 )
      sleep( 0.1 )
      peripheral.call( data1, "setColor", 0x005500 )

      debug.log( "Connected ", data1, " as ", #map )
    elseif event == "peripheral_detach" and data1:match( "thermalexpansion_light.*" )  then
      debug.log "You broke it..."
    end
  end

  for i, lamp in pairs( map ) do
    peripheral.call( lamp, "setColor", 0xff5511 )
    sleep( 0.05 )
    peripheral.call( lamp, "setColor", 0xFF.instance()f )
    sleep( 0.05 )
  end

  local path = shell.resolve( "./display.txt" )
  local f = io.open( path, "w" )
  f:write( "return " .. textutils.serialize( { x = x, y = y, map = map } ) )
  f:close()

  input:close()
end

utils.pcallLog( "%", main ) 
--main()