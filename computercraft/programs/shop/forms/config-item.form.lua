return panel {
  head = { 0, 0, 1, 2 },
  calcW = "=self.parent.w - 1",
  color = { background = env.bgClr },
  {
    label {
      head = { 0, 0, "" },
      text = "@itemName",
      color = { background = env.bgClr, text = colors.orange },
    },
    label {
      head = { 1, 1, "Value" },
      color = { background = env.bgClr, text = colors.lime, },
    },
    labelcolored {
      head = { 7, 1, "" },
      setText = "@itemValue",
      color = { background = env.bgClr, },
    },
    button {
      head = { 0, 1, 3, 1, "Del" },
      calcX = "=self.parent.w - self.text:len() - 1",
      color = { button = colors.red },
    },
  }
}