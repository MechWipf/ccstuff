return panel {
  name = "pnItem",
  head = { 0, 0, 1, 2 },
  calcW = "=self.parent.w - 1",
  color = { background = env.bgClr },
  {
    label {
      head = { 0, 0, "" },
      text = "@itemName",
      color = { background = env.bgClr },
    },
    label {
      head = { 0, 1, "C:" },
      color = { background = env.bgClr },
    },
    label {
      head = { 2, 1, "0" },
      color = { background = env.bgClr },
    },
    label {
      head = { 7, 1, "V:" },
      color = { background = env.bgClr },
    },
    label {
      head = { 9, 1, "0" },
      color = { background = env.bgClr },
    },
    button {
      head = { 0, 1, 0, 1 },
      text = "@btnText",
      calcW = "=self.text:len()",
      calcX = "=self.parent.w - self.text:len() - 1",
      color = { button = env.btnColor },
    },
  }
}