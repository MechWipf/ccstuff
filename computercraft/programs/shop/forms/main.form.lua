return panel {
  name = "pnMain",
  head = { 1, 1 },
  calcW = "@getTermW",
  calcH = "@getTermH",
  color = { background = colors.blue },
  {
    label {
      name = "lbTitle",
      head = { 0, 0, "Shop" },
      calcX = "=self.parent.w / 2 - self.text:len() / 2",
      color = { background = colors.blue }
    },
    panel {
      name = "pnMainContainer",
      head = { 0, 1 },
      calcW = "=self.parent.w",
      calcH = "=self.parent.h - 2",
      color = { background = colors.black },
    },
    button {
      name = "btnExit",
      head = { 1, 0, 1, 1, "X" },
      calcX = "=self.parent.w - 1",
      onClickLeft = "@btnExit.onClickLeft",
      color = { button = colors.red },
    },
    panel {
      name = "pnStatus",
      head = { 0, 0, 0, 1 },
      calcY = "=self.parent.h - 1",
      calcW = "=self.parent.w",
      color = { background = colors.gray },
      {
        label {
          name = "lbStatus",
          head = { 0, 0, "Wait for player." },
          color = { background = colors.gray },
        }
      }
    },
  }
}