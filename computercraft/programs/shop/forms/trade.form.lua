panel {
  name = "pnTradePlayer",
  head = { 0, 0 },
  calcW = "=math.floor(self.parent.w / 2)",
  calcH = "=self.parent.h",
  color = { background = colors.blue },
  {
    viewlist {
      name = "vlTradePlayer",
      head = { 0, 0, 5, 5, "item" },
      calcW = "=self.parent.w",
      calcH = "=self.parent.h - 1",
      color = { scroll = colors.blue },
    },
    label {
      head = { 0, 0, "Value: " },
      calcY = "=self.parent.h - 1",
      color = { background = colors.blue },
    },
    label {
      name = "lbPlayerValue",
      head = { 7, 0, "0" },
      calcY = "=self.parent.h - 1",
      color = { background = colors.blue },
    }
  }
}

panel {
  name = "pnTradeShop",
  head = { 0, 0 },
  calcW = "=math.floor(self.parent.w / 2)",
  calcH = "=self.parent.h",
  calcX = "=math.ceil(self.parent.w / 2)",
  color = { background = colors.brown },
  {
    viewlist {
      name = "vlTradeShop",
      head = { 0, 0, 5, 5, "item" },
      calcW = "=self.parent.w",
      calcH = "=self.parent.h - 1",
      color = { scroll = colors.brown },
    },
    label {
      head = { 0, 0, "Value: " },
      calcY = "=self.parent.h - 1",
      color = { background = colors.brown },
    },
    label {
      name = "lbPlayerValue",
      head = { 7, 0, "0" },
      calcY = "=self.parent.h - 1",
      color = { background = colors.brown },
    }
  }
}