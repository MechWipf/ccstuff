-- Initialization --

-- Includes
local _R = getfenv()
local    term,    colors,    os,    parallel,    shell,    utils,    sleep,    fs,    peripheral =
      _R.term, _R.colors, _R.os, _R.parallel, _R.shell, _R.utils, _R.sleep, _R.fs, _R.peripheral

local event_handler = require "vendor.event"()
local TabController = require "modularUI.helper".TabController
local Config = require "config".Config
local inv = require "inventory"
local cmd = select( 1, ... )
local rsbundle = require "rsbundle"
local modularUI

do
  modularUI = require "modularUI"()
  modularUI.dom = require "modularUI.dom"

  local path = _G.API_PATH .. "modularUI/widgets/"
  modularUI.loadWidget( path .. "base" )
  modularUI.loadWidget( path .. "panel" )
  modularUI.loadWidget( path .. "label" )
  modularUI.loadWidget( path .. "labelcolored" )
  modularUI.loadWidget( path .. "button" )
  modularUI.loadWidget( path .. "tbutton" )
  modularUI.loadWidget( path .. "viewlist" )
  modularUI.loadWidget( path .. "textbox" )

  modularUI.dom.widgets = modularUI.widgets
  modularUI.dom.clearWatches()
  modularUI.jobs = { modularUI.dom.jobWatch }
end

-- App
local appID = "shop_" .. math.random(100000, 999999)
local appDir = shell.getRunningProgram():match( "^(.*/).+$" )
local appConfigPath = _G.APPDATA_PATH .. "shop/config.von"
local app = {
  widgets = {},
  cleanShutdown = false,
  showConfig = cmd == "config",
}
-- -------------- --

-- Load Config --
local function loadConfig ()
  if not fs.exists( _G.APPDATA_PATH .. "shop" ) then fs.makeDir( _G.APPDATA_PATH .. "shop" ) end
  local defaultConfig = dofile( appDir .. "default-config.lua" )

  local init = not fs.exists( appConfigPath )

  app.config = Config( appConfigPath, defaultConfig )
  if init then app.config.save() end
end
-- ----------- --

-- Save Config --
local function saveConfig ()
  local p = {
    ["localInterface"] = app.form.tbLocalInterface:getText(),
    ["storeInterface"] = app.form.tbStoreInterface:getText(),
    ["ticketmachine"]  = app.form.tbTicketmachine:getText(),
    ["sensor"]         = app.form.tbSensor:getText(),
    ["doorSide"]       = app.form.tbRedstone:getText(),
  }
  app.config.peripherals.set( p )

  app.config.masterServer.set( app.form.tbServer:getText() )
  app.config.disableExit.set( app.form.btnDisableExit.isActive )
  app.config.autoRestart.set( app.form.btnRestart.isActive )

  app.config.save()
end
-- ----------- --

-- Add Config Item --
local function addConfigItem ( key, name, minVal, maxVal, rate, target )
  if key == "" or app.config.items.get( key ) then return false, "Invalid key." end
  if name == "" then return false, "Invalid name." end

  app.config.items.set( key, {
    ["name"]     = name,
    ["minValue"] = minVal,
    ["maxValue"] = maxVal,
    ["rate"]     = rate,
    ["target"]   = target
  })

  app.form.vlConfigItems:add({
    id = key,
    itemName = name,
    itemMinValue = minVal,
    itemMaxValue = maxVal,
    itemRate = rate,
    itemTargetCount = target,
  })

  return true
end
-- ---------------- --

-- Load Item Config from URL --
local function loadConfigURL ( url )
  local data = utils.curl( url )
  if not data then return false, "No data found." end

  local fn, err = loadstring( "return " .. data )
  if not fn then return false, err end

  local items = fn()

  app.form.vlConfigItems:clear()
  for key, item in pairs( items ) do
    app.form.vlConfigItems:add({
      id = key,
      itemName        = item.name,
      itemMinValue    = item.minValue,
      itemMaxValue    = item.maxValue,
      itemRate        = item.rate,
      itemTargetCount = item.target,
    })
  end

  app.config.items.set(items)
  app.form.vlConfigItems:invalidate()
end
-- ------------------------- --

-- Build Config Form --
local function loadConfigForm ()
  local widgets = app.widgets
  widgets.configItem = modularUI.dom.loadFile( appDir .. "forms/config-item.form.lua" )

  local i = 0
  local function wrapper ()
    i = i + 1
    local env = {
      itemName = "",
      itemValue = "",
      bgClr = i % 2 == 0 and colors.gray or colors.black,
    }
    local item = widgets.configItem( env )

    function item.apply ( _, data )
      local minValue = data.itemMinValue < 1 and ("1/%s"):format( 1 / data.itemMinValue ) or tostring(data.itemMinValue)
      local maxValue = data.itemMaxValue < 1 and ("1/%s"):format( 1 / data.itemMaxValue ) or tostring(data.itemMaxValue)

      env.itemValue = { minValue, colors.lime, "|", colors.white, maxValue }
      env.itemName = data.itemName
    end

    return item
  end

  app.form.configItemsWrapper = wrapper
  app.form.ItemModalLabel = "New Item"

  local function openModal ( widget )
    app.form.pnConfig:enable( false )
    widget:refresh()
    modularUI.startWatching( widget, term )
  end

  local function closeModal ( widget )
    app.form.pnConfig:enable(true)
    widget:kill()
    widget.isDead = false
  end

  app.form["btnLoadURL.onClickLeft"] = function ()
    openModal( app.form.pnConfigModalURL )
    app.form.tbLoadURL:clear()
  end

  app.form["btnConfigModalClose.onClickLeft"] = function ()
    closeModal( app.form.pnConfigModalURL )
  end

  app.form["btnConfigModalAccept.onClickLeft"] = function ()
    closeModal( app.form.pnConfigModalURL )
    loadConfigURL( app.form.tbLoadURL:getText() )
  end

  app.form["btnExit.onClickLeft"] = function ()
    openModal( app.form.pnCloseModal )
  end

  app.form["btnCloseModalCancel.onClickLeft"] = function ()
    closeModal( app.form.pnCloseModal )
  end

  app.form["btnCloseModalDiscard.onClickLeft"] = function ()
    os.queueEvent( "_exit" .. appID )
  end

  app.form["btnCloseModalSave.onClickLeft"] = function ()
    saveConfig()
    os.queueEvent( "_exit" .. appID )
  end

  app.form["btnItemsAdd.onClickLeft"] = function ()
    app.form.ItemModalLabel = "New Item"
    app.form.tbItemKey:clear()
    app.form.tbItemName:clear()
    app.form.tbItemMinValue:clear()
    app.form.tbItemMaxValue:clear()
    app.form.tbItemRate:clear()
    app.form.tbItemTarget:clear()
    openModal( app.form.pnConfigItemModal )
  end

  app.form["btnConfigItemModalClose.onClickLeft"] = function ()
    closeModal( app.form.pnConfigItemModal )
  end

  app.form["btnConfigItemModalSave.onClickLeft"] = function ()
    addConfigItem(
      app.form.tbItemKey:getText(),
      app.form.tbItemName:getText(),
      tonumber(app.form.tbItemMinValue:getText()),
      tonumber(app.form.tbItemMaxValue:getText()),
      tonumber(app.form.tbItemRate:getText()),
      tonumber(app.form.tbItemTarget:getText())
    )
    closeModal( app.form.pnConfigItemModal )
  end

  app.form["btnCheckbox.onClickLeft"] = function ( self )
    self.text = self.isActive and "Y" or "N"
    self:invalidate()
  end

  app.form["localInterface"] = app.config.peripherals.localInterface.get() or "unknown"
  app.form["storeInterface"] = app.config.peripherals.storeInterface.get() or "unknown"
  app.form["ticketmachine"]  = app.config.peripherals.ticketmachine.get()  or "unknown"
  app.form["sensor"]         = app.config.peripherals.sensor.get()         or "unknown"
  app.form["redstone"]       = app.config.peripherals.doorSide.get()       or "unknown"

  app.form["server"] = app.config.masterServer.get() or ""

  local disableExit = app.config.disableExit.get()
  if disableExit == nil then disableExit = false end
  app.form["disableExit"] = disableExit

  local autoRestart = app.config.autoRestart.get()
  if autoRestart == nil then autoRestart = false end
  app.form["autoRestart"] = autoRestart

  app.form.configTabController = TabController()
  modularUI.dom.buildFile( appDir .. "forms/config.form.lua", app.form, true )
  app.form.configTabController:setParent( app.form.pnConfigContainer )
  app.form.configTabController:setTab( "general" )

  app.form.btnDisableExit.text = disableExit and "Y" or "N"
  app.form.btnRestart.text     = autoRestart and "Y" or "N"

  for key, item in pairs( app.config.items.get() ) do
    app.form.vlConfigItems:add({
      id = key,
      itemName        = item.name,
      itemMinValue    = item.minValue,
      itemMaxValue    = item.maxValue,
      itemRate        = item.rate,
      itemTargetCount = item.target,
    })
  end

  return app.form.pnConfig
end
-----------------------

-- Build Main Form --
local function loadMainForm ()
  local widgets = app.widgets
  widgets.main = modularUI.dom.loadFile( appDir .. "forms/main.form.lua" )
  widgets.trade = modularUI.dom.loadFile( appDir .. "forms/trade.form.lua" )
  widgets.item = modularUI.dom.loadFile( appDir .. "forms/item.form.lua" )

  widgets.main( app.form )
  widgets.trade( app.form )

  local formContainer = app.form.pnMainContainer
  local formTmp

  formTmp = app.form.pnTradePlayer
  formTmp:setParent( formContainer )
  for i = 1, 5, 1 do
    local env = {
      bgClr = (i % 2 == 0) and colors.lightGray or colors.gray,
      btnColor = colors.blue,
      btnText = "Sell",
      itemName = "Player Test Item #" .. i,
    }

    local item = widgets.item( env )
    app.form.vlTradePlayer:add( item )
  end

  formTmp = app.form.pnTradeShop
  formTmp:setParent( formContainer )
  for i = 1, 30, 1 do
    local env = {
      bgClr = (i % 2 == 0) and colors.lightGray or colors.gray,
      btnColor = colors.brown,
      btnText = "Buy",
      itemName = "Shop Test Item #" .. i,
    }

    local item = widgets.item( env )
    app.form.vlTradeShop:add( item )
  end

  if app.config.disableExit.get() == true then
    app.form.btnExit:delParent()
    app.form.btnExit:kill()
  end

  return app.form.pnMain
end
-- --------------- --

-- Build Form --
local function loadForm ()
  local w,h = term.getSize()
  app.form = {
    getTermW = function ( _ )
      return w
    end,
    getTermH = function ( _ )
      return h
    end,

    ["btnExit.onClickLeft"] = function ( _ )
      os.queueEvent( "_exit" .. appID )
    end,
  }

  local watchForm
  if app.showConfig then
    watchForm = loadConfigForm()
  else
    watchForm = loadMainForm()
  end

  watchForm:refresh()
  modularUI.startWatching( watchForm, term )
end
-- ---------- --

local function loadPeripherals ()
  local localInterface = peripheral.wrap( app.config.peripherals.localInterface.get() or "unknown" )
  local storeInterface = peripheral.wrap( app.config.peripherals.storeInterface.get() or "unknown" )
  local ticketMachine  = peripheral.wrap( app.config.peripherals.ticketmachine.get()  or "unknown" )
  local doorRedstone   = app.config.peripherals.doorSide.get()

  app["noPeripherals"]   = ( not localInterface or not storeInterface or localInterface == storeInterface )
  app["noExchangeMoney"] = ( not ticketMachine )
  app.devices = {
    ["localInterface"] = localInterface,
    ["storeInterface"] = storeInterface,
    ["ticketMachine"]  = ticketMachine,
    ["doorRedstone"]   = doorRedstone,
  }
end

local function updateLocalItem ()
end

local function updateStoreItem ()
end

local function updateInventory ( interface )
  local items = inv.ae.getItems( interface )
  local configItems = app.config.items.get()

  local invItems = {}

  for _, item in pairs( items ) do
    local key = inv.fingerprintToString( item.fingerprint )
    local configItem = configItems[key] or configItems[key:gsub( "^(.+)(:.+)(:[0-9]+)(.*)$", "%1%2:*%4" )]
    if configItem then
      invItems[key] = item.size
    end
  end

  return invItems
end

local function updateLocalInventory ()
  local localInterface = app.devices.localInterface
  app.localItems = updateInventory( localInterface )

  local hasItems = next( app.localItems ) ~= nil

  if hasItems ~= app.hasItems then
    local doorSide = app.devices.doorRedstone
    local isBundle = rsbundle.split( doorSide )

    if isBundle then
      rsbundle.set( doorSide, hasItems )
    else
      rs.setOutput( doorSide, hasItems )
    end

    debug.log{ doorSide, hasItems }

    app.hasItems = hasItems
  end
end

local function updateStoreInventory ()
  local storeInterface = app.devices.storeInterface
  app.storeItems = updateInventory( storeInterface )
end

-- The Program --
local function program ()
  while app.showConfig == true do sleep(1e99) end
  while app.noPeripherals == true do sleep(1e99) end

  local i = 0
  while true do
    if i % 5 == 0 then
      updateLocalInventory()
    end
    if i % 10 == 0 then
      updateStoreInventory()
    end

    i = i + 1
    sleep(1)
  end
end
-- ----------- --

-- Run --
local function main ()
  local canExit = false
  repeat
    loadConfig()
    loadPeripherals()
    loadForm()

    parallel.waitForAny(
      function ()
        utils.pcallLog( "Event loop exits with error: %s", event_handler.mainLoop, event_handler )
      end,
      function ()
        utils.pcallLog( "UI loop exits with error: %s", modularUI.mainLoop, event_handler )
      end,
      function ()
        utils.pcallLog( "Program loop exits with error: %s", program )
      end,
      function ()
        os.pullEvent( "_exit" .. appID )
        app.cleanShutdown = true
        canExit = true
      end
    )

    if not app.cleanShutdown then
      term.setBackgroundColor( colors.blue )
      term.clear()
      term.setCursorPos( 1, 1 )

      term.setTextColor( colors.white )
      print( "The application encountered an unexpected error.\n" )
      term.setTextColor( colors.yellow )
      print( debug.lastError )

      if app.config.autoRestart.get() == true then
        print ""
        for i = 3, 1, -1 do
          print( "Restart in... " .. i )
          sleep(1)
        end
      else
        io.read "*l"
      end
    end
  until app.config.autoRestart.get() ~= true or canExit

  term.setBackgroundColor(colors.black)
  term.clear()
  term.setCursorPos( 1, 1 )
end

local ok, err = utils.trace( main )
if not ok then print(err) end
-- --- --
