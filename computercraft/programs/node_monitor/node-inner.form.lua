return panel {
  head = { 0, 1, 1, 1 },
  calcW = "=self.parent.w",
  {
    label {
      name = "lbAspectName",
      head = { 0, 0, "Name" },
    },
    label {
      name = "lbAspectCount",
      head = { 16, 0, "0" }
    },
    label {
      name = "lbAspectCountExtra",
      head = { 26, 0, "(+0)" }
    },
  }
}