return panel {
  name = "pnMain",
  head = { 1, 1 },
  calcW = "@pnMain.calcW",
  calcH = "@pnMain.calcH",
  color = { background = colors.orange },
  {
    label {
      name = "lbTitle",
      head = { 0, 0, "Node Monitor" },
      calcX = "=self.parent.w / 2 - self.text:len() / 2",
      color = { background = colors.orange }
    },
    panel {
      name = "pnBack",
      head = { 0, 1 },
      calcW = "@child.calcW",
      calcH = "@child.calcH",
      color = { background = colors.black },
    },
    button {
      name = "btnExit",
      head = { 1, 0, 1, 1, "X" },
      calcX = "=self.parent.w - 1",
      onClickLeft = "@btnExit.onClickLeft",
      color = { button = colors.red },
    }
  }
}