local term = term
local w,h = term.getSize()

term.clear()
term.setCursorPos( 1, 1 )
local str = ("."):rep( w )
term.blit( str, str, str:gsub(".","f") )