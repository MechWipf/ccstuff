package.path = package.path .. ";C:/Users/mechwipf/src/lua/?.lua"
local utils = require "libs.utils"

local file_path = select( 1, ... )
local items = dofile( select( 2, ... ) )

print( #items .. " items found." )

local function stringifyFingerprint ( f )
    return ("[%s]:%d:%s"):format( f.id, f.dmg, f.nbt_hash or "0" )
end

local function convert ( str )
  str = str:gsub( "%[", "{" )
  str = str:gsub( "%]", "}" )
  str = str:gsub( "([a-zA-Z0-9]+):([a-zA-Z0-9]+)([sb])", function ( s1, s2, ty )
    if s1:match("[0-9]+") then s1 = "["..s1.."]" end

    if s1 == "id" then
      for _, item in pairs( items ) do
        if item.id == s2 then
          s2 = item.name
        end
      end
    end

    if ty == "s" then s2 = "'"..s2.."'" end
    return s1.."="..s2
  end )
  str = str:gsub( "([0-9]+):{", "[%1]={" )
  str = str:gsub( "([a-zA-Z0-9]+):{", "['%1']={" )

  local fn, err = loadstring( "return " .. str )
  if not fn then
    error( err )
  end

  local km = {}
  local ret = {}
  local t = fn()

  for _, item in pairs( t["in"] ) do
    if item.id then
      local fingerprint = {
          id = item.id,
          dmg = item.Damage
      }

      local key = stringifyFingerprint( fingerprint )

      if not km[key] then
        km[key] = {
          qty = item.Count,
          fingerprint = fingerprint
        }
        ret[#ret+1] = km[key]
      else
        km[key].qty = km[key].qty + item.Count 
      end
    end
  end


  return ret
end

local out = {}
for line in io.lines( file_path ) do
  out[#out+1] = convert( line )
end

local f = io.open( "./pattern.lua", "w" )
f:write( "return " .. utils.serialize( out ) )
f:close()

print "Fin"