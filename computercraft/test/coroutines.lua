
local Events = dofile "db/apis/event.lua"
local eventHandler = Events()

local co = coroutine.create( function ()
    print("cookie")
end )

eventHandler:pushEvent( "runLegacy", function ()
    coroutine.resume( co )
    if coroutine.status( co ) == "dead" then
        os.queueEvent( "cancle" )
    end
end )

parallel.waitForAny( function ()
        eventHandler:mainLoop()
    end,
    function ( )
        os.pullEvent( "cancle" )
    end )