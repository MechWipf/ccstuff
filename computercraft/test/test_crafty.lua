require "test.testPatch"

local crafty = require "crafty"
--local utils = require "utils"
local json = require "vendor.json"
local console = require "console"

local app = {
  items = {},
  preferences = {},
}
local cmd = {}

local function findItem ( itemName, num )
  itemName = "^"..itemName.."$"

  local item
  local multi = 0

  for key, name in pairs( app.items ) do
    -- local fp = Crafty.fingerprintFromString( key )

    if string.match( name:lower(), itemName:lower() ) then
      multi = multi + 1
      item = key

      print( multi..":", key, "\t", name )
      if num and multi >= num then break end
    end
  end

  if multi > 1 and not num then
    return nil, "Found more then one item."
  end

  if not item then
    return nil, "No item found."
  end

  return item
end

function cmd.calc ( amount, itemName, num )
  -- Localize
  local myCrafty = app.myCrafty
  -- Convert some input
  amount = tonumber(amount)
  num = tonumber(num)


  local item, err = findItem( itemName, num )
  if err then
    print( err )
    return
  end

  local mats, leftover = myCrafty:calcRecipe( item, amount )

  print( "\n--- Final raw materials ---" )
  for matKey, matAmount in pairs( mats ) do
    local name = app.items[matKey]
    print( name or matKey, matAmount )
  end

  if next(leftover) ~= nil then
    print "\n--- leftover ---"
    for matKey, matAmount in pairs( leftover ) do
      local name = app.items[matKey]
      if matAmount > 0 then
        print( name or matKey, matAmount )
      end
    end
  end
end

local function show ( item )
  local myCrafty = app.myCrafty

  local recipes, err = myCrafty:getRecipe( item, true )
  if err then
    print( err )
    return
  end

  local name = app.items[item]
  print( "Resolve recipe for " .. name or item .. "." )
  print( "Found "..#recipes.." recipes." )

  local con = console.NewConsole( function () end )

  con:registerCommand( { "view", "v" }, function ( _, index )
    local recipe = recipes[tonumber(index)]

    print( recipe.pattern )
    for i, itemKey in pairs( recipe.items ) do
      local name = app.items[itemKey]
      print( i, name or itemKey )
    end
  end )

  con:registerCommand( { "select", "s" }, function ( self, index )
    if index == "none" then
      print "Marking recipe as unsolvable."
      app.preferences[item] = false
    else
      app.preferences[item] = tonumber(index)
      print "Save bla"
    end

    self:exit()
  end )

  con:registerCommand( { "quit", "exit", "q" }, function ( self )
    self:exit()
  end )

  con:run()
end

--- Cmd to show stuff
function cmd.show ( itemName, num )
  -- Convert some input
  num = tonumber(num)

  local item, err = findItem( itemName, num )
  if err then
    print( err )
    return
  end

  return show( item )
end

local function getItemNames ( url )
  local handler = http.get( url )
  local content = handler.readAll()

  return json.decode( content )
end

local function init ()
  if not _G.storage then
    _G.storage = {}
  end

  if not _G.storage.crafty then
    _G.storage.crafty = {}
  end

  if not _G.storage.crafty.names then
    local names = getItemNames "file://localhost/b:/mechwipf@hotmail.de/Dropbox/Public/ComputerCraft/ressources/items.json"
    --local names = getItemNames "https://dl.dropboxusercontent.com/u/17040958/ComputerCraft/ressources/items.json"
    _G.storage.crafty.names = names
  end

  if not _G.storage.crafty.instance then
    local myCrafty = crafty.NewCrafty()
    -- myCrafty:loadRecipes "https://dl.dropboxusercontent.com/u/17040958/ComputerCraft/ressources/all_recipes.json"
    -- myCrafty:loadOredict "https://dl.dropboxusercontent.com/u/17040958/ComputerCraft/ressources/oredictionary.json"
    myCrafty:loadRecipes "file://localhost/b:/mechwipf@hotmail.de/Dropbox/Public/ComputerCraft/ressources/all_recipes.json"
    myCrafty:loadOredict "file://localhost/b:/mechwipf@hotmail.de/Dropbox/Public/ComputerCraft/ressources/oredictionary.json"

    _G.storage.crafty.instance = myCrafty
  end

  app.items    = _G.storage.crafty.names
  app.myCrafty = _G.storage.crafty.instance

  app.myCrafty.chooseRecipe = function ( _, itemKey )
    if app.preferences[itemKey] == nil then
      show( itemKey )
    end

    if not app.preferences[itemKey] then
      print( "No recipe found for " .. app.items[itemKey] or itemKey )
      return nil, "No valid recipe."
    end

    return app.preferences[itemKey]
  end
end

local function main (...)
  local args = {...}
  local cmd_name = table.remove( args, 1 )

  local f = cmd[cmd_name]
  if f then
    return f( unpack( args ) )
  end
end

init()
main( ... )