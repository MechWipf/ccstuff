local items = {
  -- {
  --   fingerprint = { id = "minecraft:dye", dmg = 4 },
  --   qty = 10000
  -- }
}


-- local
local me, side
local inv = require "inventory"

if select( 2, ... ) == "1" then
  me = peripheral.wrap "right"
  side = "WEST"
else
  me = utils.getDevices "tileinterface"[1]
  side = "EAST"
end

for k,v in pairs( inv.getItems( me ) ) do
  print(k)
  items[#items+1] = v
end

items[1].qty = select( 1, ... )

local totalCount = 0
local moveCount = select( 1, ... )
print( "Moved: " .. totalCount )

local key = inv.fingerprintToString(items[1].fingerprint)

local t = os.clock()
local fin = false
while not fin do
  local c = inv.pushItems( me, side, items )

  if not c[key] then
    totalCount = moveCount
    fin = true
  else
    totalCount = moveCount - c[key]
    items[1].qty = c[key]
  end

  sleep(0)

  local x, y = term.getCursorPos()
  y = y - 1
  term.setCursorPos( 1, y )
  print( ("Moved: %d/%d (%d%%)"):format( totalCount, moveCount, totalCount/moveCount*100 ) )
end

print( os.clock() - t )