local utils = require "oc.utils"
local mui = require "mui"
-- local muiDom = require "mui.dom"
-- local coroutine = require "coroutine"
local event = require "event"
local term = require "term"

local widgetPath = "/mnt/dropbox/apis/mui/widgets/"

mui.loadWidget( widgetPath .. "base" )
mui.loadWidget( widgetPath .. "panel" )
mui.loadWidget( widgetPath .. "label" )
mui.loadWidget( widgetPath .. "button" )

local appName = "test_" .. math.random( 10000,99999 )

local function routineMuiLoop ()
  mui.handlerUpdate()
  mui.handlerDraw()
end

local function loadGUI ()
  local _, _, c_w, c_h = term.getGlobalArea()

  local pnBg = mui.widgets.panel( 1, 1, c_w, c_h )

  local text = "Test Program"
  local lbTitle = mui.widgets.label( math.floor( c_w / 2 - text:len() / 2 ), 0, text )
  lbTitle:setParent( pnBg )

  text = "X"
  local btnExit = mui.widgets.button( pnBg.w - 1, 0, 1, 1, text )
  btnExit.onClickLeft = function () event.push( "exit_" .. appName ) end
  btnExit:setColors{ button = 0xFF0000 }
  btnExit:setParent( pnBg )

  local pnMain = mui.widgets.panel( 0, 1, pnBg.w, pnBg.h - 1 )
  pnMain:setColors{ background = 0x000000 }
  pnMain:setParent( pnBg )

  text = "Hello World!"
  local lbHello = mui.widgets.label( math.floor( pnMain.w / 2 - text:len() / 2 ), math.floor( pnMain.h / 2 ), text )
  lbHello:setColors{ background = 0x000000 }
  lbHello:setParent( pnMain )

  pnBg:refresh()
  mui.startWatching( pnBg )
  print "Loaded GUI"
end

local function mainLoop ()
  loadGUI()
  local id = event.timer( 0.1, routineMuiLoop, 1e999 )

  local ok, err = pcall( function ()
    while true do
      local e = { event.pullMultiple( "touch", "key_down", "key_up", "scroll", "drag", "drop", "exit_" .. appName ) }
      local eventName = table.remove( e, 1 )

      if eventName == "exit_" .. appName then
        break
      elseif eventName == "touch" then
        mui.handlerClick( e[4], e[2], e[3], e[5] )
      elseif eventName == "key_down" then
        mui.handlerKeyDown( e[2], e[4] )
      elseif eventName == "key_up" then
        mui.handlerKeyDown( e[2], e[4] )
      elseif eventName == "scroll" then
        mui.handlerScroll( e[2], e[3], e[4], e[5] )
      elseif eventName == "drag" then
        mui.handlerDrag( e[4], e[2], e[3], e[5] )
      elseif eventName == "drop" then
        mui.handlerDrop( e[4], e[2], e[3], e[5] )
      end
    end
  end )

  mui.clearWatchlist()
  event.cancel( id )

  term.gpu().setBackground( 0x000000 )
  term.gpu().setForeground( 0xFFFFFF )
  term.clear()
  if not ok then
    error( err )
  end
end

mainLoop()