local power = peripheral.wrap "left"
local gate = peripheral.wrap "right"
local transeiver = peripheral.wrap "draconicevolution_tileenergytransceiver_0"

local free = 0

local getEnergyFlow = function () return transeiver.getMaxEnergyStored() - transeiver.getEnergyStored() end

while true do
  local f = power.getEnergyStored() / power.getMaxEnergyStored()

  if f > 0.95 then
    free = 500000
  elseif f > 0.90 then
    free = 250000
  elseif f > 0.80 then
    free = 50000
  else
    free = 0
  end

  gate.setSignalLowFlow( free )

  term.clear()

  local spaces = (" "):rep( 10 )
  print( ("Storage: %d%%%s"):format( f * 100, spaces ) )
  print( ("Free Energy: %d%s"):format( free, spaces ) )
  print( ("Current Flow: %d rf/t%s"):format( getEnergyFlow(), spaces ) )

  sleep( 1 )
end