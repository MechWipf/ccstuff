local _R = getfenv()
local peripheral = _R.peripheral
local utils = _R.utils
local term = _R.term
local sleep = _R.sleep

local relay = peripheral.wrap "left"

local _t = utils.unixTime()

local _energy = relay.getEnergyStored()
local maxEnergy = relay.getMaxEnergyStored()
local median = 0

local function prettyRF( x )
  local num, mod = utils.reduceNum( x )
  return string.format( "%d%s", num, mod )
end

while true do
  local energy = relay.getEnergyStored()
  local diffEnergy = energy - _energy
  _energy = energy

  local t = utils.unixTime()
  local diff = ( t - _t )
  _t = t

  median = ( median + ( diffEnergy / diff ) ) / 2
  if tostring(median) == "nan" then median = 0 end

  term.clear()
  term.setCursorPos(1,1)

  print( string.format( "Stored: %s/%s", prettyRF( energy ), prettyRF( maxEnergy ) ) )
  print( string.format( "Rate: %srf/t", prettyRF( median ) ) )

  sleep(2)
end