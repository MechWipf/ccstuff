
local inv = require "inventory"
local craft = peripheral.wrap "right"

local items = inv.getItems( craft )

local slots = {}

for k, item in pairs( items ) do
  for _, slot in pairs( item.slots ) do
    slots[slot-1] = item.fingerprint
  end
end

utils.fileWrite( "recipe.txt", utils.serialize( slots ) )