VMonitor = dofile '/db/apis/peripheralPlus/virtualMonitor.lua'
dofile '/db/apis/peripheralPlus/init.lua'


local mon = peripheral.wrap 'right'
local termH, termW = term.native().getSize()
local monH, monW = mon.getSize()

local vMonitor = VMonitor( 'vMonitor', math.min( termH, monH ), math.min( termW, monW ) )
vMonitor.addMirrow( 'term', term.native() )
vMonitor.addMirrow( 'monitor_right', mon )

peripheral.addVirtual( vMonitor )
term.redirect( vMonitor )
term.clear()
term.setCursorBlink( false )
