local _R = getfenv()

local inv = require "inventory"

local function getFilter ()
  local princess = false
  local drone = false
  local queen = false
  local frame = false

  return function ( fp, item )
    if not princess and item.raw_name == "item.for.beeprincessge" then
      princess = item
      return true
    end
    if not drone and item.raw_name == "item.for.beedronege" then
      drone = item
      return true
    end
    if not queen and item.raw_name == "item.for.beequeenge" then
      queen = true
    end
    if item.raw_name == "item.frameoblivion" then
      if fp.dmg + 10 >= item.max_dmg then
        frame = true
      end
    end


    return false
  end,
  function ()
    if princess and drone and not queen and not frame then
      return princess, drone
    else
      return false
    end
  end
end

local apiary = peripheral.wrap "right"

local function main ()
  local filter, ret = getFilter()

  inv.getItems( apiary, filter )

  local princess, drone = ret()
  if princess and drone then
    for k, slot in pairs( princess.slots ) do
      if slot == 1 then return end
    end
    princess.pushSlot = 1
    inv.pushItems( apiary, "north", { princess } )
    for k, slot in pairs( drone.slots ) do
      if slot == 2 then return end
    end
    drone.pushSlot = 2
    drone.qty = 1
    inv.pushItems( apiary, "north", { drone } )
  end
end

while true do
  main()
  sleep(5)
end

