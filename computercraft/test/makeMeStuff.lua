local _R = getfenv()
local turtle = _R.turtle
local utils = _R.utils
local sleep = _R.sleep

local json = require "vendor.json"

local function stringifyFingerprint ( f )
    return ("[%s]:%d:%s"):format( f.id, f.dmg, f.nbt_hash or "0" )
end

local app = {
  items = {}
}

local function init ()
  if not _R.items then
    _G.items = {}
  end

  if not _R.items.oredict then
    local oredict_json = utils.curl "https://dl.dropboxusercontent.com/u/17040958/ComputerCraft/ressources/oredictionary.json"
    _G.items.oredict = json.decode( oredict_json )
    sleep()
  end

  if not _R.items.names then
    local names_json = utils.curl "https://dl.dropboxusercontent.com/u/17040958/ComputerCraft/ressources/items.json"
    _G.items.names = json.decode( names_json )
    sleep()
  end
end



local function main (...)
  local cmd = select(1, ...)
  if cmd == "recipe" then
  elseif cmd == "search" then
    local keyword = "^" .. select( 2, ... ) .. "$"

    for _, itemName in pairs( _R.items.names ) do
      if type(itemName) == "string" and string.match( itemName, keyword ) then
        print( _ )
        print( "\t", itemName )
        sleep( 0.5 )
      end
    end
  elseif cmd == "craft" then
  end
end


init()
main(...)