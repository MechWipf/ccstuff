local gateInfo = peripheral.wrap('front').get
local engines = true
local _engines = false
local time = os.clock()
local reset = false
 
while true do
	local info = gateInfo()
	
	if info['Full Energy'] and engines == true then
		engines = false
	elseif info['No Energy'] and engines == false then
		engines = true
	end

	if reset then
		redstone.setOutput( 'left', false )		
		time = os.clock()
		reset = false
	end


	if os.clock() > time + 60 then
		redstone.setOutput( 'left', true )
		reset = true
	end

	if engines == false then
		time = os.clock()
	end
	
	if engines ~= _engines then
		redstone.setOutput( 'left', false )		
		time = os.clock()
		
		_engines = engines
		redstone.setOutput( 'right', engines )
		
		write('Turning engines ' .. ( engines and 'on' or 'off' ) .. '\n')
	end
	
	sleep(5)
end