function UnNom(dir,slot,amount)
	if slot then turtle.select(slot) end
	local fc = {turtle.dropUp,turtle.dropDown,turtle.drop}
	return fc[dir](amount)
end

function Nom(dir,slot)
	if slot then turtle.select(slot) end
	local fc = {turtle.suckUp,turtle.suckDown,turtle.suck}
	return fc[dir]()
end