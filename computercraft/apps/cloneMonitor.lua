local xwrite = term.write;
local xscroll = term.scroll;
local xsetCursorPos = term.setCursorPos;
local xsetCursorBlink = term.setCursorBlink;
local xclear = term.clear;
local xclearLine = term.clearLine;

local mon = peripheral.wrap('right');


function term.write(_text)
	mon.write(_text)
	return xwrite(_text)
end

function term.scroll(_n)
	mon.scroll(_n)
	return xscroll(_n)
end

function term.setCursorPos(_x,_y)
	mon.setCursorPos(_x,_y)
	return xsetCursorPos(_x,_y)
end

function term.setCursorBlink(_b)
	mon.setCursorBlink(_b)
	return xsetCursorBlink(_b)
end

function term.clear()
	mon.clear()
	return xclear()
end

function term.clearLine(_n)
	mon.clearLine(_n)
	return xclearLine(_n)
end

term.clear()
term.setCursorPos(1,1)
print("CraftOS 1.4")
