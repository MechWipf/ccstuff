os.loadAPI('apis/astar')

local function writePoint( X, Y, Clr, Char )
	if not term.isColour() and Clr ~= colours.white then Clr = colours.white end

	term.setCursorPos( X, Y )
	term.setBackgroundColor( Clr )
	term.write(Char or " ")
	term.setBackgroundColor( colours.black )
end

local Map = astar.newMap( 50, 18 )

term.clear()

for i = 0, 16 do
	Map:setNode( 5, i, false )
	writePoint( 5 + 1, i + 1, colours.white )
end

for i = 1, 17 do
	Map:setNode( 9, i, false )
	writePoint( 9 + 1, i + 1, colours.white )
end

for i = 0, 17 do
	if i ~= 10 then
	Map:setNode( 13, i, false )
	writePoint( 13 + 1, i + 1, colours.white )
	end
end

local startP 	= vector.new(  0,  0 )
local endP		= vector.new( 49, 17 )

local time = os.clock()
local path = astar.calc( Map, startP, endP )
time = os.clock() - time

--if path then
	for i, point in ipairs(path) do
		writePoint( point.x + 1, point.y + 1, colours.orange )
	end

	writePoint( startP.x + 1, startP.y + 1, colours.red  )
	writePoint(   endP.x + 1,   endP.y + 1, colours.blue )

--end
term.setCursorPos( 1, 19 )
write( "Calculation time: " .. time )
read()