local Length = 0
local Count = 0
local tArgs = { ... }

if #tArgs < 1 and #tArgs > 3 then
	print("Usage: tunnel3x3.lua <length> <delay(on/off)>")
	return
end

local MaxLength = tonumber(tArgs[1])
local delay = (tArgs[2] == "off")

local function tryMove(n)
	local dig  = 	{turtle.dig,		turtle.digUp,	 	turtle.digDown	  	}
	local move ={turtle.forward,	turtle.up,		 	turtle.down   	  	}
	local det  =	{turtle.detect,	turtle.detectUp, 	turtle.detectDown}
	
	while dig[n]() do
		sleep(0.5)
		if not det[n]() then break end
	end
	
	local timeout = 0
	while not move[n]() do sleep(0.1) timeout = timeout + 1 end
end

local function tryDig()
	Mining = false
	local i = 0
	while i < 9 do
		i = i + 1
		turtle.select(i)
		if turtle.getItemCount(i) == 0 or (turtle.compare() and turtle.getItemSpace(i) >= 4) then Mining = true i = 9 end
	end
	turtle.select(1)
	while turtle.dig() do
		Count = Count + 1
		if delay then sleep(0.5) end
		if not turtle.detect() then return true end
	end
	return false
end

local function tryDigUp()
	Mining = false
	local i = 0
	while i < 9 do
		i = i + 1
		turtle.select(i)
		if turtle.getItemCount(i) == 0 or (turtle.compareUp() and turtle.getItemSpace(i) >= 4) then Mining = true i = 9 end
	end
	turtle.select(1)
	while turtle.digUp() do
		Count = Count + 1
		if delay then sleep(0.5) end
		if not turtle.detectUp() then return true end
	end
	return false
end

local Mining = true
while Mining do
	tryMove(1)
	turtle.turnLeft()
	tryDig()
	tryMove(2)
	tryDig()
	tryMove(2)
	tryDig()
	
	turtle.turnRight()
	turtle.turnRight()
	tryDig()
	tryMove(3)
	tryDig()
	tryMove(3)
	tryDig()
	turtle.turnLeft()

	print("Blocks mined.. "..Count)
	
	Length = Length + 1
	if not turtle.detectDown() then turtle.placeDown() end
	if Length > MaxLength then print("Finished tunneling, returning.") Mining = false end
end

turtle.turnRight()
turtle.turnRight()

while Length > 0 do
	tryMove(1)
	Length = Length - 1
end

turtle.turnRight()
turtle.turnRight()

print("Returned.\nTotaly Mined Blocks: "..Count)
print("Push the 'Any Key' to shutdown.")
io.read()
