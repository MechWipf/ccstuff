rednet.open('right')

local server = -1

while server == -1 do
	print('waiting for connection...')
	local id,msg = rednet.receive()
	print(id,msg)
	if msg == 'remote.getTurtles()' then rednet.send(id,'true'); print('Message from id '..id..'.') end
	if msg == 'remote.setTurtle()' then rednet.send(id,'true'); server = id; print('Connected to id '..id..'.') end
end

while true do
	local id,msg = rednet.receive()
	if id == server then
		print(msg)
		local func = loadstring(msg,'lua')
		func()
	else
		if msg == 'remote.getTurtles()' then rednet.send(id,'false') end
	end
end