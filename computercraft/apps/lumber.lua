local tArgs = {...}
print("Checking content")

if #tArgs < 2 or #tArgs > 3 then
	print("Useage: lumber <x> <y> [empty]")
	return
end
local X = tonumber(tArgs[1])
local Y = tonumber(tArgs[2])
local Empty = tArgs[3] == "true"

local count = turtle.getItemCount(1)
local scount = X*Y
if count < scount then
	print("Insert atleast "..scount.." Saplings in Slot 1")
	while turtle.getItemCount(1) < scount do sleep(0.5) end
end

local function move(nDir, n)
	local dir = { turtle.forward, turtle.back, turtle.down, turtle.up }
	for _ = 1, n, 1 do while not dir[nDir]() do end end
end

local function place()
	while not turtle.detect() do turtle.place() end
end

turtle.select(1)

for i = 1, Y, 1 do
	for k = 1, X, 1 do
		local height = 0
	
		move( 1, k == 1 and 1 or 3 )
		turtle.dig()
		move( 1, 1 )
		while turtle.digUp() do move( 4, 1 ) height = height + 1 end
		move( 3, height )
	end
	
	for k = 1, X, 1 do
		move( 2, 1)
		place()
		if k < X then move( 2, X == 1 and 1 or 3 )
		else move( 2, 1 ) end
	end
	
	turtle.turnLeft()
	if i < Y then
		move( 1, 4 )
		turtle.turnRight()
	end
end

if Y > 1 then move( 2, (Y-1)*4 ) end
turtle.turnRight()

if Empty then
	turtle.turnRight()
	turtle.turnRight()
	for i = 1, 9, 1 do
		turtle.select(i)
		turtle.drop()
	end
	turtle.turnRight()
	turtle.turnRight()
end

print("Finished lumberjacking.")
shell.run("shutdown")
