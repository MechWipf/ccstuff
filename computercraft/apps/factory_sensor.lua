local bExit = false;
local tLen = {};
local tBundleData = {};
local tDelay = {};
local nRuntime = 0;
local trash = {};

local tSimplyfy = {
	['Condenser'] = 'Condenser',
	['DIAMOND'] = 'Diamond Chest'
}

local tItems = {
	['item.itemIngotAdvIron'] = 'Ref. Iron',
	['item.itemIngotTin'] = 'Tin',
	['item.itemDustCoal'] = 'Coal Dust',
	['item.itemRubber'] = 'Rubber',
	['item.itemIngotCopper'] = 'Copper',
	['item.coal'] = 'Coal',
	['item.redstone'] = 'Redstone',
	['item.itemPartCircuit'] = 'Circuit',
	['item.itemBatRE'] = 'REBattery',
	['tile.glass'] = 'Glass',
	['tile.furnace'] = 'Furnace',
	['null@3'] = 'Solar Panel',
	['null@0'] = 'Copper Cable'
}

term.setCursorBlink(false)
term.setCursorPos(1,1)
term.clear()

if not fs.exists('appdata') then fs.makeDir('appdata') end
if not fs.exists('appdata/factory') then fs.makeDir('appdata/factory') end
if not fs.exists('appdata/factory/bundle.cfg') then
	local data = ''

	for k,v in pairs(colors) do
		if type(v) == 'number' then
			tBundleData[k] = nil
			data = data .. k .. ':' .. 'nil' .. '\n'
		end
	end
	
	local f = fs.open('appdata/factory/bundle.cfg','w')
	f.write(data)
	f.close()
else
	print('Loading Bundle.cfg')
	local line = ''
	
	local f = fs.open('appdata/factory/bundle.cfg','r')
	while line do
		line = f.readLine()
		if line then
			local clr, p = string.match(line,'(.*):(.*)')
			if p == 'nil' then p = nil end
			tBundleData[clr] = p
			print(clr,':',p or 'nil')
			sleep(0.1)
		end
	end
	f.close()
	sleep(0.5)
end

function writePos(x,y,str)
	local _x,_y = term.getCursorPos()
	term.setCursorPos(x,y)
	write(str)
	term.setCursorPos(_x,_y)
end

function findInTable(_t,_c)
	for k,v in pairs(_t) do
		if type(k) == 'string' and string.find(_c,k) then return v end
	end
	return nil
end

function findInTableR(_t,_c)
	for k,v in pairs(_t) do
		if type(v) == 'string' and string.find(_c,v) then return k end
	end
	return nil
end

local osPullEvent = os.pullEvent
function os.pullEvent( _sFilter )
	local event, p1, p2, p3, p4, p5 = os.pullEventRaw( _sFilter )
	if event == 'terminate' then
		bExit = true
		os.pullEvent = osPullEvent
	end
	return event, p1, p2, p3, p4, p5
end

local first = true
while not bExit do
	local pControler = peripheral.wrap('right')
	local sSensor = 'Sensor'
	local sProbe  = 'InventoryInfo'
	local sProbe2 = 'InventoryContent'
	local tTargetsx = {pControler.getAvailableTargets(sSensor,sProbe)}
	local tTargets = {}
	
	for i,sTarget in pairs(tTargetsx) do
		local str = nil
		
		str = findInTable(tSimplyfy,sTarget)
		
		if not str then str = string.match(sTarget,'(.+),.+,.+,.+') end
		if not string.find(str,'Turtle') then tTargets[#tTargets+1] = {str,sTarget} end
	end

	if first then
		term.clear()
		term.setCursorPos(1,1)
		
		writePos(1,1,'| Inventories |Content U/%|       Status         |')
		writePos(1,2,'|-------------|-----------|----------------------|')
	end

	
	local last = 3
	for k,v in pairs(tTargets) do
		local printName = v[1]
		local uniqName  = v[2]
		local i = k+2
		--################################################--
		pControler.setTarget(sSensor,uniqName)
		pControler.setReading(sSensor,sProbe)
		local data = {pControler.getSensorReading(sSensor)}
		pControler.setReading(sSensor,sProbe2)
		local content = {pControler.getSensorReading(sSensor)}
		--################################################--
		local len = tLen[{2,i}] or 0
		local cLen = string.len(printName)
		tLen[{2,i}] = cLen

		local str = '|'..printName..string.rep(' ',math.max(len-cLen,0))
		writePos(1,i,string.sub(str,1,15))
		writePos(15,i,'|')
		--################################################--
		
		local unit = data[8]
		local perc = math.floor(data[8]/(data[6]*64)*1000)/10
		local str = unit..string.rep(' ',4-string.len(unit))
		local strp = perc..string.rep(' ',5-string.len(perc))
		writePos(16,i,str)
		writePos(22,i,strp)
		
		--################################################--
		
		local item = content[2] or ''
		local str = findInTable(tItems,item)
		if not str then str = 'Empty' end
		
		
		local add = ''
		if str == "Solar Panel" then
			add = ' @Output'
		elseif str == 'Coal' then
			add = ' @Fuel'
		elseif str == 'Circuit' or str == 'REBattery' or str == 'Copper Cable' then
			add = ' @Sorting'
		elseif str == 'Empty' then
			--nothing
		elseif math.floor(os.clock())%2 == 0 and unit < 256 then
			add = ' @Refill!!'
		elseif (perc < 20) or (perc < 80 and trash['Refill.'..uniqName]) then
			local clr = findInTableR(tBundleData,item)
			
			if clr then
				trash['Refill.'..uniqName] = true
				if not tDelay[clr] or tDelay[clr] - nRuntime < 0 then
					tDelay[clr] = nRuntime + 5
					rsbundle.pulse(clr)
				end
			
				add = ' @Auto Refill'
			end
		elseif perc > 80 and trash['Refill.'..uniqName] then
			trash['Refill.'..uniqName] = false
		end
		
		str = str .. add
		str = str .. string.rep(' ',22-string.len(str))
		writePos(28,i,string.sub(str,1,22))

		
		writePos(27,i,'|')
		writePos(50,i,'|')
		last = i+1
	end
	
	if first then
		writePos(1,last,'|------------------------------------------------|')
		first = false
		if trash.last ~= last then
			first = true
			trash.last = last
		end
	end


	--bExit = true
	sleep(0.5)
	nRuntime = nRuntime + 0.5
end

term.clear()
term.setCursorPos(1,1)
