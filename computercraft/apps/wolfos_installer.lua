local dlink = "http://dl.dropbox.com/u/17040958/ComputerCraft/WolfOS/files/"
local f2install =
{
	"f#startup",
	"d#WOLF",
	"d#WOLF/APIS",
	"f#WOLF/APIS/WAPI",
	"f#WOLF/APIS/WFCI",
	"f#WOLF/APIS/WNIL",
	"f#WOLF/APIS/WPCI",
	"d#WOLF/data",
	"f#WOLF/data/easter_egg.txt",
	"d#WOLF/utilities",
	"f#WOLF/utilities/textEditor",
	"d#WOLF/WolfOS",
	"d#WOLF/WolfOS/Client",
	"d#WOLF/WolfOS/Client/programs",
	"d#WOLF/WolfOS/Client/programs/utilities",
	"f#WOLF/WolfOS/Client/programs/utilities/diskManager",
	"f#WOLF/WolfOS/Client/programs/utilities/fileBrowser",
	"f#WOLF/WolfOS/Client/programs/utilities/textEditor",
	"f#WOLF/WolfOS/Client/programs/musicPlayer",
	"f#WOLF/WolfOS/Client/mainMenu",
	"f#WOLF/WolfOS/Client/programMenu",
	"f#WOLF/WolfOS/Client/utilityMenu",
	"f#WOLF/controlPanel",
	"f#WOLF/login",
	"f#WOLF/startup",
	"f#WOLF/startupMenu"
}

local function httpRequest(url)
	http.request(url)
	return http.get(url)
end


for i = 1, #f2install, 1 do
	local str = f2install[i]
	ftype, str = string.match(str,"(.+)#(.+)")

	if fs.exists(str) then fs.delete(str) end
	if ftype == "f" then
		local file = fs.open(str,"w")
		local data = httpRequest(dlink..str)
		file.write(data:readAll())
		data.close()
		file.close()
		print(str)
	elseif ftype == "d" then
		fs.makeDir(str)
	end
	
end