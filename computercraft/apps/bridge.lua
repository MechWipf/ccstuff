local tArgs = {...}

if #tArgs < 2 or #tArgs > 3 then
	print("Usage: bridge.lua <width> <length> [driveback(0|1)]")
	return
end

local mWidth = tonumber(tArgs[1])
local mLength = tonumber(tArgs[2])
local building = true
local driveBack = tArgs[3] == "1"
local pos = vector.new(0,0,0)
local dir = vector.new(1,0,0)
local ang = 0


local function rotated(self,phi)
	local c, s = math.cos(phi), math.sin(phi)
	return vector.new(c * self.x - s * self.y, s * self.x + c * self.y,self.z)
end

local function forward()
	local b = turtle.forward()
	pos = pos + ( b and rotated(vector.new(1,0,0),ang*(math.pi/180)):round() or vector.new(0,0,0) )
	print(pos)
	return b
end

local function up()
	local b = turtle.forward()
	pos = pos + ( b and vector.new(0,0,1) or vector.new(0,0,0) )
	print(pos)
	return b
end

local function down()
	local b = turtle.forward()
	pos = pos + ( b and vector.new(0,0,-1) or vector.new(0,0,0) )
	print(pos)
	return b
end

local function left()
	turtle.turnLeft()
	ang = ang - 90
	print(ang)
end

local function right()
	turtle.turnRight()
	ang = ang + 90
	print(ang)
end

local function tryMove(n)
	local dig  = 	{turtle.dig,		turtle.digUp,	 	turtle.digDown	  	}
	local move ={forward,			up,		 			down   	  				}
	local det  =	{turtle.detect,	turtle.detectUp, 	turtle.detectDown	}
	
	while dig[n]() do
		sleep(0.5)
		if not det[n]() then break end
	end
	
	local timeout = 0
	while not move[n]() do sleep(0.1) timeout = timeout + 1 end
end

while mLength-pos.x > 0 do
	tryMove(1)
	right()
	for i = 1, mWidth, 1 do
		turtle.digDown() turtle.placeDown()
		if i ~= mWidth then tryMove(1) end
	end
	left()
	tryMove(1)
	left()
	for i = 1, mWidth, 1 do
		turtle.digDown() turtle.placeDown()
		if i ~= mWidth then tryMove(1) end
	end
	right()
end

if driveBack then
	right() right()
	while pos.x > 0 do tryMove(1) end
end