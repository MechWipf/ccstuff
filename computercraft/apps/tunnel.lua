if not movement then os.loadAPI( 'apis/movement' )
if not movement then die( 'Cant load movement api.' )

local mov = movement
local tArgs = {...}
local mLength = 0
local length = 0
local mWidth = 0
local width = 0
local mHeight = 0
local height = 0
local delay = true

if #tArgs < 3 or #tArgs > 4 then
	print("Usage: tunnel.lua <x> <y> <z> <delay(on/off)>")
	return
else
	mWidth  = tonumber(tArgs[1])
	mHeight = tonumber(tArgs[2])
	mLength = tonumber(tArgs[3])
	delay = not tArgs[4] == "off"
end

local Mining = true
while Mining do
	if length ~= mLength do move(1) end
	length = length + 1
	
	turtle.turnLeft()
	
end