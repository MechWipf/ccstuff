os.loadAPI('programs/apis/timer')

local mycolor = 1
local input = ''
local pass = 'cookies!'
local blarg = rs.setBundledOutput
local fu = tonumber

local function open()
	blarg('bottom',fu('000',2))
	sleep(0.5)
	blarg('bottom',fu('001',2))
	sleep(0.5)
	blarg('bottom',fu('101',2))
	sleep(0.5)
	blarg('bottom',fu('001',2))
	sleep(0.5)
	blarg('bottom',fu('101',2))
	sleep(0.5)
	blarg('bottom',fu('001',2))
end

local function close()
	sleep(0.5)
	blarg('bottom',fu('001'),2)
	sleep(0.5)
	blarg('bottom',fu('011'),2)
	sleep(0.5)
	blarg('bottom',fu('001'),2)
	sleep(0.5)
	blarg('bottom',fu('011'),2)
	sleep(0.5)
	blarg('bottom',fu('001'),2)
	sleep(0.5)
	blarg('bottom',fu('000'),2)
end

while true do
	term.clear()
	term.setCursorPos(1,1)
	write('Enter Password: ')
	inp = io.read()
	if inp == pass then
		print('Password correct!')
		open()
		sleep(1)
		io.read()
		close()
	end
end