--[[--------------------------------------------------------------------------------------
											EnergyInfoPanel
--------------------------------------------------------------------------------------]]--
if not von then
	if not os.loadAPI( "apis/von" ) then error( "Failed while loading apis/von." ) end
end

if not goroutine then
	if not os.loadAPI( "apis/goroutine" ) then error( "Failed while loading apis/goroutine" ) end
end

local rn = rednet
local InfoP = {}
local term = term

local function coGetSensors()
	while true do
		if not rn.isOpen( "back" ) then rn.open( "back" ) end
		rn.send( 43, "getAllSensors" )
		local _, sCards = rn.receive()
		InfoP.Cards = von.deserialize( sCards )
		print(os.clock() .. "\treceived cards. " .. #InfoP.Cards .. "\n")
		sleep(3)
	end
end

local function main()
	print("Started")
	goroutine.spawn( "getSensors", coGetSensors )

	local lastT = ""
	while true do
		os.sleep(1)
		
		if InfoP.Cards and tostring( InfoP.Cards ) ~= lastT then
			lastT = tostring( InfoP.Cards )
			
			local Cardnr = 0
			for k,Card in ipairs( InfoP.Cards ) do
				term.setCursorPos( 1, 0 + k )
				
				if Card._energyL then
					Cardnr = Cardnr + 1
					term.write( "Energy in Line " .. Cardnr .. ": " .. Card._energyL )
				end
			end
		end		
	end
	
	print("Ended")
end

--parallel.waitForAll( main, coGetSensors )
goroutine.run( main )