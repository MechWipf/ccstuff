local tArgs = {...}

if #tArgs < 2 or #tArgs > 3  then
	print("Usage: sdig <length> <maxheight> {rows}")
	return
end

local function tryMove(n)
	local dig  = {turtle.dig,		turtle.digUp,	 turtle.digDown	  }
	local move = {turtle.forward,	turtle.up,		 turtle.down   	  }
	local det  = {turtle.detect,	turtle.detectUp, turtle.detectDown}
	
	while det[n]() and dig[n]() do
		sleep(0.5)
		if not det[n]() then break end
	end
	local timeout = 0
	return move[n]()
end

local function tryDig(n)
	local _dig  = {turtle.dig,		turtle.digUp,	 turtle.digDown	  }
	local _det  = {turtle.detect,	turtle.detectUp, turtle.detectDown}
	local dig  = _dig[n]
	local det  = _move[n]
	
	while det() and dig() do
		sleep(0.5)
		if not det() then return true end
	end
	return false
end

local maxLength = tonumber(tArgs[1])
local maxHeight = math.abs(tonumber(tArgs[2]))
local Rows = tonumber(tArgs[3] or "1")
local Dig = true
local Dir = tonumber(tArgs[2]) < 0
if Dir then Dir = 1 else Dir = 0 end
local Length = 0

local length = 0
while Dig do
	if length ~= maxLength then tryMove(1) end
	length = length + 1
	
	local height = 0
	while tryMove(2+Dir) and height < maxHeight do height = height + 1 end
	for i = 1, height, 1 do tryMove(3-Dir) end
	if length == maxLength then Dig = false end
end
turtle.turnRight()
turtle.turnRight()
for i = 1, length, 1 do tryMove(1) end
turtle.turnRight()
turtle.turnRight()
