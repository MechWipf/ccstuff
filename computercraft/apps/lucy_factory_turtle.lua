-- Defining Item Positions
local RubberPos     = {1,1}
local CoalDustPos   = {2,1}
local IronPos       = {3,1}
local CopperPos     = {4,1}

local TinPos        = {1,2}
local FurnacePos    = {2,2}
local GlassPos      = {3,2}
local RedstonePos   = {4,2}

local CablePos      = {4,3}
local CircuitsPos   = {3,3}
local BatteryPos    = {2,3}
local GeneratorPos  = {1,3}

local Pos        = 1
local Dir        = 2
local NoHazCable = 0

local Multiplier = 4

local math = math

-- Function fuckshit
function Move(pos)
	local dif = Pos - pos
	
	if not (dif == 0) then
		for i = 1,(dif < 0 and -dif or dif) do
			if dif < 0 then
				turtle.back()
			else
				turtle.forward()
			end
		end
		
		Pos = pos
	end
	
--	sleep(0.5)
end

function UnNom(dir,slot,amount)
	if slot then turtle.select(slot) end
	local fc = {turtle.dropUp,turtle.dropDown,turtle.drop}
	return fc[dir](amount)
end

function Nom(dir,slot)
	if slot then turtle.select(slot) end
	local fc = {turtle.suckUp,turtle.suckDown,turtle.suck}
	return fc[dir]()
end

function OmNom(dir,slot,amount)
	amount = amount * Multiplier
	
--	local checkright = (slot < 4) or ((slot > 4) and (slot < 8)) or ((slot > 8) and (slot < 12))
--	local slotright = 0
--	if checkright == true then
--		slotright = turtle.getItemCount(slot + 1)
--		print("slot fits in crafting area. checkright = true")
--		checkright = slotright == 0
--		print("right slot is " .. (checkright and "empty, still true" or "used, falsed"))
--	end
	
	Nom(dir,slot)
	local count = turtle.getItemCount(slot)
	
	if checkright == true then
		if turtle.getItemCount(slot + 1) > 0 then
			UnNom(dir,slot + 1,64)
		end
	end
	
	while count < amount do
		Nom(dir,slot)
		count = turtle.getItemCount(slot)
		
		sleep(0.3)
	end
	
	UnNom(dir,slot,turtle.getItemCount(slot) - amount)
end

function Crafty(amount,slot)
	amount = amount * Multiplier
	if slot then
		turtle.select(slot)
	end
	
	turtle.craft(amount)
	
--	sleep(0.5)
end

function bck()
	for i = 1,4 do
		turtle.back()
	end
end

print("Loaded Solar Factory program v0.5.8")
print("Moving to initial position...")

-- Entering Initial Position
Move(4)

print("Done!")

while true do
	if turtle.getFuelLevel() < 36 then
		Move(-1)
		
		Nom(1,1)
		turtle.refuel()
	else
		Move(4)
		turtle.turnRight()
		Nom(3,1)
		NoHazCable = (turtle.getItemCount(1) > Multiplier - 1) and 0 or 1
		if NoHazCable == 0 then
--			print("Yez, i haz cable")
		else
--			print("nope, no cablez :<")
		end
		
		UnNom(3,1,64)
		turtle.turnLeft()
		
		Move(RubberPos[1])
		for i = 1,6 do
			local int = i + ((i > 3) and 5 or 0)
			
			OmNom(RubberPos[2],int,2 + NoHazCable)
		end
		
		Move(CopperPos[1])
		for i = 1,3 do
			OmNom(CopperPos[2],i + 4,2 + NoHazCable)
		end
		
		Crafty(10,15)
		sleep(0.5)
		Crafty(10,16)
		
		-- Cables done
		-- Sort them to make the circuits
		
		Move(4)
		turtle.turnRight()
		UnNom(3,15,64)
		UnNom(3,16,64)
		
		OmNom(3,1,2)
		OmNom(3,2,3)
		OmNom(3,3,2)
		OmNom(3,9,2)
		OmNom(3,10,2)
		OmNom(3,11,2)
		turtle.turnLeft()
		
		-- alrighty
		
		Move(RedstonePos[1])
		OmNom(RedstonePos[2],5,2)
		OmNom(RedstonePos[2],7,2)
		
		Move(IronPos[1])
		OmNom(IronPos[2],6,2)
		
		Crafty(2 + NoHazCable,16)
		
		Move(CircuitsPos[1])
		turtle.turnRight()
		UnNom(3,16,64)
		turtle.turnLeft()
		
		-- 2 Circuits done!
		-- Let's do the battery...
		
		Move(TinPos[1])
		OmNom(TinPos[2],5,1)
		OmNom(TinPos[2],7,1)
		OmNom(TinPos[2],9,1)
		OmNom(TinPos[2],11,1)
		
		Move(RedstonePos[1])
		OmNom(RedstonePos[2],6,1)
		OmNom(RedstonePos[2],10,1)
		
		Crafty(1,16)
		
		Move(2)
		turtle.turnRight()
		UnNom(3,16,64)
		turtle.turnLeft()
		
		-- Battery done!
		-- Lets do the machine block
		
		Move(IronPos[1])
		
		OmNom(IronPos[2],1,1)
		OmNom(IronPos[2],2,1)
		OmNom(IronPos[2],3,1)
		OmNom(IronPos[2],5,1)
		OmNom(IronPos[2],7,1)
		OmNom(IronPos[2],9,1)
		OmNom(IronPos[2],10,1)
		OmNom(IronPos[2],11,1)
		
		Crafty(1,5)
		
		-- Alrighty, did this one too
		-- Now to the generator
		
		Move(2)
		turtle.turnRight()
		OmNom(3,1,1)
		
		Move(FurnacePos[1])
		OmNom(FurnacePos[2],9,1)
		
		Crafty(1,10)
		turtle.turnLeft()
		
		-- Did the generator!
		-- Almost there!
		
		Move(CircuitsPos[1])
		turtle.turnRight()
		OmNom(3,9,1)
		OmNom(3,11,1)
		turtle.turnLeft()
		
		Move(CoalDustPos[1])
		OmNom(CoalDustPos[2],1,1)
		OmNom(CoalDustPos[2],3,1)
		OmNom(CoalDustPos[2],6,1)
		
		Move(GlassPos[1])
		OmNom(GlassPos[2],2,1)
		OmNom(GlassPos[2],5,1)
		OmNom(GlassPos[2],7,1)
		
		Crafty(1,16)
		
		Move(-1)
		UnNom(2,16,64)
	end
end

for i = 1,4 do
	turtle.back()
end