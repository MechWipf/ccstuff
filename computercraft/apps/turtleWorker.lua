if not von then
	if not os.loadAPI "apis/von" then
		error "Failed while loading 'apis/von'"
	end
end

if not event then
	if not os.loadAPI "apis/event" then
		error "Failed while loading 'apis/event'"
	end
end


local sModemSide = nil
for n,sSide in ipairs( rs.getSides() ) do
	if peripheral.getType( sSide ) == "modem" then
		sModemSide = sSide
		break
	end
end

if not sModemSide then
	error "No modem attached!"
end

local ev = event.events()
local modem = peripheral.wrap( sModemSide )
local CHANNEL_RC = 65533
local tEnv = {}
setmetatable( tEnv, { __index = getfenv() } )

if not modem.isOpen( CHANNEL_RC ) then
	modem.open( CHANNEL_RC )
end

ev:event( "modem_message", "rc_MMessage", function( _, sSide, sChannel, sReplyChannel, sMessage, nDistance )
	if sChannel == CHANNEL_RC or sChannel == os.getComputerID() then
		if string.find( sMessage, "run:" ) then
			local sFunc = string.match( sMessage, "^run%:(.+)$" )
			local func, err = loadstring( sFunc )
			local ret = nil

			if func then
				setfenv( func, tEnv )
				valid, ret = pcall( func )

				if not valid then err = ret func = nil end
			end

			if not func then
				modem.transmit( sReplyChannel, os.getComputerID(), err)
				return false
			end

			if ret then
				if type( ret ) == "table" then ret = von.serialize( ret ) end
				elseif type( ret ) ~= "string" then ret = tostring(ret) end
				modem.transmit( sReplyChannel, os.getComputerID(), ret )
			end
		end
end)

print("Remotecontrol activated!")
ev:mainLoop()
