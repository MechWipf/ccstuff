local yield = coroutine.yield

function coroutine.yield( _sFilter )
	local event, p1, p2, p3, p4, p5 = yield( _sFilter )
	if event == "terminate" then
		--print( "Starting Shell" )
		os.run({},'rom/programs/shell')
	end
	return event, p1, p2, p3, p4, p5
end

function os.shutdown()
	print('Canceled shutdown.')
end

function os.reboot()
	print('Canceled reboot')
end

term.clear()
term.setCursorPos(1,1)
print('Hacked CraftOS 1.3')
if fs.exists('startup') then
	write('Load startup? (Y|N)')
	local valid = false
	while not valid do
		local ev,p1 = coroutine.yield('char')
		if ev == 'char' and p1 == 'y' then
			valid = true
			print('')
			os.run({},'startup')
		elseif  ev == 'char' and p1 == 'n' then
			valid = true
			print('')
		end
	end
end
coroutine.yield = yield