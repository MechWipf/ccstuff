local tArgs = {...}
local sDlPath = 'http://dl.dropboxusercontent.com/u/17040958/ComputerCraft/'
local tProgs = {}
local tFilter = {}
local bForce = false
local sPath = ''

if not von then
	if not os.loadAPI('apis/von') then
		error( 'Cant detect vON. Quitting program.' )
	end
end

local tOldIndex = {}
local tIndex = nil
local sName = nil



function printTable(t, tabs)
    if not t then return end
    if not tabs then tabs = 0 end

    for k,v in pairs(t) do
        local data = string.rep('\t', tabs)..tostring(k)..' = '

        if type(v) == 'table' and tabs < 10 then
            print(data..'{')
            printTable(v, tabs+1)
            print(string.rep('\t', tabs)..'}')
        else
            print(data..tostring(v))
        end
    end
end

function split( self, inSplitPattern, outResults )
	if not outResults then
		outResults = { }
	end

	local theStart = 1
	local theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )

	while theSplitStart do
		table.insert( outResults, string.sub( self, theStart, theSplitStart-1 ) )
		theStart = theSplitEnd + 1
		theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
	end

	table.insert( outResults, string.sub( self, theStart ) )

	return outResults
end

local function getIndex( _path )
	local sPath = sDlPath .. _path

	http.request( sPath .. '/index.csv' )
	local file = http.get( sPath .. '/index.csv' )

	if file then
		return file.readAll(), sPath
	else
		error( 'Cant find a directory named "' .. _path .. '"!' )
	end
end

local function parse( _t )
	local bQuote = false;
	local sVar = ''
	local tTable = {}

	for k,v in pairs(_t) do
		if string.match( v, '^".*' ) and not bQuote then
			bQuote = true
			sVar = v
		elseif bQuote then
			sVar = sVar..' '..v
			if string.match( v, '.*"' ) then
				bQuote = false
				tTable[#tTable+1] = string.gsub( sVar, '(")', function(s) return '' end )
			end
		else
			tTable[#tTable+1] = v
		end
	end
	return tTable
end

local function parseIndex( _s )
	local _t = {}
	local sName = nil

	for k,v in ipairs( split( _s, '\n' ) ) do
		local splt = split( v, '[;$]' )

		if not sName then
			sName = splt[1]
		else
			local idx = splt[1]
			table.remove(splt, 1)
			_t[idx] = splt
		end
	end

	return _t, sName
end

local function unwrapPath( str )
	return string.match( str, '^(.*/)([^/]*)$' )
end

local function updateLocal( name, index )
	if not fs.exists( 'appdata' ) then fs.makeDir( 'appdata' ) end
	if not fs.exists( 'appdata/updater' ) then fs.makeDir( 'appdata/updater' ) end

	local file = fs.open( 'appdata/updater/' .. name, 'w' )
	file.write( von.serialize( index ) )
	file.close()
end

local function updateNeeded( sFile )
	if not tOldIndex then return true end
	if not tOldIndex[sFile] then return true end
	if tOldIndex[sFile][3] < tIndex[sFile][3] then return true end

	return bForce
end

local function update( _dlPath, _filePath )
	local filePath = sDlPath .. '/' .. _filePath


	http.request( filePath )
	local httpFile = http.get( filePath )

	if not httpFile then print( 'File not found.' ) return false end

	local _dir = unwrapPath( _filePath )

	if not fs.exists( _dir ) then fs.makeDir( shell.resolve( _dir ) ) end

	local file = fs.open( _dir .. '/' .. tIndex[_filePath][1], 'w' )
	file.write( httpFile.readAll() )
	file.close()

	print( ' rev' .. ( tOldIndex[_filePath] and tOldIndex[_filePath][3] or 0) .. ' to rev' .. tIndex[_filePath][3] )

	tOldIndex[_filePath] = tIndex[_filePath]
end

for k,v in pairs( parse( tArgs ) ) do
	if v == '-f' then
		bForce = true
	elseif string.find( v, '-p:' ) then
		local data = string.match( v, '%-p%:(.+)' )
		string.gsub( data, '(.-);+', function(s) tFilter[s] = 1  end )
	elseif string.find( v, '-path:' ) then
		sPath = string.match( v, '%-path%:(.+)' )
		print( '"', sPath, '"' )
	end
end

if not sPath then error( 'You need to define a path with -path:<path>' ) end

tIndex, sDlPath = getIndex( sPath )
tIndex, sName = parseIndex( tIndex )

if fs.exists( 'appdata/updater/' .. sName ) then
	local file = fs.open( 'appdata/updater/' .. sName, 'r' )
	tOldIndex = von.deserialize( file.readAll() )
	file.close()
end

if not tIndex then error( 'Something went wrong. Check you index file!' ) end

for sFile, tFile in pairs( tIndex ) do
	if ( #tFilter > 0 and tFilter[tIndex[sFile][1]] ) or #tFilter == 0 then
		if updateNeeded( sFile ) then
			write( 'Updating: ' .. tIndex[sFile][1] )
			update( sDlPath, sFile )
		else
			print( 'UpToDate: ' .. tIndex[sFile][1] )
		end
	else
		print( 'Skipping: ' .. tIndex[sFile][1] )
	end
end

updateLocal( sName, tOldIndex )
print( 'Finished.' )
