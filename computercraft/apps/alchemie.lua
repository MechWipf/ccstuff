local tArgs = {...}
local mSide = ""
local zutaten = 0
local anzahl = 1
local menu = {}
local selected = 1
local startAt = 8
local l = {}
os.pullEvent = os.pullEventRaw

if #tArgs > 0 then
	print("Ohne Argumente Starten!")
	print("Einzelheiten werden im Programm eingestellt.")
	return
end

for k,v in pairs(rs.getSides()) do
	if peripheral.getType(v) == "monitor" then mSide = v end
end
local monitor = peripheral.wrap( mSide )

local function out(str)
	rs.setBundledOutput("back",tonumber(str,2))
end

local function printStatus(n,x,y)
		for k = 1, n, 1 do
			term.setCursorPos(x,y)
			local str = math.floor((k/30)*100).."%"
			local len = string.len(str)
			write(string.rep("0",math.max(4-len,0))..str)
			sleep(1)
		end
end

local function brauenS()
	for i = 18, 2, -1 do term.setCursorPos(1,i) term.clearLine() end
	print("")
	print("Bitte Warten...")
	term.redirect(monitor)
	local i = 0
	while i < anzahl  do
		term.setCursorPos(1,3)
		write("Arbeitet...")
		write("Flaschen: "..i.."/"..anzahl)
		local s = 0
		while s < 3 and i < anzahl do
			s = s + 1
			i = i + 1
			out("01000") sleep(0.5)
			out("00000") sleep(0.5)
		end
		term.setCursorPos(1,5)
		write("Pase 1 von "..zutaten..".")
		out("00001") sleep(0.5)
		out("00000") printStatus(30,13,3)
		if zutaten > 1 then
			term.setCursorPos(1,5)
			write("Phase 2 von "..zutaten..".")
			out("00010") sleep(0.5)
			out("00000") printStatus(30,13,3)
			if zutaten > 2 then
				term.setCursorPos(1,5)
				write("Phase 3 von "..zutaten..".")
				out("00100") sleep(0.5)
				out("00000") printStatus(30,13,3)
			end
		end
		out("10000") sleep(0.5)
		out("00000") sleep(0.5)
		out("10000") sleep(0.5)
		out("00000") sleep(0.5)
		out("10000") sleep(0.5)
		out("00000") sleep(0.5)
	end
	term.restore()
	l.start()
end

local function brauen()
	for i = 18, 2,-1 do term.setCursorPos(1,i) term.clearLine() end
	print("")
	if zutaten == 0 then
		startAt = 3
		write("W�hle die Menge an Zutaten: ")
		menu = {
			{"Eine Zutat",function() zutaten = 1 brauen() end},
			{"Zwei Zutaten",function() zutaten = 2 brauen() end},
			{"Drei Zutaten",function() zutaten = 3 brauen() end},
			{"Abbrechen",function() l.start() end}
		}
	else
		startAt = 3
		write("Anzahl: ")
		write(anzahl)
		menu = {
			{"+ 1",function() anzahl = math.min(anzahl+1,54) brauen() end},
			{"+ 5",function() anzahl = math.min(anzahl+5,54) brauen() end},
			{"+ 10",function() anzahl = math.min(anzahl+10,54) brauen() end},
			{"Max",function() anzahl = 54 brauen() end},
			{"- 1",function() anzahl = math.max(anzahl-1,1) brauen() end},
			{"- 5",function() anzahl = math.max(anzahl-5,1) brauen() end},
			{"- 10",function() anzahl = math.max(anzahl-10,1) brauen() end},
			{"Min",function() anzahl = 1 brauen() end},
			{"Start",function() brauenS() end},
			{"Abbrechen",function() l.start() end}
		}
	end
end

function l.start()
	term.clear()
	term.setCursorPos(1,1)
	print("#- Willkommen beim Alchemie Assistenten v1.1 -#")
	print("")
	print("Dieses Programm kann automatisch aus einer Auswahl von 3 Zutaten Tr�nke brauen und Lagern.")
	print("Man kann bis zu 54 Tr�nke brauen bevor das Lager voll ist.")
	term.redirect(monitor)
	term.clear()
	term.setCursorPos(1,1)
	print("Alchemie Assistent v1.1")
	term.restore()
	term.setCursorPos(1,8)
	print("W�hle Aktion: ")
	menu = { {"Konfiguriere Brauen",brauen}, {"Beenden",l.shutdown} }
	selected = 1
	zutaten = 0
	startAt = 8
end


function l.shutdown()
	term.clear()
	term.setCursorPos(1,1)
	error()
end

l.start()
while true do
	sleep(0.1)
	
	local y = 0
	
	for k,v in pairs(menu) do
		y = y + 1
		local str = menu[k][1]
		term.setCursorPos(1,startAt+(y))
		term.clearLine()
		if y == selected then write(" [") else write("  ") end
		write(str)
		if y == selected then write("]") end
	end
	
	event, param = os.pullEvent()
	if event == "key" then
		if param == 200 then --up
			selected = math.max(selected - 1,1)
		elseif param == 208 then --down
			selected = math.min(selected + 1,#menu)
		elseif param == 28 then --enter
			menu[selected][2]()
		end
	end

end