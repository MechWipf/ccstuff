local function clamp(n,min,max)
	if n > max then n = max end
	if n < min then n = min end
	return n
end

local function detect(n)
	if n == 1 then
		return turtle.detectUp()
	else
		return turtle.detectDown()
	end
end

local function dig(n)
	if n == 1 then
		while turtle.digUp() do sleep(0.5) end
	else
		while turtle.digDown() do sleep(0.5) end
	end
end

local function move(n)
	if n == 1 then
		return turtle.up()
	else
		return turtle.down()
	end
end

local function place(n)
	if n == 1 then
		return turtle.placeUp()
	else
		return turtle.placeDown()
	end
end

local tArgs = { ... }
if #tArgs ~= 1 then
	print("Usage: ladder <length>")
	return
end

local Mining = true
local Length = clamp(tonumber(tArgs[1]),-63,63)
local CLen = 0
local Dir = 1
if Length < 0 then Dir = -1 end

print("Fill slot1 with any block and slot2 with ladders")
print("Dir = "..Dir)
io.read()

turtle.select(1)
while Mining do
	if detect(Dir) then dig(Dir) end
	if detect(Dir) then
		print("Abort Mining. Start ladder building.")
		Mining = false
	elseif CLen == math.abs(Length) then
		print("Finished Mining. Start ladder building.")
		Mining = false
	else
		move(Dir)
		if not turtle.detect() then turtle.place() end
		CLen = CLen + 1
	end
end

turtle.select(2)
for i = 1, CLen, 1 do
	move(-Dir)
	place(Dir)
end

print("Finished building.")