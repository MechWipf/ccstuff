local farm = {}
farm.lastPos = vector.new(0,0,0)
farm.pos = vector.new(0,0,0)
farm.ang = 0

local vecnull = vector.new(0,0,0)

local function toRad(ang)
	return ang * (math.pi / 180)
end

local function rotated(self,phi)
	local c, s = math.cos(phi), math.sin(phi)
	return vector.new(c * self.x - s * self.y, s * self.x + c * self.y,self.z)
end

local function clamp(n0,n1,n2)
	if n0 > n2 then n0 = n1 end
	if n0 < n1 then n0 = n2 end
	return n0
end

local function moveFW()
	local v = turtle.forward()
	farm.pos = farm.pos:add(v and rotated(vector.new(1,0,0),toRad(farm.ang)):round() or vecnull)
	return v, farm.pos
end

local function moveBW()
	local v = turtle.back()
	farm.pos = farm.pos:sub(v and rotated(vector.new(1,0,0),toRad(farm.ang)):round() or vecnull)
	return v, farm.pos
end

local function moveUP()
	local v = turtle.up()
	farm.pos = farm.pos:add(v and vector.new(0,0,1) or vecnull)
	return v, farm.pos
end

local function moveDW()
	local v = turtle.down()
	farm.pos = farm.pos:sub(v and vector.new(0,0,1) or vecnull)
	return v, farm.pos
end

local function turnR()
	turtle.turnRight()
	farm.ang = farm.ang + 90
end

local function turnL()
	turtle.turnLeft()
	farm.ang = farm.ang - 90
end

function setPlantHight(n)
	farm.plantHight = n
end

function run()
	farm.first = true
	turtle.select(1)
	turnR()
	while not turtle.compare() do
		if farm.first == false then moveFW() end
		farm.first = false
		if turtle.compareDown() then
			farm.lastPos = farm.pos
			turnL()
			while farm.pos.z < farm.plantHight do moveUP() end
			while not turtle.compare() do
				moveFW()
				while turtle.detectDown() and farm.pos.z > 1 do turtle.digDown() moveDW() end
				turtle.digDown()
				while farm.pos.z < farm.plantHight do moveUP() end
			end
			while farm.pos.x > 0 do moveBW() end
			while farm.pos.z > 0 do moveDW() end
			turnR()
		end
	end
	while farm.pos.y > 0 do moveBW() end
	turnR()
	for i = 2, 9, 1 do
		turtle.select(i)
		if turtle.getItemCount(i) > 0 then turtle.drop() end
	end
	turnL()
	turnL()
end