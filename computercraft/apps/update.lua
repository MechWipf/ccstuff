local hPath = "http://dl.dropbox.com/u/17040958/ComputerCraft/Computer/"
local F_Version = 0
local L_Version = 0
local tArgs = { ... }

local function split(self,sep)
        local sep, fields = sep or ":", {}
        local pattern = string.format("([^%s]+)", sep)
        self:gsub(pattern, function(c) fields[#fields+1] = c end)
        return fields
end

local function httpRequest(url)
	http.request(url)
	return http.get(url)
end

local function getOnlineVersion()
--	local file = httpRequest(hPath.."version.txt")
--	local ret = tonumber(file:readAll())
--	file.close()
	return 1
end

local function updateLVersion()
	local file = fs.open("programs/version","w")
	file.write(F_Version.."\n")
	file.close()
end

local function update()
	local file = {}
	
	if fs.exists("programs") then fs.delete("programs") end
	fs.makeDir("programs")
	fs.makeDir("programs/apis")
	
	file = httpRequest(hPath.."index.txt")
	local Progs = split(file.readAll(),"\n")
	file.close()

	for n,v in pairs(Progs) do
		print("Updating "..v)
		file = httpRequest(hPath..v)
		if file then
			local filel = fs.open("programs/"..v,"w")
			filel.write(file:readAll())
			filel.close()
			file.close()
		else
			print("Not updated...")
		end
	end
	
	updateLVersion()

end

if turtle then print("Sorry, only updates for Computer!") return end
print("Checking for Updates.")
F_Version = getOnlineVersion()

if fs.exists("programs/version") then
	local file = fs.open("programs/version","r")
	L_Version = tonumber(file.readAll()) or 0
	file.close()
	print("Online Version: " .. F_Version)
	print("Local Version: " .. L_Version)
end

if tArgs[1] == "-f" then
	print("Force update")
elseif L_Version >= F_Version then
	print("No update available.")
	return
end

textutils.slowPrint("Updating...")
update()