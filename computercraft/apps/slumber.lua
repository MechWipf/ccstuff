
if turtle.detect() then
	local H = 0
	print("Starte abbau.")
	turtle.dig()
	turtle.forward()
	while turtle.detectUp() do
		turtle.digUp()
		turtle.up()
		H = H + 1
	end
	for i = 1, H, 1 do
		turtle.down()
	end
	print("Fertig mit Abbauen.")
	return
else
	print("Muss vor Baum stehen")
	return
end