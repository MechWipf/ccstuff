-- Initialization --

-- Includes
local _R = getfenv()
local    term,    colors,    os,    parallel,    modularUI =
      _R.term, _R.colors, _R.os, _R.parallel, _R.modularUI

local event_handler = require "vendor.event"()
local modularUIDom = require "modularUI.dom"

do
  if not modularUI then
    modularUI = require "modularUI"

    local path = _G.API_PATH .. "modularUI/widgets/"
    modularUI.loadWidget( path .. "base" )
  end

  modularUIDom.widgets = modularUI.widgets
end

local appID = "appnamehere_" .. math.random(100000, 999999)
-- -------------- --

-- Build Form --
local function loadForm ()
end
-- ---------- --

-- The Program --
local function program ()

end
-- ----------- --

-- Run --
local function main ()
  loadForm()

  parallel.waitForAny(
    function ()
      event_handler:mainLoop()
    end,
    function ()
      modularUI.mainLoop( event_handler )
    end,
    function ()
      program()
    end,
    function ()
      os.pullEvent( "_exit" .. appID )
    end
  )

  term.setBackgroundColor(colors.black)
  term.clear()
  term.setCursorPos( 1, 1 )
end

print(pcall( main ))
-- --- --
