-- ****************************** --
-- User: MechWipf
-- Date: 14.07.2015
-- Time: 19:48
-- ****************************** --

if not vfs then
    error "No vfs found!"
end

local function gsplit(s,sep)
	local lasti, done, g = 1, false, s:gmatch('(.-)'..sep..'()')
	return function()
		if done then return end
		local v,i = g()
		if s == '' or sep == '' then done = true return s end
		if v == nil then done = true return s:sub(lasti) end
		lasti = i
		return v
	end
end

local fs_httpx = {
  index = {},
  isReadOnly = function () return true end,

  exists = function ( self, path )
    if path == "/" then return true end

    local seg = self:getSegment( path )
    if not seg then return false end

    return true
  end,

  open = function ( self, path, mode )
    if mode ~= "r" then return nil, "HTTP FS only supports text reading!" end

    local remotePath = self:getRemotePath( path )
    if not remotePath then return nil, "File not found: " .. path or "" end

    return http.get( remotePath )
  end,

  list = function ( self, path )
    local seg = self:getSegment( path )
    if not seg or type(seg) ~= "table" then return {} end

    local list = {}
    for k, _ in pairs( seg ) do
      list[#list+1] = k
    end

    return list
  end,

  isDir = function ( self, path )
    if path == "/" then return true end

    local seg = self:getSegment( path )
    if type(seg) == "table" then return true end

    return false
  end,

  getDrive = function ( self ) return self.base end,
  find = function ( self, path ) return { path } end,

  getDir = function ( self, path )
    return path:gmatch("(.*/)")()
  end,

  refresh = function ( self )
    local h = http.get( self.tree )
    if not h then error "Tree file not found." end
    local c = h.readAll()

    self.index = von.deserialize( c )
  end,

  getRemotePath = function ( self, path )
    local seg = self:getSegment( path )

    if type(seg) == "string" then
      return self.base .. seg
    end

    return nil
  end,

  getSegment = function ( self, path )
    local parent = { [self.index] = nil }
    local t = self.index

    for part in gsplit( path:gsub("^/", ""), "/" ) do
      if part == ".." then
        t = parent[t]
        if not t then return nil end
      elseif part ~= "." and part ~= "" then
        local newT = t[part]
        if not newT then return nil end
        parent[newT], t = t, newT
      end
    end

    return t
  end,


}

vfs.registerFS( "httpx", function ( mp, opts )
  local t = { base = opts.base, tree = opts.tree }
  setmetatable( t, { __index = fs_httpx } )
  t:refresh()
  if not _G.dbg then _G.dbg = t end
  return t
end )
