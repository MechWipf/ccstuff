-- ****************************** --
-- User: MechWipf
-- Date: 18.07.2015
-- Time: 20:34
-- ****************************** --

local von = von or require "vendor.von"
local fs, pcall =
      fs, pcall

local Config = {}

--- Loads a file and parses it as config.
-- It will return the default table if no file was found.
-- @param path [string]
-- @param default [table]
function Config.load ( path, default )
    local ok, ret = pcall( function ()
        if not fs.exists( path ) then
            local f = fs.open( path, "w" )
            f.write( von.serialize( default ) )
            f.close()

            return default
        else
            local f = fs.open( path, "r" )
            local content = f.readAll()
            f.close()

            return von.deserialize( content )
        end
    end )

    if ok then
        return ret
    else
        return ok, ret
    end
end

--- Writes a table as config to the defined path.
-- @param path [string]
-- @param config [table]
-- @return config, error
function Config.write( path, config )
    local ok, ret = pcall( function ()
        local f = fs.open( path, "w" )
        f.write( von.serialize( config ) )
        f.close()

        return config
    end )

    if ok then
        return ret
    else
        return ok, ret
    end
end

function Config.Config ( path, default )
    local conf = {}
    default = default or {}

    local c, err = Config.load( path, default )
    if not c then return false, err end

    conf._str = ""
    conf._raw = c

    local fakeIndex

    local function get ( k )
        return table.deep( c, k )
    end

    local function set ( k, v )
        table.deepWrite( c, k, v )
    end

    local function save ()
        return Config.write( path, c )
    end

    function fakeIndex ( self, k )
        if k == "get" then
            return function () return get( self._str ) end
        elseif k == "set" then
            return function ( v ) return set( self._str, v ) end
        elseif k == "save" then
            return save
        end

        local str = self._str == "" and k or self._str .. "." .. k
        return setmetatable( { _str = str }, { __index = fakeIndex } )
    end

    setmetatable( conf, { __index = fakeIndex } )
    return conf
end

return Config