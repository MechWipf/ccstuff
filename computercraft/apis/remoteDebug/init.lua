local pcall = pcall

local config = require "config"
local json = require "vendor.json"
local config_path = _G.APPDATA_PATH .. "remoteDebug/config.von"

local module = {

  serviceGetDeviceUrl = "http://kiwigaming.de/cc/v1/devices",
  serviceGetDevice = function ( self )
    local handler = http.post( self.serviceGetDeviceUrl, "" )
    if not handler then return {}, "Could not open handler." end
    if handler:getResponseCode() ~= 200 then return {}, "HTTP error." end

    local content = handler:readAll() handler:close()
    if not handler then return false, "Error accessing webservice." end
    local ok, ret = pcall( json.decode, content )
    if not ok then return {}, "Maleformed json data." end

    return ret
  end,

  serviceSendDebugUrl = "http://kiwigaming.de/cc/v1/msgs",
  serviceSendDebug = function ( self, ... )
    if not self.config then return false, "Load config first." end
    if not self.config.deviceKey then return false, "No deviceKey for sending messages." end
    if not self.config.receiver then return false, "No receiver specified." end
    if not select( "#", ... ) then return false, "Cannot send nil data." end


    local data = {
      sourceId = self.config.deviceKey,
      targetPhon = self.config.receiver,
      content = self:prepareData( ... )
    }

    local headers = {
      ["Content-Type"] = "application/json"
    }

    http.request( self.serviceSendDebugUrl, json.encode( data ), headers )
   -- if not handler then return false, "Error accessing webservice." end
   --local content = handler:readAll() handler:close()

    --return content == "ok"
  end,

  prepareData = function ( self, ... )
    local data

    if select( "#", ... ) == 1 and type( select( 1, ... ) ) == "table" then
        data = utils.serialize( {...} )
    else
        local s = {}
        local c = select( "#", ... )
        for i = 1, c do s[#s+1] = tostring( select( i, ... ) ) end

        data = table.concat( s, "\t" )
    end

    return data
  end,

  getConfig = function ( self )
    self.config = config.load( config_path, {} )
  end,

  saveConfig = function ( self )
    config.write( config_path, self.config )
  end,

  getDevice = function ( self )
    if not self.config then
      local ok, err = pcall( self.getConfig, self )
      if not ok then return false, err end
    end

    if not self.config.deviceKey then
      local device, err = self:serviceGetDevice()
      if err then return false, "Can't register device: " .. err  end

      self.config.deviceKey = device.uuid
      self.config.devicePhon = device.phon

      local ok, err = pcall( self.saveConfig, self )
      if not ok then return false, err end
    end

    return true
  end,

  setReceiver = function ( self, receiver )
    if not self.config then self:getConfig() end
    self.config.receiver = receiver
    self:saveConfig()

    return true
  end,

  registerDebugger = function ( self, url )
    local _, err

    _, err = self:getDevice()
    if err then return false, err end

    if not self.config.receiver then return false, "No receiver set." end
    if not debug then return false, "No debug environment." end

    debug.send = function ( ... )
      self:serviceSendDebug( ... )
    end
    debug.enable()

    return true
  end
}

return module