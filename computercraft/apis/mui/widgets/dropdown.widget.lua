-- ****************************** --
-- User: MechWipf
-- Date: 13.10.2016
-- Time: 20:39
-- ****************************** --

widget( "dropdown", widgets.base ) {

    _construct = function ( self, text, list )
        self.text = text
        self.list = list
        self.isActive = false
    end,

    _onClick = function ( self, btn, x, y )
        self.focus = false
    end,

    _onKey = function ( self, key )
        if self.focus then
            if key == keys.enter then
                if self._onClick then self:_onClick( 1, 0, 0 ) end
            elseif key == keys.tab then
                if self.next then
                    self:setFocus( self.next )
                end
            end

            if self.onKey then self:onKey( private[self].protected and nil or key ) end
        end
    end,

    _update = function ( self )
        if self.hover then
            self:invalidate()
        end

        if math.floor( ( os.clock() * 2 ) % 2 ) ~= self.clock then
            self.clock = math.floor( ( os.clock() * 2 ) % 2 )
            self.f = not self.f

            self:invalidate()
        end
    end,

    _draw = function ( self, g )
        local x, y = self:getPos()
        local w, h = self.w, self.h

        if self.isActive then
            g.setBackgroundColor( self:getColor( "button_active" ) )
        else
            g.setBackgroundColor( self:getColor( "button" ) )
        end

        g.setTextColor( self:getColor( "text" ) )

        for i = 0, self.h - 1 do
            g.setCursorPos( x, y + i )
            g.write( (" "):rep(self.w) )
        end

        g.setCursorPos( x + math.floor( w/2 - self.text:len()/2 ) , y )
        local text = ( self.f and self.focus ) and "_" .. self.text:sub( 2 ) or self.text
        g.write( text )
    end,
}
