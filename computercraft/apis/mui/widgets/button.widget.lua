
widget( "button", widgets.base ) {

    _construct = function ( self, text )
        self.text = text
    end,

    _onClick = function ( self, btn, x, y )
        if btn == 0 then
            if self.onClickLeft then self:onClickLeft() end
        elseif btn == 1 then
            if self.onClickRight then self:onClickRight() end
        end

        self.focus = false
    end,

    _onKey = function ( self, key )
        if self.focus then
            if key == keys.enter then
                if self._onClick then self:_onClick( 1, 0, 0 ) end
            elseif key == keys.tab then
                if self.next then
                    self:setFocus( self.next )
                end
            end

            if self.onKey then self:onKey( private[self].protected and nil or key ) end
        end
    end,

    _update = function ( self )
        if self.hover then
            self:invalidate()
        end

        if self.focus and math.floor( ( os.clock() * 2 ) % 2 ) ~= self.clock then
            self.clock = math.floor( ( os.clock() * 2 ) % 2 )
            self.f = not self.f

            self:invalidate()
        end
    end,

    _draw = function ( self, g )
        local x, y = self:getPos()
        local w, h = self.w, self.h

        g.setBackgroundColor( self:getColor( "button" ) )
        g.setTextColor( self:getColor( "text" ) )

        for i = 0, self.h - 1 do
            g.setCursor( x, y + i )
            g.write( (" "):rep(self.w) )
        end

        g.setCursor( x + math.floor( w/2 - self.text:len()/2 ) , y )
        local text = ( self.f and self.focus ) and "_" .. self.text:sub( 2 ) or self.text
        g.write( text )
    end,
}
