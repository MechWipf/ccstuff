widget( "panel", widgets.base ) {

    _draw = function ( self, g )
        local x, y = self:getPos()
        local w, h = self.w, self.h

        g.setBackgroundColor( self:getColor( "background" ) )

        for i = 0, h - 1 do
            g.setCursor( x, y + i )
            g.write( (" "):rep( w) )
        end
    end,
}