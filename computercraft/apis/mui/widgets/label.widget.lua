
widget( "label", widgets.base ) {
    __construct = function ( self, x, y, text )
        self.x = x
        self.y = y
        self.w = text:len()
        self.h = 1

        self.text = text

        self.enabled = true
        self.hidden = false
        self.__redraw = true
        self.colors = defaultColors

        self.child = {}

        if parent and parent.addChild then
            parent:addChild( self )
        end
    end,

    _draw = function ( self, g )
        local x, y = self:getPos()
        local w, h = self.w, self.h

        g.setTextColor( self:getColor( "text" ) )
        g.setBackgroundColor( self:getColor( "background" ) )

        g.setCursor( x, y )
        g.write( self.text )

        if self.text:len() < self.w then
            g.write( (" "):rep( self.w - self.text:len() ) )
        end

        if self._w then
            g.write( (" "):rep( self.w - self.text:len() ) )
            self.w = self._w
            self._w = nil
        end
    end,

    setText = function ( self, text, w )
        self._w = w or text:len()

        if self.w < self._w then
            self.w = self._w
        end

        self.text = text
        self:invalidate()
    end,
}
