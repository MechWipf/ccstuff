local term = require "term"
local gpu = term.gpu()

local modularUI = { widgets = {}, widgetsRaw = {}, loadedWidgets = {} }
local pendingFocusChange
local g = {
  setCursor = term.setCursor,
  getCursor = term.getCursor,

  setCursorBlink = term.setCursorBlink,
  getCursorBlink = term.getCursorBlink,

  clear = term.clear,
  clearLine = term.clearLine,

  write = term.write,

  setTextColor = gpu.setForeground,
  setBackgroundColor = gpu.setBackground,
}

local function widget ( className, inherite )
  return function ( prototype )
    local class_mt = { __index = prototype }

    class_mt.__metatable = prototype

    modularUI.widgetsRaw[className] = prototype
    modularUI.widgets[className] = function ( ... )
      local newInst = setmetatable( { type = className }, class_mt )

      if newInst.__construct then newInst:__construct( ... ) end
      return newInst
    end

    if inherite then
      setmetatable( prototype, { __index = inherite } )
    end
  end

end

local watcher = {}
local defaultColors = {
  background     = 0x696969,
  textBackground = 0x4ac4f1,
  text           = 0xFFFFFF,
  button         = 0x0000FF,
  button_active  = 0x122e8c,
  scroll         = 0xa8c6d7
}

local function killWidget ( self )
  for k, item in ipairs(watcher) do
    if item.widget == self then
      self.isDead = true
      table.remove(watcher, k)
    end
  end
end

modularUI.killWidget = killWidget

function modularUI.loadWidget ( path, silent, force )
  if not path then return end

  if not force and modularUI.loadedWidgets[path] then return end

  if not silent then
    print( "loading " .. path:gsub( "(.*/)", "" ) )
  end

  local f = io.open( path .. ".widget.lua", "r" )
  if not f then error "File does not exist." end

  do
    local sandbox_env = setmetatable( {
      killWidget = killWidget,
      widgets = modularUI.widgetsRaw,
      widget = widget,
      defaultColors =
      defaultColors,
      term = term
    }, { __index = _ENV } )

    local chunk, err = load( f:read "*a", "sandbox string", "bt", sandbox_env )
    if err then error( err ) end

    ok, err = pcall( chunk )
    if err then error( err, 2 ) end
  end

  modularUI.loadedWidgets[path] = true
end

function modularUI.loadWidgets ( path )

end

function modularUI.startWatching ( obj, window )
  table.insert( watcher, 1, { widget = obj, window = window or term } )
end

function modularUI.stopWatching ( obj )
  for k, v in pairs( watcher ) do
    if tostring( v ) == tostring( obj ) then
      table.remove( watcher, k )
      -- local str = "{ "
      -- for k,v in pairs( watcher ) do str = str .. " %d = %q, " % { k, tostring( v ) } end
      -- str = str .. " }"
      -- print( str )
    end
  end
end

function modularUI.clearWatchlist ()
  watcher = {}
end

function modularUI.mainLoop ( handler )
  while true do
    modularUI.handlerUpdate()
    modularUI.handlerDraw()
    os.sleep(0.1)
  end
end

function modularUI.handlerDraw ()
  for i = 0, #watcher - 1 do
    local item = watcher[ #watcher - i ]
    if item.widget.__draw and not item.widget._disabled then
      item.widget:__draw( g )
    end
  end
end

function modularUI.handlerUpdate ()
  for i, item in ipairs( watcher ) do
    if item.widget.__update and not item.widget._disabled then item.widget:__update() end
  end

  if pendingFocusChange then
    if not pendingFocusChange[1].focus then
      print( "Invalid focus change." )
      pendingFocusChange = nil
      return
    end

    pendingFocusChange[1].focus = false
    pendingFocusChange[2].focus = true
    pendingFocusChange = nil
  end
end

local click
function modularUI.handlerClick ( btn, x, y )
  local canFocus = true

  for i, item in ipairs( watcher ) do
    if item.widget.__onClick
      and canFocus
      and math.inRange( x, item.widget.x, item.widget.x + item.widget.w )
      and math.inRange( y, item.widget.y, item.widget.y + item.widget.h )
      and not item.widget._disabled
    then
      if i ~= 1 then
        table.remove( watcher, i )
        table.insert( watcher, 1, item )
      end

      item.widget:__onClick( btn, x, y)
      item.widget.focus = true
      canFocus = false
    elseif not item.widget._disabled then
      item.widget.focus = false
    end
  end

  click = { x = x, y = y }
end

function modularUI.handlerDrag ( btn, x, y )
  local item = watcher[1]
  if not item then return end

  if item.widget.__onDrag and not item.widget._disabled then item.widget:__onDrag( btn, click.x, click.y, x, y ) end
end

function modularUI.handlerDrop ( btn, x, y )
  local item = watcher[1]
  if not item then return end

  if item.widget.__onDrag and not item.widget._disabled then item.widget:__onDrag( btn, click.x, click.y, x, y ) end
end

function modularUI.handlerScroll ( x, y, dir )
  local item = watcher[1]
  if not item then return end

  if item.widget.__onScroll and not item.widget._disabled then item.widget:__onScroll( dir, x, y ) end
end

function modularUI.handlerKeyDown ( key )
  local item = watcher[1]
  if not item then return end

  if item.widget.__onKey and not item.widget._disabled then item.widget:__onKey( key ) end
end

function modularUI.handlerKeyUp ( key )
  local item = watcher[1]
  if not item then return end

  if item.widget.__onKey and not item.widget._disabled then item.widget:__onKey( key ) end
end

return modularUI
