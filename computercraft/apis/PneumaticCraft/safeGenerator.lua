local pipeSide = "front"
local wireSide = "right"


local pipe = peripheral.wrap( pipeSide )

while true do
  local f = pipe.getPressure() / pipe.getDangerPressure()

  if f < 0.8 then
    rs.setAnalogOutput( wireSide, 15 )
  else
    rs.setAnalogOutput( wireSide, 0 )
  end

  sleep(1)
end