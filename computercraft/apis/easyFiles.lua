local easyFiles = {}
easyFiles._loaded = {}

local function gsplit(s,sep)
	local lasti, done, g = 1, false, s:gmatch('(.-)'..sep..'()')
	return function()
		if done then return end
		local v,i = g()
		if s == '' or sep == '' then done = true return s end
		if v == nil then done = true return s:sub(lasti) end
		lasti = i
		return v
	end
end

local function getFile ( paths, path )
  local sb = { "module '", path,"' not found:\n" }
  local f

  path = path:gsub( "%.", "/" )
  for p in gsplit( paths, ";" ) do
    p = p:gsub( "?", path )

    f = io.open( p, "r" )
    if f then
      local content = f:read "*a"; f:close()
      return content
    else
      sb[#sb+1] = ("no file '%s'\n"):format( p )
    end
  end

  return nil, table.concat( sb, "" )
end

function easyFiles:require ( path )
	if self._loaded[path] then return self._loaded[path] end

  local content, err = getFile( self.paths, path )
  if err then error( err ) end

	local fn = loadstring( content )()
	self._loaded[path] = fn
  return fn
end

local function loadfileOnline ( _sPath )
	local _s = http.get( _sPath ).readAll()

	if not _s or #_s == 0 then
		return nil, "No such File: " .. _sPath
	end

	return loadstring( _s )
end

function easyFiles:inherite ( paths )
  local ef = easyFiles.new( paths )
  ef.paths = paths .. ";" .. self.paths

  return ef
end

local tAPIsLoading = {}
function easyFiles.loadAPIOnline ( path, name )
	local _G = getfenv(0)
	if tAPIsLoading[name] == true then
		error( "API " .. name .. " is already being loaded" )
		return false
	end
	tAPIsLoading[name] = true

	local tEnv = {}
	setmetatable( tEnv, { __index = _G } )
	local fnAPI, err = loadfileOnline( path )
	if fnAPI then
		setfenv( fnAPI, tEnv )
		fnAPI()
	else
		error( err )
    tAPIsLoading[name] = nil
		return false
	end

	local tAPI = {}
	for k,v in pairs( tEnv ) do
		tAPI[k] =  v
	end

	_G[name] = tAPI
	tAPIsLoading[name] = nil
	return true
end

function easyFiles.new ( paths )
  local ef = { ["paths"] = paths }
  setmetatable( ef, { __index = easyFiles } )

  return ef
end

-- nessecary for loadAPI from CC
local env = getfenv()
do
  local ef = easyFiles.new( "/apis/?.lua;/apis/?/init.lua;?;?.lua" )

  env.require = function ( path )
    return ef:require( path )
  end

  env.getInstance = function ()
    return ef
  end

  env.loadAPIOnline = easyFiles.loadAPIOnline
  env.new = easyFiles.new
	env._loaded = easyFiles._loaded
end

return easyFiles