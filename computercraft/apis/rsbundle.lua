local    colors,    rs,    sleep =
      _G.colors, _G.rs, _G.sleep

local c = {}
local bundle = {}

for _, side in pairs(rs.getSides()) do
  c[side] = {}
  for colorIndex, colorByte in pairs(colors) do
    if type(colorByte) == 'number' then
      c[side][colorIndex] = { false, colorByte }
    end
  end
end

local function setSignals(side)
  local clrList = c[side]
  local outn = 0
  for _,Clr in pairs(clrList) do
    if Clr[1] then
      outn = outn + Clr[2]
    end
  end
  rs.setBundledOutput(side,outn)
end

local function split(sideColor)
  local side, color = sideColor:match "(%a*)%.?(%a*)"
  if not c[side] then return false, "Not a valid side!" end
  if not c[side][color] then return false, "Not a valid color!" end
  return side, color
end

function bundle.set(sideColor,bool)
  local side, color = split(sideColor)
  if not side then error( color ) end
  c[side][color][1] = bool
  setSignals(side)
end

function bundle.toggle(sideColor)
  local side, color = split(sideColor)
  if not side then error( color ) end
  bundle.set(sideColor, not c[side][color][1])
end

function bundle.pulse(sideColor, delay)
  delay = (delay or 0.2)/2
  bundle.toggle( sideColor )
  sleep(delay)
  bundle.toggle( sideColor )
  sleep(delay)
end

function bundle.pulseSingle(sideColor, delay)
  delay = delay or 0.2
  bundle.toggle( sideColor )
  sleep( delay )
  bundle.toggle( sideColor )
end

function bundle.getIn(sideColor)
  local side, color = split(sideColor)
  if not side then error( color ) end
  return colors.test(rs.getBundledInput(side),colors[color])
end

function bundle.getOut(sideColor)
  local side, color = split(sideColor)
  if not side then error( color ) end
  return c[side][color][1]
end

function bundle.clear(side)
  if not c[side] then error "Not a valid side!" end
  for _,Clr in pairs(c[side]) do Clr[1] = false end
  setSignals(side)
end

function bundle.all(side)
  if not c[side] then error "Not a valid side!" end
  for _,Clr in pairs(c[side]) do Clr[1] = true end
  setSignals(side)
end

bundle.split = split

return bundle