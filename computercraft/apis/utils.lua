---
-- Some usefull functions
-- @module utils

local unpack = unpack or table.unpack
local _tostring = tostring
local utils = {}

function utils.tostring ( o )
    local _type = type( o )

    if _type == "string" then
        return o
    elseif _type == "boolean" then
        return o and "True" or "False"
    elseif _type == "function" then
        return "Function"
    else
        local ok, ret = pcall( function () return _tostring( o ) end )
        if not ok then return "_unknown_" end
        return ret
    end
end

--- Serializes a table.
-- @tparam table t
-- @tparam[opt=true] bool closed functional table?
-- @treturn string serialized table
function utils.serialize ( t, closed )
    if closed == nil then closed = true end
    local string_builder = {}
    local parent_table = { [t] = false }
    local tabs = 0
    local key = nil

    if closed then
        tabs = 1
        string_builder[#string_builder+1] = ( "{" )
    end

    while next( t, key ) do
        local value, data
        key, value = next( t, key )

        if type(key) == "number" then
            data = ("%s[%s] = "):format( ("\t"):rep( tabs ), key )
        else
            data = ("%s[%q] = "):format( ("\t"):rep( tabs ), tostring( key ) )
        end

        local _type = type(value)
        if _type == "table" and parent_table[value] == nil then
            string_builder[#string_builder+1] = ( data .. "{ " )
            parent_table[value] = { t, tabs, key }
            t, tabs, key = value, tabs + 1, nil
        elseif _type == "number" then
            string_builder[#string_builder+1] = ( ("%s%s,"):format( data, value ) )
        else
            string_builder[#string_builder+1] = ( ("%s%q,"):format( data, tostring( value ) ) )
        end

        while next( t, key ) == nil and parent_table[t] do
            t, tabs, key = unpack( parent_table[t] )
            string_builder[#string_builder+1] = ( ("\t"):rep( tabs ) .. "}," )
        end
    end

    if closed then
        string_builder[#string_builder+1] = ( "}" )
    end

    return table.concat( string_builder, "\n" )
end

function utils.serialize2 ( t, closed )
    if closed == nil then closed = true end
    local string_builder = {}
    local parent_table = { [t] = false }
    local tabs = 0
    local key = nil

    if closed then
        tabs = 1
        string_builder[#string_builder+1] = ( "{" )
    end

    local value, data
    while next( t, key ) do
        local loop = function()
            key, value = next( t, key )

            print( "log", tostring(key), tostring(value) )
            sleep(.1)

            if type(key) == "number" then
                data = ("%s[%s] = "):format( ("\t"):rep( tabs ), key )
            else
                data = ("%s[%q] = "):format( ("\t"):rep( tabs ), tostring( key ) )
            end

            local _type = type(value)
            if _type == "table" and parent_table[value] == nil then
                string_builder[#string_builder+1] = ( data .. "{ " )
                parent_table[value] = { t, tabs, key }
                t, tabs, key = value, tabs + 1, nil
            elseif _type == "number" then
                string_builder[#string_builder+1] = ( ("%s%s,"):format( data, value ) )
            else
                string_builder[#string_builder+1] = ( ("%s%q,"):format( data, tostring( value ) ) )
            end

            while next( t, key ) == nil and parent_table[t] do
                t, tabs, key = unpack( parent_table[t] )
                string_builder[#string_builder+1] = ( ("\t"):rep( tabs ) .. "}," )
            end
        end

        local ok, err = utils.trace( loop )
        if not ok then debug.log( err ) end
    end

    if closed then
        string_builder[#string_builder+1] = ( "}" )
    end

    return table.concat( string_builder, "\n" )
end

--- Serializes and prints a table
-- @tparam table t
function utils.printTable( t )
    print( utils.serialize( t, false ) )
end

--- Shortcut for os.run without env
-- @tparam string path to file
-- @param[opt] ... arguments to pass to the file
function utils.run ( path, ... )
    os.run( {}, path, ... )
end

--- Decodes a nbt table to a more readable lua table
-- @tparam table t in table
-- @treturn table out table
function utils.nbtToTable ( t )
    if not t then return end

    local out = {}
    local tagType = t.type

    if tagType == "NBTTagCompound" then
        for _, v in pairs( t.keys ) do
            out[v] = utils.nbtToTable( t.value[v] )
        end
    elseif tagType == "NBTTagList" then
        for _, v in pairs( t.value ) do
            out[ #out + 1 ] = utils.nbtToTable( v )
        end
    elseif tagType:match( "NBTTag.+" ) then
        return t.value
    end

    return out
end

--- Converts a UUID into id and meta
-- @tparam number uuid number to convert
-- @treturn[1] int id
-- @treturn[1] int meta
function utils.getID(uuid)
    local meta
    local id

    if uuid > 32768 then
        meta = uuid % 32768
        id = uuid - meta * 32768
    else
        id = uuid
        meta = 0
    end

    return id, meta
end

--- Converts id and meta into a UUID
-- @tparam int id
-- @tparam int meta
-- @treturn int uuid
function utils.getUUID( id, meta )
    return id + meta * 32768
end

--- Gets all devices or a subset based on filter
-- @tparam[opt] string sFilter a device name (without the '_number' part)
-- @treturn table list of all found devices
function utils.getDevices( sFilter )
    local tDevices = {}

    for _, sName in pairs(peripheral.getNames()) do
        local name = sName:gsub("^(.+)_[0-9]*", function(s) return s end)

        if peripheral.getType(name) then
            name = peripheral.getType(name)
        end

        if not tDevices[name] then
            tDevices[name] = {}
        end
        tDevices[name][#tDevices[name]+1] = peripheral.wrap(sName)
    end

    if sFilter then
        return tDevices[sFilter]
    else
        return tDevices
    end
end

local math = math
--- Addition to the math library
-- @tparam number n
-- @tparam number min
-- @tparam number max
-- @treturn bool is n in range of min and max?
function math.inRange( n, min, max )
    return n <= max and n >= min
end

--- Addition to the math library
-- @tparam number n
-- @tparam number min
-- @tparam number max
function math.clamp ( n, min, max )
    if n > max then return max end
    if n < min then return min end

    return n
end

--- Addition to the math library
-- @tparam table points list of points: `{{x,y},...}`
function math.linearRegression ( points )
    local n = #points

    local xsum  = 0
    local x2sum = 0
    local ysum  = 0
    local xysum = 0

    for i = 1, n, 1 do
        xsum  = xsum  + points[i][1]
        ysum  = ysum  + points[i][2]
        x2sum = x2sum + points[i][1]^2
        xysum = xysum + points[i][1] * points[i][2]
    end

    local a = ( n * xysum - xsum * ysum ) / ( n * x2sum - xsum^2 )
    local b = ( x2sum * ysum - xsum*xysum ) / ( x2sum * n - xsum^2 )

    return a, b
end

function utils.format ( a, b )
    if type( b ) == "table" then
        for k, _ in pairs( b ) do b[k] = tostring( b[k] )  end
        return a:format( unpack( b ) )
    end

    return a:format( tostring( b ) )
end

function utils.gsplit(s,sep)
	local lasti, done, g = 1, false, s:gmatch('(.-)'..sep..'()')
	return function()
		if done then return end
		local v,i = g()
		if s == '' or sep == '' then done = true return s end
		if v == nil then done = true return s:sub(lasti) end
		lasti = i
		return v
	end
end

function utils.explode ( s, sep )
    local r = {}
    for p in gsplit( s, sep ) do
        r[#r+1] = p
    end
    return r
end

s_add = function ( a, b ) -- the + operator
    return a .. tostring( b )
end

s_sub = function ( a, b ) -- the - operator
    return a:gsub( b, "" )
end

s_mul = function ( a, b ) -- the * operator
    return a:rep( b )
end

s_mod = function ( a, b )
    if type( b ) == "table" then
        for k, _ in pairs( b ) do b[k] = tostring( b[k] )  end
        return a:format( unpack( b ) )
    end

    return a:format( tostring( b ) )
end

-- if you have string.explode (check out the String exploding snippet) you can also add this:
s_div = function ( a, b ) -- the / operator
    return a:explode( b )
end

function _G.table:foreach ( callback )
    local k, v = next( self )
    while k do
        callback( k, v )

        k,v = next( self )
    end
end

local function deep ( self, path, write )
    local parent = nil
    local t = self

    local _part
    for part in utils.gsplit( path:gsub("^%.", ""), "%." ) do
        _part = part
        local newT = t[part]

        if not newT and not write then
            return nil
        elseif write then
            t[part] = {}
            newT = t[part]
        end

        parent, t = t, newT
    end

    if write then
        return t, parent, _part
    else
        return t
    end
end

function _G.table.deep ( self, path )
    return deep( self, path, false )
end

function _G.table.deepWrite ( self, path, value )
    local _, t, part = deep( self, path, true )
    t[part] = value
end


do
    local debug = {}
    local os = os

    debug._enabled = false

    function debug.enable ()
        debug._enabled = true
    end

    function debug.disable ()
        debug._enabled = false
    end

    function debug.isEnabled ()
        return debug._enabled
    end

    function debug.send () end

    function debug.hold ( id )
        os.pullEvent( "_debug_resume" .. ( id or "" ) )
    end

    function debug.resume ( id )
        os.pushEvent( "_debug_resume" .. ( id or "" ) )
    end

    local function nativeWriteLog ( ... )
        local log = io.open( "stdout.log", "a" )
        local args = {...}

        if #args == 1 and type(args[1]) == "table" then
            log:write( utils.serialize( args[1] ) .. "\n", false )
        else
            log:write( table.concat( {...}, "\t" ) .. "\n" )
            log:close()
        end
    end

    local fs = fs
    function debug.writeLog( ... )
        if debug and debug.isEnabled() then
            debug.send( ... )
            return nil
        end

        if not fs.exists( "stdout.log" ) then
            local log = io.open( "stdout.log", "w" )
            log:write("")
            log:close()
        end

        nativeWriteLog( ... )
    end
    debug.log = debug.writeLog

    debug.lastError = ""
    function utils.getLastError ()
        return debug.lastError
    end

    _G.debug = debug
end

function utils.pcallLog ( msg, func, ... )
    local args = {...}
    local ret = { utils.trace( function () func(unpack(args)) end ) }
    if not ret[1] then
        debug.lastError = msg:format( ret[2] )
        debug.writeLog( debug.lastError )
    end

    return unpack( ret )
end

function utils.enableLPNetworking ()

    os.pullEventRaw = function ( sFilter )
        local eventData

        while true do
            eventData = { coroutine.yield( sFilter ) }

            if not sFilter and ( eventData[1] == "LP_MESSAGE" or eventData[1] == "LP_BROADCAST" ) then
                local package = eventData[1] == "LP_BROADCAST" and textutils.unserialize( eventData[3] ) or eventData[3]

                local newEvent = {
                    "modem_message",
                    "lpmodem",
                    package.nChannel,
                    package.nReplyChannel,
                    package.tMessage,
                }

                _G.os.queueEvent( unpack( newEvent ) )
            else
                break
            end
        end

        --writeLog( "event: %s" % ( textutils.serialize( eventData ) - "[\r\n]" ) )
        return unpack( eventData )
    end

end

function utils.fileRead ( path )
    local f = io.open( path, "r" )
    if not f then error "File not found" end
    local content = f:read "*a" f:close()
    return content
end

function utils.fileWrite( path, content )
    local f = io.open( path, "w" )
    f:write( content )
    f:close()
end

function utils.fileWriteStream( path, callback )
    local f = io.open( path, "w" )
    while true do
        local content = callback()
        if content == false then break end
        f:write( content )
    end
    f:close()
end

function utils.binaryRead ( path )
  local f = io.open( path, "rb" )
  if not f then error "File not found" end
  local content = {}
  local b = f:read()
  while b do
    content[#content+1] = b
    b = f:read()
  end
  return content
end

function utils.binaryWrite ( path, content )
  local f = io.open( path, "wb" )
  for _, b in pairs( content ) do
    f:write( b )
  end
  f:close()
end

function utils.tostring ( v )
    if type( v ) == "table" then
        return utils.serialize( v )
    else
        return _tostring( v )
    end
end
-- _G.tostring = utils.tostring

function utils.reduceNum ( x, t, s )
    t = t or { " ", "k", "M", "G", "T", "P", "E", "Z", "Y" }
    s = s or 1000

    local i = 1
    while math.abs(x) > s^2 do
        x = x / s
        i = i + 1
    end

    return x, t[i]
end

function utils.reduceNumX ( x, t )
    if not t or not x then error "null argument" end

    local i = 1
    while t[i+1] and math.abs(x) > t[i+1][2]*2 or false do
        x = x / t[i+1][2]
        i = i + 1
    end

    return x, t[i][1]
end

do
    if not class then pcall( dofile, "db/apis/class.lua" ) end
    if _G.class then
        local table = table

        _G.class "breadcrumb" {
          __construct = function ( self, str )
              local split = utils.explode( str, "%." )
              for _,v in pairs(split) do self:add( v ) end
          end,

          add        = function ( self, str ) self[#self+1] = str end,
          back       = function ( self      ) table.remove( self, #self ) end,
          check      = function ( self, str ) return table.concat( self, "." ) == str end,
          last       = function ( self      ) return self[#self] end,
          __tostring = function ( self      ) return table.concat( self, "." ) end
        }
    end
end

function utils.toHex ( str )
    local s = str:gsub( ".", function (c)
        return string.format( "%02x", c:byte() )
    end )
    return s
end

function utils.fromHex ( str )
    local s = str:gsub( "..", function (c)
        return string.char( tonumber( c, 16 ) )
    end )
    return s
end

function utils.loadHex ( str )
    str = utils.fromHex( str )

    return loadstring( str )
end

function utils.createBinaryRunner ( chunk )
    local runner = "loadstring(({(%q):gsub('..',function (c) return string.char(tonumber(c,16)) end)})[1])()"
    local data = utils.toHex( string.dump( chunk, true ) )

    return runner:format( data )
end

function utils.unixTime ( t )
    t = t or { day = os.day(), time = os.time() }
    return ( t.day * 24000 + t.time ) * 1000
end

function utils.trace ( fn, ... )
    local args = {...}
    local sb = {}
    local ret = xpcall(function() return fn(unpack(args)) end, function(err)
        local trace = {}
        local i, hitEnd, _, e = 4, false
        repeat
            _, e = pcall(function() error("<tracemarker>", i) end)
            i = i + 1
            if e == "xpcall: <tracemarker>" then
                hitEnd = true
                break
            end
            table.insert(trace, e)
        until i > 10
        table.remove(trace)
        if err:match("^" .. trace[1]:match("^(.-:%d+)")) then table.remove(trace, 1) end
        sb[#sb+1] = ("\nProgram has crashed! Stack trace:")
        sb[#sb+1] = (err)
        for i, v in ipairs(trace) do
            sb[#sb+1] = ("  at " .. v:match("^(.-:%d+)"))
        end
        if not hitEnd then
            sb[#sb+1] = ("  ...")
        end
    end)

    return ret, table.concat( sb, "\n" )
end

local _launch
function utils.launch ( prog )
    if _launch then error "You can not launch twice" end
    local shutdown = os.shutdown
    if not prog then prog = "rom/programs/advanced/multishell" end

    os.shutdown = function ()
        os.run( {}, prog )
        shutdown()
    end

    os.queueEvent( "modem_message", {} )
    os.queueEvent( "key", 11 )
    _launch = true
end

function utils.curl ( url )
    local handler = http.get( url )
    if handler then
        return handler.readAll(), nil
    end

    return nil, "Error"
end

function utils.isLaunched ()
    return _launch
end

if getfenv then
    local _R = getfenv()
    for name, fun in pairs(utils) do
        _R[name] = fun
    end
end

return utils