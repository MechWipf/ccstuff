-- ****************************** --
-- User: MechWipf
-- Date: 27.06.2015
-- Time: 17:12
-- ****************************** --

do
    local g = getfenv(1)

    function g.class(className)
        return function(prototype)
            local metatable = { __index = prototype }
            prototype.__metatable = metatable
            getfenv(1)[className] = function(...)
                local self = setmetatable({}, metatable)
                if self.__construct then self:__construct(...) end
                return self
            end
        end
    end

end

