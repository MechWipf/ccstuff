local _R = getfenv()
local    math,    setmetatable,    ipairs,    pairs,    table,    print,    loadfile,    error,    setfenv,    getfenv,    pcall,    term,    tostring,    sleep,    colors =
      _R.math, _R.setmetatable, _R.ipairs, _R.pairs, _R.table, _R.print, _R.loadfile, _R.error, _R.setfenv, _R.getfenv, _R.pcall, _R.term, _R.tostring, _R.sleep, _R.colors

return function ()
    local modularUI = { widgets = {}, widgetsRaw = {}, jobs = {} }

    local function widget ( className, inherite )

        return function ( prototype )
            local class_mt = { __index = prototype }

            class_mt.__metatable = prototype

            modularUI.widgetsRaw[className] = prototype
            modularUI.widgets[className] = function ( ... )
                local newInst = setmetatable( { type = className }, class_mt )

                if newInst.__construct then newInst:__construct( ... ) end
                return newInst
            end

            if inherite then
                setmetatable( prototype, { __index = inherite } )
            end
        end

    end

    local watcher = {}
    local pendingFocusChange
    local defaultColors = {
        background          = colors.gray,
        textBackground      = colors.cyan,
        text                = colors.white,
        text_focus          = colors.yellow,
        button              = colors.blue,
        button_active       = colors.lightBlue,
        scroll              = colors.lightGray
    }

    local function killWidget ( self )
        for k, item in ipairs(watcher) do
            if item.widget == self then
                table.remove(watcher, k)
            end
        end
    end

    --- Removes a widget.
    -- @name ModularUI.killWidget
    -- @param widget [Widget]
    modularUI.killWidget = killWidget
    modularUI.loadedWidgets = {}

    --- Load a widget template.
    -- @name ModularUI.loadWidget
    -- @param path [string] Absolute path to template file.
    -- @param silent [boolean] When true: don't print the status.
    -- @param force [boolean] When true: reload an already loaded widget.
    -- @usage The file ending will be attached. It only loads .widget.lua files.
    function modularUI.loadWidget ( path, silent, force )
        if not path then return end

        if not force and modularUI.loadedWidgets[path] then return end

        if not silent then
            print( "loading " .. path:gsub( "(.*/)", "" ) )
        end

        local chunk, err = loadfile( path .. ".widget.lua" )
        if err then error( err ) end

        local env = {
            setFocus = function ( self, target ) pendingFocusChange = { self, target } end,
            killWidget = killWidget,
            widgets = modularUI.widgetsRaw,
            widget = widget,
            defaultColors = defaultColors
        }

        if modularUI.env then
            for k,v in pairs( modularUI.env ) do
                env[k] = v
            end
        end

        chunk = setfenv( chunk, setmetatable( env, { __index = getfenv(1) } ) )
        local ok, _ = pcall( chunk )
        if not ok and err then error( err, 2 ) end

        modularUI.loadedWidgets[path] = true
    end

    -- function modularUI.loadWidgets ( path )
    -- end

    function modularUI.patchWindow ( window )
        local mt = setmetatable( {}, { __index = window } )
        local env = setmetatable( {}, { __index = mt } )

        mt.write = function ( text )
            local a_x, a_y, a_w, a_h = env.getArea()
            local x, y = env.getCursorPos()

            if math.inRange( x, a_x, a_w )
                and math.inRange( y, a_y, a_h - 1)
            then
                text = text:sub( 1, a_w - x )
                window.write( text )
            end
        end

        mt.pushArea = function ( x, y, w, h )
            local c = { env.getArea() }

            env.area[#env.area + 1] = {
                math.clamp( x, c[1], c[3] ),
                math.clamp( y, c[2], c[4] ),
                math.clamp( w, c[1], c[3] ),
                math.clamp( h, c[2], c[4] )
            }
        end

        mt.getArea = function ()
            local area = env.area[#env.area]
            return unpack( area )
        end

        mt.popArea = function ()
            local area = env.area[#env.area]

            if #env.area > 2 then
                env.area[#env.area] = nil
            end

            return unpack( area )
        end

        mt.isMon = function ( mon )
            return mon == window
        end

        local w, h = env.getSize()
        env.area = { { 1, 1, w + 1, h + 1 } }

        return env
    end

    --- Start watching a form
    -- @name ModularUI.startWatching
    -- @param obj [Widget] Widget to be watched.
    -- @param window [table] Window table (term or monitor).
    function modularUI.startWatching ( obj, window )
        window = modularUI.patchWindow( window or term )
        table.insert( watcher, 1, { widget = obj, window = window } )
    end

    --- Stop watching a form
    -- @name ModularUI.stopWatching
    -- @param obj [Widget]
    function modularUI.stopWatching ( obj )
        for key, val in pairs( watcher ) do
            if tostring( val ) == tostring( obj ) then
                table.remove( watcher, key )

                local str = "{ "
                for k,v in pairs( watcher ) do str = str .. (" %d = %q, "):format( k, tostring( v ) ) end
                str = str .. " }"
                print( str )
            end
        end
    end

    --- Clears the whole watchlist
    -- @name ModularUI.clearWatchlist
    -- @usage Use with caution. Can corrupt GUI elements.
    function modularUI.clearWatchlist ()
        watcher = {}
    end

    --- Register all events used by the GUI
    -- @name ModularUI.registerEvents
    -- @param handler [Event]
    -- @see Event
    function modularUI.registerEvents ( handler )
        handler:event( "monitor_touch", "modularUI_touch"  , modularUI.handlerTouch  )
        handler:event( "mouse_click"  , "modularUI_click"  , modularUI.handlerClick  )
        handler:event( "mouse_drag"   , "modularUI_drag"   , modularUI.handlerDrag   )
        handler:event( "mouse_scroll" , "modularUI_scroll" , modularUI.handlerScroll )
        handler:event( "key"          , "modularUI_key"    , modularUI.handlerKey    )
        handler:event( "char"         , "modularUI_char"   , modularUI.handlerChar   )
        handler:event( "key_up"       , "modularUI_key_up" , modularUI.handlerKeyUp  )
        handler:event( "paste"        , "modularUI_paste"  , modularUI.handlerPaste  )
    end

    function modularUI.mainLoop ( handler )
        modularUI.inLoop = true
        modularUI.registerEvents( handler )

        local i = 0
        while true do
            if i % 2 == 0 then
                modularUI.handlerDraw()
            end

            modularUI.handlerUpdate()
            for _, job in pairs( modularUI.jobs ) do job() end

            sleep(0.1); i = i +1;
        end
    end

    function modularUI.handlerDraw ()
        for i = 0, #watcher - 1 do
            local item = watcher[ #watcher - i ]
            if item.widget.__draw and not item.widget._disabled then
                item.widget:__draw( item.window )
            end
        end
    end

    function modularUI.handlerUpdate ()
        for _, item in ipairs( watcher ) do
            if item.widget.__update and not item.widget._disabled then item.widget:__update() end
        end

        if pendingFocusChange then
            if not pendingFocusChange[1].focus then
                if debug then debug.log( "Invalid focus change." ) end
                pendingFocusChange = nil
                return
            end

            pendingFocusChange[1].focus = false
            pendingFocusChange[2].focus = true
            pendingFocusChange = nil
        end
    end

    function modularUI.handlerTouch ( _, mon, x, y )
        local canFocus = true

        for i, item in ipairs( watcher ) do
            if item.window.isMon( mon ) then
                if item.widget.__onTouch
                    and canFocus
                    and math.inRange( x, item.widget.x, item.widget.x + item.widget.w )
                    and math.inRange( y, item.widget.y, item.widget.y + item.widget.h )
                    and not item.widget._disabled
                then
                    if i ~= 1 then
                        table.remove( watcher, i )
                        table.insert( watcher, 1, item )
                    end

                    item.widget:__onTouch( x, y )
                    item.widget.focus = true
                    canFocus = false
                elseif not item.widget._disabled then
                    item.widget.focus = false
                end
            end
        end
    end

    local click
    function modularUI.handlerClick ( _, btn, x, y )
        local canFocus = true

        for i, item in ipairs( watcher ) do
            if item.widget.__onClick
                and canFocus
                and math.inRange( x, item.widget.x, item.widget.x + item.widget.w )
                and math.inRange( y, item.widget.y, item.widget.y + item.widget.h )
                and not item.widget._disabled
            then
                if i ~= 1 then
                    table.remove( watcher, i )
                    table.insert( watcher, 1, item )
                end

                item.widget:__onClick( btn, x, y)
                item.widget.focus = true
                canFocus = false
            elseif not item.widget._disabled then
                item.widget.focus = false
            end
        end

        click = { x = x, y = y }
    end

    function modularUI.handlerDrag ( _, btn, x, y )
        local item = watcher[1]
        if not item then return end

        if item.widget.__onDrag and not item.widget._disabled then item.widget:__onDrag( btn, click.x, click.y, x, y ) end
    end

    function modularUI.handlerScroll ( _, dir, x, y )
        local item = watcher[1]
        if not item then return end

        if item.widget.__onScroll and not item.widget._disabled then item.widget:__onScroll( dir, x, y ) end
    end

    function modularUI.handlerKey ( _, key )
        local item = watcher[1]
        if not item then return end

        if item.widget.__onKey and not item.widget._disabled then item.widget:__onKey( key ) end
    end

    function modularUI.handlerKeyUp ( _, key )
        local item = watcher[1]
        if not item then return end

        if item.widget.__onKeyUp and not item.widget._disabled then item.widget:__onKeyUp( key ) end
    end

    function modularUI.handlerChar ( _, char )
        local item = watcher[1]
        if not item then return end

        if item.widget.__onChar and not item.widget._disabled then item.widget:__onChar( char ) end
    end

    function modularUI.handlerPaste ( _, text )
        local item = watcher[1]
        if not item then return end

        if item.widget.__onPaste and not item.widget._disabled then item.widget:__onPaste( text ) end
    end

    return modularUI
end