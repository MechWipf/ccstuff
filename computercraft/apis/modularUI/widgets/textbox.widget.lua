local private = {}

if not sha256 then error "sha256 is missing!" end

widget( "textbox", widgets.base ) {

    __construct = function ( self, x, y, w, ... )
        self.x = x
        self.y = y
        self.w = w
        self.h = 1

        self.enabled = true
        self.hidden = false
        self.__redraw = true

        private[self] = {
            text = "",
            protected = false,
        }

        private[self].text = ""
        private[self].protected = false

        self.cursorX = 0

        self.child = {}

        if parent and parent.addChild then
            parent:addChild( self )
        end
    end,

    _onKey = function ( self, key )
        if self.focus then
            if key == keys.backspace and self.cursorX ~= 0 then
                private[self].text = private[self].text:sub( 1, self.cursorX - 1 ) .. private[self].text:sub( self.cursorX + 1 )
                self.cursorX = math.max( self.cursorX - 1, 0 )
                self:invalidate()
            elseif key == keys.left then
                self.cursorX = math.max( self.cursorX - 1, 0 )

                self:invalidate()
            elseif key == keys.right then
                self.cursorX = math.min( self.cursorX + 1, private[self].text:len() )

                self:invalidate()
            elseif key == keys.enter then
                    if self.onEnter then self:onEnter() end
                    if self.next then
                        self:setFocus( self.next )
                    end
            elseif key == keys.tab then
                if self.next then
                    self:setFocus( self.next )
                end
            end

            if self.onKey then self:onKey( private[self].protected and nil or key ) end
        end
    end,

    _onKeyUp = function ( self, key )
        if self.focus then
            if self.onKeyUp then self:onKeyUp( private[self].protected and nil or key ) end
        end
    end,

    _onChar = function ( self, char )
        if self.focus then
            local newText = private[self].text:sub( 1, self.cursorX ) .. char .. private[self].text:sub( self.cursorX + 1 )
            if self.regex and not newText:match(self.regex) then return end
            private[self].text = newText
            self.cursorX = self.cursorX + 1
            self:invalidate()

            if self.onChar then self:onChar( private[self].protected and nil or char ) end
        end
    end,

    _onPaste = function ( self, pasted )
        if self.focus then
            private[self].text = private[self].text .. pasted
            self.cursorX = self.cursorX + pasted:len()
            self:invalidate()

            if self.onPaste then self:onPaste( private[self].protected and nil or pasted ) end
        end
    end,

    _update = function ( self )
        if self._focus ~= self.focus then
            self:setCursor( self.focus )
            self:invalidate()
        end
        self._focus = self.focus

        if math.floor( ( os.clock() * 2 ) % 2 ) ~= self.clock then
            self.clock = math.floor( ( os.clock() * 2 ) % 2 )
            self.f = not self.f

            self:invalidate()
        end
    end,

    _onClick = function ( self )
        self.cursorX = private[self].text:len()
    end,

    _draw = function ( self, g )
        local x, y   = self:getPos()
        local w      = self.w
        local text   = private[self].text
        local scroll = 0

        if self.cursorX >= w then
            scroll = ( self.cursorX - w ) + 1
        end

        if private[self].protected then text = text:gsub( ".", "*" ) end

        if self.cursorBlink then
            if self.f then
                text = text:sub( 1, self.cursorX ) .. "_" .. text:sub( self.cursorX + 2 )
            end
        end

        text = text:sub( scroll + 1, scroll + w )

        g.setBackgroundColor( self:getColor( "textBackground" ) )
        g.setTextColor( self:getColor( "text" ) )

        g.setCursorPos( x, y )
        g.write( text )
        g.write( (" "):rep( w - text:len() ) )
    end,

    setCursor = function ( self, bool )
        self.cursorBlink = bool
    end,

    checkText = function ( self, textb )
        local texta = sha256( private[self].text )
        return texta == textb
    end,

    setProtected = function ( self, bool )
        private[self].protected = bool == true
        private[self].text = ""
    end,

    setText = function ( self, text )
        if not private[self].protected then
            private[self].text = text
            return
        end

        error( "Tried to access protected data." )
    end,

    addText = function ( self, text )
        private[self].text = string.format( "%s%s", private[self].text or "", text or "" )
    end,

    getText = function ( self )
        if not private[self].protected then
            return private[self].text
        end

        return sha256( private[self].text )
    end,

    clear = function ( self )
        private[self].text = ""
    end,
}
