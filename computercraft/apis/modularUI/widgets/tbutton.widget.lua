-- ****************************** --
-- User: MechWipf
-- Date: 28.06.2015
-- Time: 12:08
-- ****************************** --

widget( "tbutton", widgets.base ) {

    _construct = function ( self, text )
        self.text = text

        self.isActive = false
        self._isActive = false
        self.deactivateOnClick = true
    end,

    _onClick = function ( self, btn, x, y )
        if self.deactivateOnClick then
            self.isActive = not self.isActive
        else
            self.isActive = true
        end

        if btn == 1 then
            if self.onClickLeft then self:onClickLeft() end
        elseif btn == 2 then
            if self.onClickRight then self:onClickRight() end
        end

        self.focus = false
    end,

    _onKey = function ( self, key )
        if self.focus then
            if key == keys.enter then
                if self._onClick then self:_onClick( 1, 0, 0 ) end
            elseif key == keys.tab then
                if self.next then
                    self:setFocus( self.next )
                end
            end

            if self.onKey then self:onKey( key ) end
        end
    end,

    _update = function ( self )
        if self.isActive ~= self._isActive then
            self:invalidate()
            self._isActive = self.isActive
        end

        if self.hover then
            self:invalidate()
        end

        if math.floor( ( os.clock() * 2 ) % 2 ) ~= self.clock then
            self.clock = math.floor( ( os.clock() * 2 ) % 2 )
            self.f = not self.f

            self:invalidate()
        end
    end,

    _draw = function ( self, g )
        local x, y = self:getPos()
        local w, h = self.w, self.h

        if self.isActive then
            g.setBackgroundColor( self:getColor( "button_active" ) )
        else
            g.setBackgroundColor( self:getColor( "button" ) )
        end

        g.setTextColor( self:getColor( "text" ) )

        for i = 0, self.h - 1 do
            g.setCursorPos( x, y + i )
            g.write( (" "):rep(self.w) )
        end

        g.setCursorPos( x + math.floor( w/2 - self.text:len()/2 ) , y )
        local text = ( self.f and self.focus ) and "_" .. self.text:sub( 2 ) or self.text
        g.write( text )
    end,
}
