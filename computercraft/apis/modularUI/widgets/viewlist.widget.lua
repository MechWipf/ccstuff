
local widgets = widgets
local widget = widget
local ipairs = ipairs
local math = math
local table = table
local error = error
local pairs, type, tostring = pairs, type, tostring
local setmetatable = setmetatable

local private = {}

local addChild = widgets.base.addChild
local clearChilds = widgets.base.clearChilds

widget( "viewlist", widgets.base ) {

    _construct = function ( self, mode )
        self.mode = mode or "text"
        self.scroll = 1
        self.scrollMax = 1
        private[self] = { items = {}, w = {} }
    end,

    _onScroll = function ( self, dir )
        self.scroll = self.scroll + dir
        self.scroll = math.clamp( self.scroll, 1, math.max( 1, self.scrollMax - (self.h - 1) ) )

        if self.scroll ~= self.scroll_ then
            self:invalidate()
            self.scroll_ = self.scroll
        end
    end,

    _onClick = function ( self, btn, x, y )
      local _x = self:getPos()

      if x == _x + self.w - 1 then
        if y >= self.h / 2 then
          self:_onScroll( 3 )
        else
          self:_onScroll( -3 )
        end
      end
    end,

    _draw = function ( self, g )
        local x, y = self:getPos()
        local w, h = self.w, self.h

        self:calcScroll()
        local maxScroll = math.max( 1, self.scrollMax - self.h )

        clearChilds( self )

        if self.mode == "item" then
            for i = 0, h - 1 do
                g.setBackgroundColor( self:getColor( "background" ) )
                g.setCursorPos( x, y + i )
                g.write( ( (" "):rep( w - 1 ) ) )
            end

            local virtual_h = 0
            local used_h = 0
            for i = 1, self:count() do
                local item = private[self].items[i]
                virtual_h = virtual_h + item.h

                if virtual_h >= self.scroll and ( virtual_h - item.h ) < self.scrollMax then
                    item:setPos( nil, virtual_h - item.h - self.scroll + 1 )
                    addChild( self, item )
                    item:refresh()

                    used_h = used_h + item.h
                end

                if virtual_h - (self.scroll -1) >= self.h then break end
            end
        elseif self.mode == "text" then
            for i = 0, h - 1 do
                local item = private[self].items[ i + self.scroll ]
                if type( item ) ~= "table" then
                        item = {
                        { "text", self:getColor( "text" ) },
                        { "background", self:getColor( "background" ) },
                        item and tostring(item) or ""
                    }
                end

                g.setCursorPos( x, y + i )

                local cx = 0
                for _, v in pairs( item ) do
                    if type( v ) == "table" then
                        if v[1] == "text" then
                            g.setTextColor( v[2] )
                        elseif v[1] == "background" then
                            g.setBackgroundColor( v[2] )
                        end
                    else
                        if ( cx + v:len() ) > ( w - 1 ) then v = v:sub( 1, ( w - 1 ) - cx ) end
                        g.write( v )
                        cx = cx + v:len()
                    end
                end

                if cx < ( w - 1 ) then
                    g.write( (" "):rep( ( w - 1 ) - cx ) )
                end
            end
        elseif self.mode == "semi_item" then
            for i = 0, h - 1 do
                g.setBackgroundColor( self:getColor( "background" ) )
                g.setCursorPos( x, y + i )
                g.write( ( (" "):rep( w - 1 ) ) )
            end

            local virtual_h = 0
            local used_h = 0
            local itemCount = 0
            for i = 1, self:count() do
                local itemData = private[self].items[i]
                local item = private[self].w[i] or self.wrapper()
                item:apply( itemData )
                private[self].w[i] = item

                virtual_h = virtual_h + item.h

                if virtual_h >= self.scroll then
                    item:setPos( nil, virtual_h - item.h - self.scroll + 1 )
                    addChild( self, item )
                    item:refresh()

                    used_h = used_h + item.h
                    itemCount = itemCount + 1
                end

                if virtual_h - (self.scroll - 1) >= self.h then break end
            end

            self.scrollMax = virtual_h + 1 + ( self:count() - itemCount ) * ( self.itemHeight or 1 )
            maxScroll = math.max( 1, self.scrollMax - self.h )
        end

        if not self.hideScrollbar then
            for i = 0, h - 1 do
                g.setCursorPos( x + w - 1, y + i )
                g.setBackgroundColor( self:getColor( "scroll" ) )
                g.setTextColor( self:getColor( "text" ) )

                if i == 0 then
                    g.write( "^" )
                elseif i == h - 1 then
                    g.write( "v" )
                elseif i == math.clamp( math.floor( ( self.scroll / maxScroll ) * h ), 1, h - 2 ) then
                    g.write( "=" )
                else
                    g.write( "|" )
                end
            end
        end
    end,

    addChild = function ( self, item )
        self:add( item )
    end,

    clearChilds = function ( self )
        error( "Can't change childs on managed widgets." )
    end,

    setAutoupdate = function ( self, bool )
        self.autoupdate = bool == true
    end,

    setScroll = function ( self, n )
        self.scroll = math.clamp( n, 1, math.max( 1, self.scrollMax - self.h ) )
        if self.autoupdate then self:invalidate() end
    end,

    insert = function ( self, index, item )
        if not self.simpleWrapper then
            item.parent = self
            item:refresh()
        end

        table.insert( private[self].items, index, item )
        self:calcScroll()
        if self.autoupdate then self:invalidate() end
    end,

    add = function ( self, item )
        if not self.simpleWrapper then
            item.parent = self
            item:refresh()
        end

        private[self].items[ #private[self].items + 1 ] = item
        self:calcScroll()
        if self.autoupdate then self:invalidate() end
    end,

    remove = function ( self, index )
        table.remove( private[self].items, index )
        self:calcScroll()
        if self.autoupdate then self:invalidate() end
    end,

    clear = function ( self )
        private[self].items = {}
        if self.autoupdate then self:invalidate() end
    end,

    count = function ( self )
        if private[self].items.len then
            return private[self].items:len()
        else
            return #private[self].items
        end
    end,

    setSimpleWrapper = function ( self, switch )
        self.simpleWrapper = switch == true
    end,

    setContainer = function ( self, container )
        if self.simpleWrapper then
            private[self].items = container
        else
            private[self].mt = { __index = container }
            private[self].items = setmetatable( private[self].items, private[self].mt )
        end
        self:calcScroll()
    end,

    clearContainer = function ( self )
        private[self].mt = { __index = nil }
    end,

    calcScroll = function ( self )
        if self.mode == "item" then
            local scrollMax = 0
            for _, item in pairs( private[self].items ) do
                scrollMax = scrollMax + item.h
            end

            self.scrollMax = scrollMax
        else
            self.scrollMax = self:count() * ( self.itemHeight or 1 )
        end
    end,
}
