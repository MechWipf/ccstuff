local defaultColors, math, table =
      defaultColors, math, table

widget( "progressbar", widgets.base ) {
    __construct = function ( self, x, y, w, text )
        self.x = x
        self.y = y
        self.w = w
        self.h = 1

        self.percent = 0

        self.text = text

        self.enabled = true
        self.hidden = false
        self.__redraw = true
        self.colors = defaultColors

        self.child = {}
    end,

    _draw = function ( self, g )
        local x, y = self:getPos()
        local w, h = self.w, self.h
        local perc = self.percent

        local text = self.text
        local padding = w - text:len()

        if padding < 0 then text = "" padding = 0 end

        text = table.concat({
            (" "):rep( math.floor( padding / 2 ) ),
            text,
            (" "):rep( math.ceil( padding / 2 ) )
        })

        local cut = math.floor( w * perc )
        text = {
            text:sub( 1, cut ),
            text:sub( cut + 1, text:len() )
        }

        g.setTextColor( self:getColor( "text" ) )

        g.setCursorPos( x, y )
        g.setBackgroundColor( self:getColor( "textBackground" ) )
        g.write( text[1] )

        g.setCursorPos( x + cut, y )
        g.setBackgroundColor( self:getColor( "background" ) )
        g.write( text[2] )


        -- if self._w then
        --     g.write( (" "):rep( self.w - self.text:len() ) )
        --     self.w = self._w
        --     self._w = nil
        -- end
    end,

    setText = function ( self, text )
        self.text = text
        self:invalidate()
    end,

    setPercent = function ( self, percent )
        self.percent = percent
        self:invalidate()
    end
}
