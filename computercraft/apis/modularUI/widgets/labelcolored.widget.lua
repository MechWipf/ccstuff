
widget( "labelcolored", widgets.base ) {
    __construct = function ( self, x, y, text )
        self.x = x
        self.y = y
        self.w = text:len()
        self._w = 0
        self.h = 1

        self:setText( text )

        self.enabled = true
        self.hidden = false
        self.__redraw = true
        self.colors = defaultColors

        self.child = {}

        if parent and parent.addChild then
            parent:addChild( self )
        end
    end,

    _draw = function ( self, g )
        local x, y = self:getPos()
        local w, h = self.w, self.h

        g.setBackgroundColor( self:getColor( "background" ) )
        g.setTextColor( self:getColor( "text" ) )

        g.setCursorPos( x, y )
        for _, v in pairs( self.text ) do
          if type(v) == "string" then
            g.write( v )
          else
            g.setTextColor( v )
          end
        end

        if self:length() < self.w then
            g.write( (" "):rep( self.w - self:length() ) )
        end

        if self._w then
            g.write( (" "):rep( self.w - self:length() ) )
            self.w = self._w
            self._w = nil
        end
    end,

    length = function ( self )
        local w = 0
        for _, v in pairs( self.text ) do
            if type(v) == "string" then
                w = w + v:len()
            end
        end

        return w
    end,

    setText = function ( self, text, w )
        if type(text) ~= "table" then
            self.text = { tostring(text) }
        else
            self.text = text
        end

        self._w = w or self:length()

        if self.w < self._w then
            self.w = self._w
        end

        self:invalidate()
    end,
}
