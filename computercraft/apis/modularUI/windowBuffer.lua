local module = {}

function module.WindowBuffer ( x, y, w, h )
  local _windowBuffer = {}

  -- Stuff
  local lines = {}
  local updatedLines = {}

  local textColor = colors.white
  local backgroundColor = colors.black

  local cursorPosY, cursorPosX = 1, 1

  local posX, posY = x, y
  local width, height = w, h

  function _windowBuffer.write ( ... )
    local stringBuffer = {}

    local count = select( "#", ... )
    local i = 1
    while i <= count do
      i = i + 1
      stringBuffer[#stringBuffer+1] = tostring( select( i, ... ) )
    end

    local text = table.concat( stringBuffer )
  end

  function _windowBuffer.blit ()
  end

  function _windowBuffer.clear ()
  end

  function _windowBuffer.clearLine ()
  end

  function _windowBuffer.getCursorPos ()
  end

  function _windowBuffer.setCursorPos ()
  end

  function _windowBuffer.setCursorBlink ()
  end

  function _windowBuffer.isColor ()
  end

  function _windowBuffer.isColour ()
  end

  function _windowBuffer.setTextColor ( color )
  end

  function _windowBuffer.setTextColour ( color )
    _windowBuffer.setTextColor( color )
  end

  function _windowBuffer.setBackgroundColor ( color )
  end

  function _windowBuffer.setBackgroundColour ( color )
    _windowBuffer.setBackgroundColor( color )
  end

  function _windowBuffer.getTextColor ( color )
  end

  function _windowBuffer.getTextColour ( color )
    return _windowBuffer.getTextColor( color )
  end

  function _windowBuffer.getBackgroundColor ( color )
  end

  function _windowBuffer.getBackgroundColour ( color )
    _windowBuffer.getBackgroundColor( color )
  end

  function _windowBuffer.getSize ()
  end

  function _windowBuffer.scroll ()
  end

  function _windowBuffer.getPosition ()
  end

  function _windowBuffer.drawOn ( target, fullDraw )
  end

  return _windowBuffer
end

return module