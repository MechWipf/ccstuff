return {
  Wrapper = function ( modularUI, dom, fnNewItem )
    -- local item = fnNewItem()

    -- function item:apply ( data )
    -- end

    -- local pnCraftPanel = modularUI.widgets.panel( 1, 0, vlItemsCrafting.w - 1, 1 )
    -- pnCraftPanel:setColors{ background = colors.black }

    -- local lbCraftLabel = modularUI.widgets.label( 0, 0, "" )
    -- lbCraftLabel:setColors{ background = colors.black }
    -- lbCraftLabel:setParent( pnCraftPanel )

    -- if app.inventory and app.inventory.requestCrafting then
    --   local _text = "C"
    --   local btnCraftOpen = modularUI.widgets.button( 0, 0, _text:len(), 1, _text )
    --   btnCraftOpen:setParent( pnCraftPanel )

    --   _text = "S"
    --   local btnStockOpen = modularUI.widgets.button( 1, 0, _text:len(), 1, _text )
    --   --btnStockOpen:setParent( pnCraftPanel )
    --   btnStockOpen:setColors{ text = colors.orange, button = colors.brown }

    --   function btnCraftOpen.onClickLeft ()
    --     app:showCraftForm ( pnCraftPanel.fingerprint )
    --   end

    --   function btnStockOpen.onClickLeft ()
    --     app:showCraftForm ( pnCraftPanel.fingerprint, true )
    --   end

    --   lbCraftLabel:setPos( 2 )
    -- end

    -- function pnCraftPanel:apply ( data )
    --   self.fingerprint = data[2]
    --   lbCraftLabel:setText( data[1] )
    -- end

    -- return pnCraftPanel
  end,
  shutdown = function ( wasClean )
    if not wasClean then
      term.setBackgroundColor( colors.blue )
      term.clear()
      term.setCursorPos( 1, 1 )

      term.setTextColor( colors.white )
      print( "The application encountered an unexpected error.\n" )
      term.setTextColor( colors.yellow )
      print( debug.lastError )
      io.read "*l"
    end

    term.setBackgroundColor(colors.black)
    term.clear()
    term.setCursorPos( 1, 1 )
  end,
  TabController = function ( tabList, parent )
    return {
      tabList = tabList,
      parent = parent,
      activeTab = nil,

      switchTab = function ( self, tabName )
        for _, tab in pairs( self.tabList ) do
          local active = ( tab.name == tabName )

          tab.button.isActive = active
          tab.button:invalidate()

          for _, w in pairs( tab.widgets ) do
            if active then
              w:setParent( self.parent )
            elseif w.parent then
              w:delParent()
            end
          end
        end

        self.activeTab = tabName
        self.parent:refresh()
      end,

      setTab = function ( self, tabName )
        for _, tab in pairs( self.tabList ) do
          local active = ( tab.name == tabName )

          tab.button.isActive = active

          for _, w in pairs( tab.widgets ) do
            if active then
              w:setParent( self.parent )
            elseif w.parent then
              w:delParent()
            end
          end
        end

        self.activeTab = tabName
        self.parent:invalidate()
      end,

      addTab = function ( self, tab )
        self.tabList[#self.tabList+1] = tab
      end,

      setParent = function ( self, newParent )
        self.parent = newParent
      end,
    }
  end
}