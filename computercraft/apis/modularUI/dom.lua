local module = {}

module._watches = {}
local _watches = module._watches

local function _build ( widget_name, opts, env )
  local head = opts.head or {}
  opts.head = nil

  local children = opts[1] or {}
  opts.children = nil

  local color = opts.color or {}

  local name = opts.name

  local widget = module.widgets[widget_name]( unpack( head ) )

  if name then
    env[name] = widget
  end

  for _, child in pairs( children ) do
    widget:addChild( child )
  end

  for k, v in pairs( opts ) do
    if type(v) == "string" and v:sub( 1, 1 ) == "@" then
      local fname = v:sub(2)
      if env[fname] == nil then error( "Missing link: " .. fname ) end

      if type(widget[k]) == "function" then
        widget[k]( widget, env[fname] )
      else
        widget[k] = env[fname]
      end

      _watches[#_watches+1] = { widget, env, k, fname, env[fname] }
    elseif type(v) == "string" and v:sub( 1, 1 ) == "#" then
      local fname = v:sub(2)
      if env[fname] == nil then error( "Missing link: " .. fname ) end

      widget[k] = env[fname]( widget )
    elseif type(v) == "string" and v:sub( 1, 1 ) == "=" then
      local fn = loadstring( ("return function ( self ) return %s end"):format( v:sub(2) ) )
      setfenv( fn, setmetatable( { env = env }, { __index = getfenv() } ) )
      widget[k] = fn()
    else
      if type(widget[k]) == "function" then
        widget[k]( widget, v)
      else
        widget[k] = v
      end
    end
  end

  if color then
    widget:setColors( color )
  end

  if widget.init then
    widget:init()
    widget.init = nil
  end

  return widget
end

--- Generate GUI from function
-- @name Dom.build
-- @param fn [function]
-- @param env [table] Environment table that should be used.
-- @return returns whatever fn returns.
function module.build ( fn, env )
  local function __index ( _, tag_name )
    if _G[tag_name] then return _G[tag_name] end

    return function ( opts )
      return _build( tag_name, opts, env )
    end
  end

  local mt = setmetatable( { env = env }, { __index = __index } )
  setfenv( fn, mt )

  return fn()
end

local cache = {}
--- Build GUI from file
-- @name Dom.buildFile
-- @param path [string] Path to file.
-- @param env [table] Environment table that should be used.
-- @return returns whatever the file returns.
function module.buildFile ( path, env, noCache )
  local fn

  if cache[path] then
    fn = cache[path]
  else
    fn = loadfile( path )
    if not noCache then cache[path] = fn end
  end

  return module.build( fn, env )
end

--- Preload a function
-- @name Dom.load
-- @param fn [function]
-- @return build_fn(env) [function]
-- @usage Use this to prebuild some GUI widgets
function module.load ( fn )
  return function ( env )
    return module.build( fn, env )
  end
end

--- Preload a file
-- @name Dom.loadFile
-- @param path [string] Path to file.
-- @return build_fn(env) [function]
-- @usage Use this to prebuild some GUI widgets
function module.loadFile ( path )
  return function ( env )
    return module.buildFile( path, env )
  end
end

function module.jobWatch ()
  local i, c = 1, #_watches

  while i <= c do
    local watch = _watches[i]
    local widget, env, widgetKey, envKey, lastVal = unpack( watch )

    if widget.isDead then
      table.remove( _watches, i )
      c = #_watches
    else
      if env[envKey] ~= lastVal then
        if type(widget[widgetKey]) == "function" then
          widget[widgetKey]( widget, env[envKey] )
        else
          widget[widgetKey] = env[envKey]
        end

        widget:invalidate()
        watch[5] = env[envKey]
      end
      i = i + 1
    end
  end
end

function module.clearWatches ()
  _watches = {}
  module._watches = _watches
end

return module