local function class(className)
	return function(prototype)
		local metatable = { __index = prototype }
		prototype.__metatable = metatable
		_G[className] = function(...)
			local self = setmetatable({}, metatable)
			if self.__construct then self:__construct(...) end
			return self
		end
	end
end

--- Creates a new event handler
-- @class function
-- @name Event
-- @return instance [Event]
-- @usage Import the module via dofile and call the returning table to get a Event instance.
class "events" {
	__construct = function(self)
		self.pullEvent_handlers = {}
		self.event_handlers = {}
		self.timers = {}
		self.callbacks = {}
		self.intervals = {}

		local function globalTimerHandler(_, timerId)
			if self.timers[timerId] then
				self.timers[timerId]()
			elseif self.intervals[timerId] then
				local intervalHandler, delay, times = unpack( self.intervals[timerId] )
				intervalHandler()
				if times > 0 then self:interval( delay, intervalHandler, times ) end
			end
		end

		self:event("timer", self.timers, globalTimerHandler)
		self:event("alarm", self.timers, globalTimerHandler)
	end,

	--- Adds a new listener
	-- @name Event.event
	-- @param self [Event]
	-- @param event_name [string] The name of the event to listen.
	-- @param key [string] The name your handler has, to identify it.
	-- @param event_handler [function] A callback function which will be called when the event occures.
	event = function(self, event_name, key, event_handler)
		if type(event_name) == "table" then
			for _, _event_name in pairs(event_name) do
				self:event(_event_name, key, event_handler)
			end
			return
		end
		if not self.event_handlers[event_name] then self.event_handlers[event_name] = {} end
		self.event_handlers[event_name][key] = event_handler
	end,

	--- Adds a new timer
	-- @name Event.timer
	-- @param self [Event]
	-- @param delay [number] In seconds.
	-- @param handler [function] Callback.
	timer = function(self, delay, handler)
		local timerId = os.startTimer(delay)
		self.timers[timerId] = handler
		return { cancel = function() self.timers[timerId] = nil end }
	end,

	--- Adds a new interval
	-- @name Event.interval
	-- @param self [Event]
	-- @param delay [number] In seconds.
	-- @param handler [function] Callback.
	-- @param times [number] How often this should be called. default: #inf.
	-- @return close_handler [table] A table with a cancel function.
	interval = function(self, delay, handler, times)
		times = times or 1e999
		local timerId = os.startTimer(delay)
		self.intervals[timerId] = { handler, delay, times - 1 }
		return { cancel = function() self.intervals[timerId] = nil end }
	end,

	--- Adds a new alarm.
	-- Alarms are set to an ingame time.
	-- @name Event.alarm
	-- @param self [Event]
	-- @param time [number] A minecraft time.
	-- @param handler [function] Callback.
	-- @return close_handler [table] A table with a cancel function.
	alarm = function(self, time, handler)
		local timerId = os.setAlarm(time)
		self.timers[timerId] = handler
		return { cancel = function() self.timers[timerId] = nil end }
	end,

	--- Sleep for some time. (blocking the execution)
	-- @name Event.sleep
	-- @param self [Event]
	-- @param delay [number] In seconds.
	sleep = function(self, delay)
		local b = true
		self:timer(delay, function() b = false end)
		while b do
			self:iteration()
			if self.returnValue then error(tostring(self), 0) end
		end
	end,

	pushEvent = function ( self, name, callback )
		self.callbacks[name] = callback
	end,

	killCallback = function ( self, name )
		self.callbacks[name] = false
	end,

	iteration = function(self)
		local event_handlers = self.event_handlers -- optimization
		local callbacks = self.callbacks

		local ok, msg = pcall(function(event_name, ...)
			local t = event_handlers[event_name]

			if t then
				for _, event_handler in pairs(t) do
					event_handler(event_name, ...)
				end
			end

			for _, callback in pairs(callbacks) do
				if callback then
					callback( event_name, ... )
				end
			end

		end, coroutine.yield())

		if not ok then
			if msg == tostring(self) then return end
			error(msg,0)
		end
	end,

	--- Starts the main loop of the event handler.
	-- @name Event.mainLoop
	-- @param self [Event]
	-- @return reason [string]
	mainLoop = function(self)
		while true do
			self:iteration()
			if self.returnValue then return unpack(self.returnValue) end
		end
	end,

	--- Quits the event handler
	-- @name Event.quit
	-- @param self [Event]
	-- @param ...
	quit = function(self, ...)
		self.returnValue = {...}
		error(tostring(self), 0)
	end,
}

return _G.events