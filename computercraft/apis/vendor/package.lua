-- +---------------------+------------+---------------------+
-- |                     |            |                     |
-- |                     |   Package  |                     |
-- |                     |            |                     |
-- +---------------------+------------+---------------------+

local version = "Version 1.4.0pr1"

-- Pastebin uploader/downloader for ComputerCraft, by Jeffrey Alexander (aka Bomb Bloke).
-- Handles multiple files in a single paste, as well as non-ASCII symbols within files.
-- http://www.computercraft.info/forums2/index.php?/topic/21801-package-pastebin-uploader-downloader

-- This is a reduced build. API only, base64/pastebin functionality removed.

---------------------------------------------
------------Variable Declarations------------
---------------------------------------------

local band, brshift, blshift = bit.band, bit.brshift, bit.blshift

---------------------------------------------
------------Function Declarations------------
---------------------------------------------

local function compressIterator(ClearCode)
	local startCodeSize = 1
	while math.pow(2, startCodeSize) < ClearCode do startCodeSize = startCodeSize + 1 end

	local EOI, ClearCode = math.pow(2, startCodeSize) + 1, math.pow(2, startCodeSize)
	startCodeSize = startCodeSize + 1

	local curstring, len, curbit, curbyte, outputlist, codes, CodeSize, MaxCode, nextcode, curcode = "", 2, 1, 0, {0}, {}, startCodeSize, math.pow(2, startCodeSize) - 1, EOI + 1

	local function packByte(num)
		local mask = 1

		for i = 1, CodeSize do
			if band(num, mask) == mask then curbyte = curbyte + curbit end
			curbit, mask = curbit * 2, mask * 2

			if curbit > 128 or (i == CodeSize and num == EOI) then
				local counter = blshift(brshift(#outputlist - 1, 8), 8) + 1
				outputlist[counter] = outputlist[counter] + 1
				if outputlist[counter] > 255 then outputlist[counter], outputlist[counter + 256], len = 255, 1, len + 1 end
				outputlist[len] = curbyte
				curbit, curbyte, len = 1, 0, len + 1
			end
		end
	end

	packByte(ClearCode)

	return function(incode)
		if not curcode then
			curcode = incode
			return
		end

		if not incode then
			packByte(curcode)
			packByte(EOI)
			return outputlist
		end

		curstring = curstring .. string.char(incode)
		local thisCode = codes[curstring]

		if thisCode then
			curcode = thisCode
		else
			codes[curstring] = nextcode
			nextcode = nextcode + 1

			packByte(curcode)

			if nextcode == MaxCode + 2 then
				CodeSize = CodeSize + 1
				MaxCode = math.pow(2, CodeSize) - 1
			end

			if nextcode == 4095 then
				packByte(ClearCode)
				CodeSize, MaxCode, nextcode, codes = startCodeSize, math.pow(2, startCodeSize) - 1, EOI + 1, {}
			end

			curcode, curstring = incode, string.char(incode)
		end
	end
end

local function compressInternal(inputlist, valRange)
	if type(inputlist) ~= "table" and type(inputlist) ~= "string" then error("package.compress: Expected: table, string or file handle", 2) end

	if not valRange then valRange = 256 end
	if type(valRange) ~= "number" or valRange < 2 or valRange > 256 then error("package.compress: Value range must be a number between 2 - 256.", 2) end

	if type(inputlist) == "table" and inputlist.close then
		local templist = {}
		if inputlist.readLine then
			templist = inputlist.readAll()
		else
			local len = 1
			for thisByte in inputlist.read do
				templist[len] = thisByte
				len = len + 1
			end
		end
		inputlist.close()
		inputlist = templist
	end

	if type(inputlist) == "string" then
		local templist = {}
		for i = 1, #inputlist do templist[i] = inputlist:byte(i) end
		inputlist = templist
	end

	if #inputlist == 0 then return {} end

	local outputlist, compressIt = {}, compressIterator(valRange)

	local sleepCounter = 0
	for i = 1, #inputlist do
		compressIt(inputlist[i])

		sleepCounter = sleepCounter + 1
		if sleepCounter > 1023 then
			sleepCounter = 0
			sleep()
		end
	end

	return compressIt(false)
end

local function decompressIterator(ClearCode, codelist)
	if type(codelist) == "table" then
		local codetable, spot = codelist, 0

		codelist = function()
			spot = spot + 1
			return codetable[spot]
		end
	end

	local startCodeSize = 1
	while math.pow(2, startCodeSize) < ClearCode do startCodeSize = startCodeSize + 1 end

	local EOI, ClearCode = math.pow(2, startCodeSize) + 1, math.pow(2, startCodeSize)
	startCodeSize = startCodeSize + 1

	local lastcounter, CodeSize, MaxCode, maskbit, nextcode, codes, gotbytes = codelist(), startCodeSize, math.pow(2, startCodeSize) - 1, 1, EOI + 1, {}, 1
	local curbyte = codelist()
	for i = 0, ClearCode - 1 do codes[i] = {[0] = i} end

	return function()
		while true do
			local curcode, curbit = 0, 1

			for i = 1, CodeSize do
				if band(curbyte, maskbit) == maskbit then curcode = curcode + curbit end
				curbit, maskbit = curbit * 2, maskbit * 2

				if maskbit > 128 and not (i == CodeSize and curcode == EOI) then
					maskbit, curbyte, gotbytes = 1, codelist(), gotbytes + 1

					if gotbytes > lastcounter then
						if curbyte == 0 then break end
						lastcounter, gotbytes = curbyte, 1
						curbyte = codelist()
					end
				end
			end

			if curcode == ClearCode then
				CodeSize, MaxCode, nextcode, codes = startCodeSize, math.pow(2, startCodeSize) - 1, EOI + 1, {}
				for i = 0, ClearCode - 1 do codes[i] = {[0] = i} end
			elseif curcode ~= EOI then
				if codes[nextcode - 1] then codes[nextcode - 1][#codes[nextcode - 1] + 1] = codes[curcode][0] else codes[nextcode - 1] = {[0] = codes[curcode][0]} end

				if nextcode < 4096 then
					codes[nextcode] = {}
					for i = 0, #codes[curcode] do codes[nextcode][i] = codes[curcode][i] end
					nextcode = nextcode + 1
				end

				if nextcode - 2 == MaxCode then
					CodeSize = CodeSize + 1
					MaxCode = math.pow(2, CodeSize) - 1
				end

				return codes[curcode]
			else return end
		end
	end
end

local function decompressInternal(codelist, outputText, valRange)
	if type(codelist) ~= "table" then error("package.decompress: Expected: table or file handle", 2) end

	if not valRange then valRange = 256 end
	if type(valRange) ~= "number" or valRange < 2 or valRange > 256 then error("package.decompress: Value range must be a number between 2 - 256.", 2) end

	if codelist.read then
		codelist = codelist.read
	elseif codelist.readLine then
		codelist.close()
		error("package.decompress: Use binary-mode file handles", 2)
	elseif #codelist == 0 then return outputText and "" or {} end

	local outputlist, decompressIt, len = {}, decompressIterator(valRange, codelist), 1

	local sleepCounter = 0
	while true do
		local output = decompressIt()

		if output then
			for i = 0, #output do
				outputlist[len] = output[i]
				len = len + 1
			end
		else break end

		sleepCounter = sleepCounter + 1
		if sleepCounter > 1023 then
			sleepCounter = 0
			sleep()
		end
	end

	return outputText and string.char(unpack(outputlist)) or outputlist
end

---------------------------------------------
------------     Load As API     ------------
---------------------------------------------

compress =      compressInternal
decompress =    decompressInternal

function open(file, mode, valRange)
	if (type(file) ~= "table" and type(file) ~= "string") or type(mode) ~= "string" then error("package.open: Expected: file (string or handle), mode (string). Got: " .. type(file) .. ", " .. type(mode) .. ".", 2) end

	mode = mode:lower()
	local binary, append, read, write, newhandle = mode:find("b") ~= nil, mode:sub(1, 1) == "a", mode:sub(1, 1) == "r", mode:sub(1, 1) == "w", {}

	if not valRange then valRange = binary and 256 or 128 end
	if type(valRange) ~= "number" or valRange < 2 or valRange > 256 then error("package.decompress: Value range must be a number between 2 - 256.", 2) end

	if not (append or write or read) then error("package.open: Invalid file mode: " .. mode, 2) end

	if type(file) == "string" then
		if append and fs.exists(file) then
			local oldfile = open(file, "r" .. (binary and "b" or ""), valRange)
			if not oldfile then return nil end

			if binary then
				local olddata = {}
				for byte in oldfile.read do olddata[#olddata + 1] = byte end
				oldfile.close()
				newhandle = open(file, "wb", valRange)
				for i = 1, #olddata do newhandle.write(olddata[i]) end
			else
				local olddata = oldfile.readAll()
				oldfile.close()
				newhandle = open(file, "w", valRange)
				newhandle.write(olddata)
			end

			return newhandle
		end

		file = fs.open(file, mode:sub(1, 1) .. "b")
		if not file then return nil end
	else
		if (write and (file.writeLine or not file.write)) or (read and not file.read) then error("package.open: Handle / mode mismatch.", 2) end

		local tempfile, keys = {}, {}

		for key, _ in pairs(file) do keys[#keys + 1] = key end
		for i = 1, #keys do
			tempfile[keys[i]] = file[keys[i]]
			file[keys[i]] = nil
		end

		file = tempfile
	end

	if read then
		local decompressIt, counter, listlen, outputlist, getVal = decompressIterator(valRange, file.read), 1, 0

		function getVal()
			if not counter then return nil end

			if counter >= listlen then
				outputlist = decompressIt()

				if not outputlist then
					counter = false
					return nil
				end

				listlen, counter = #outputlist, -1
			end

			counter = counter + 1
			return outputlist[counter]
		end

		if binary then
			newhandle.read = getVal
		else
			function newhandle.readLine()
				local line = {}

				repeat
					local inVal = getVal()
					if inVal then
						line[#line + 1] = inVal
					else
						newhandle.readLine = function() return nil end
						newhandle.readAll = newhandle.readLine
						return #line > 0 and string.char(unpack(line)) or nil
					end
				until line[#line] == 10

				line[#line] = nil
				if line[#line] == 13 then line[#line] = nil end
				return string.char(unpack(line))
			end

			function newhandle.readAll()
				local lines = {}
				for line in newhandle.readLine do lines[#lines + 1] = line end
				return table.concat(lines, "\n")
			end
		end

		function newhandle.extractHandle()
			local keys = {}
			for key, _ in pairs(newhandle) do keys[#keys + 1] = key end
			for i = 1, #keys do newhandle[keys[i]] = nil end
			return file
		end
	else
		local compressIt = compressIterator(valRange)

		if binary then
			newhandle.write = compressIt
		else
			function newhandle.write(text)
				for i = 1, #text do compressIt(text:byte(i)) end
			end

			function newhandle.writeLine(text)
				for i = 1, #text do compressIt(text:byte(i)) end
				compressIt(10)
			end
		end

		newhandle.flush = file.flush

		function newhandle.extractHandle()
			local output, fWrite = compressIt(false), file.write
			for j = 1, #output do fWrite(output[j]) end
			local keys = {}
			for key, _ in pairs(newhandle) do keys[#keys + 1] = key end
			for i = 1, #keys do newhandle[keys[i]] = nil end
			return file
		end
	end

	function newhandle.close()
		newhandle.extractHandle().close()
	end

	return newhandle
end

function lines(file)
	if type(file) == "string" then
		file = open(file, "r")
	elseif type(file) ~= "table" or not file.readLine then
		error("package.lines: Expected: file (string or \"r\"-mode handle).", 2)
	end

	return function()
		if not file.readLine then return nil end

		local line = file.readLine()
		if line then
			return line
		else
			file.close()
			return nil
		end
	end
end

return {
	compress = compress,
	decompress = decompress
}