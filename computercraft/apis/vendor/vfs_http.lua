-- ****************************** --
-- User: MechWipf
-- Date: 14.07.2015
-- Time: 19:49
-- ****************************** --
local von, vfs, http, assert, setmetatable =
      von, vfs, http, assert, setmetatable

if not vfs then
  error "No vfs found!"
end

if not von then
  error "No von found!"
end

local function getIndex ( path )
  local f = http.get( path .. "index.von" )
  if not f then return false end
  local data = f:readAll() f:close()
  return von.deserialize( data )
end

local fs_http = {
  cache = {},

  isReadOnly = function ()
    return true
  end,

  exists = function ( self, path )
    local index, err = self:getIndex()
    if err then return http.get( self.base .. path:sub(2) ) ~= nil end
    return path == "/" or index[path]
  end,

  open = function ( self, path, mode )
    assert( mode == "r", "HTTP FS only supports text reading!" )
    return http.get( self.base .. path:sub(2) )
  end,

  list = function( self, path )
    local index, err = self:getIndex()
    if err then self.cache[path] = {} return {} end
    if self.cache[path] then return self.cache[path] end

    local list = {}
    for v, _ in pairs( index ) do
      v:gsub( "^" .. path .. "/?([a-zA-Z-_%(%)%.]+)$", function ( s ) list[#list+1] = s end )
    end

    self.cache[path] = list
    return { unpack( list ) }
  end,

  isDir = function( self, path )
    if path == "/" then
      return true
    else
      local index, err = self:getIndex()
      if err then return false end

      if index[path] == "d" then return true end
    end
  end,

  getDrive = function ( self )
    return self.base
  end,

  find = function ( self, path )
    return { path }
  end,

  getIndex = function ( self )
    if not self.index then
      self.index = getIndex( self.base )
      if not self.index then
        return nil, false
      end
    end

    return self.index
  end,
}

vfs.registerFS( "http", function ( mp, opts )
        local t = { base = opts.base }
        setmetatable( t, { __index = fs_http } )
        return t
end )
