--- Module for crafting stuff
-- Not very interesting

local json = require "vendor.json"
local http = _G.http
local sleep = _G.sleep or function () end

local _mt = {}
local Crafty = {
  _mt = _mt,
}

-- function _mt:run ()
-- end

function _mt:loadRecipes ( url )
  local handler = http.get( url )
  local content = handler.readAll()

  local recipes = json.decode( content )
  self.recipes = recipes
end

function _mt:loadOredict ( url )
  local handler = http.get( url )
  local content = handler.readAll()

  local oreDict = json.decode( content )
  self.oreDict = oreDict
end

function _mt:getRecipe ( itemKey, all )
  all = all or false
  local recipes = self.recipes[itemKey]

  if not recipes then
    return nil, "No recipes found."
  elseif #recipes > 1 and not all then
    local idx = self:chooseRecipe( itemKey )
    local recipe = recipes[idx]
    if recipe then
      return recipe
    else
      return nil, "No recipes found."
    end
  elseif all then
    return recipes
  end

  return recipes[1]
end

local function calcRecipe( self, todoRecipes, raw, storage )
  local item, amount = unpack( table.remove( todoRecipes, 1 ) )
  local recipe, err = self:getRecipe( item )

  if err then
    print( "No Recipes found for " .. item )
    raw[item] = ( raw[item] or 0 ) + amount
    return
  end


  if ( storage[item] or 0 ) < amount then
    -- How much do we actually need to craft
    amount = amount - ( storage[item] or 0 )
    -- How often do we need to do the crafting
    local n = math.ceil( amount / recipe.qty )
    -- And how much do we really gain (some recipes create overhead)
    local cAmount = recipe.qty * n
    -- Update the storage
    storage[item] = cAmount - amount

    for i, material in pairs( recipe.items ) do
      local qty = Crafty.patternCountItem( recipe.pattern, i ) * n
      todoRecipes[#todoRecipes+1] = { material, qty }
    end
  else
    storage[item] = ( storage[item] or 0 ) - amount
  end
end

function _mt:calcRecipe ( itemKey, amount )
  local storage = {}
  local todoRecipes = { { itemKey, amount } }
  local raw = {}

  local s = 0
  while #todoRecipes > 0 do
    if s > 1000 then s = 0; sleep() else s = s + 1 end
    calcRecipe( self, todoRecipes, raw, storage )
  end

  return raw, storage
end

function _mt.chooseRecipe ()
  return nil, "Multiple recipes found."
end

function Crafty.NewCrafty ()
  local obj = {
    items = {},
    recipes = {},
  }
  setmetatable( obj, { __index = Crafty._mt } )
  return obj
end

--- Converts string to fingerprint
-- @tparam string str
-- @treturn [table|fingerprint]
function Crafty.fingerprintFromString ( str )
  local id, dmg, hash = str:match( "(.-):([0-9]+):*(.*)" )
  return { id = id, dmg = dmg, nbtHash = hash }
end

--- Convert fingerprint to string
-- @tparam [table|fingerprint] fingerprint
-- @treturn string
function Crafty.fingerprintToString ( fingerprint )
  return ("%s:%s%s"):format( fingerprint.id, fingerprint.dmg, fingerprint.nbtHash and ":"..fingerprint.nbtHash or "" )
end

--- Get the amount of an item used in a pattern
-- @tparam string pattern
-- @tparam int itemNum
-- @treturn int amount
function Crafty.patternCountItem ( pattern, itemNum )
    local count = 0
    pattern:gsub( itemNum, function () count = count + 1 end )
    return count
end

--- Get amount of all items used in a recipe
-- @tparam [table|recipe] recipe
-- @treturn table amount of all items
function Crafty.recipeCountItems ( recipe )
  local amount = {}
  local pattern = recipe.pattern
  for i, item in pairs( recipe.items ) do
    amount[item] = Crafty.patternCountItem( pattern, i )
  end

  return amount
end

return Crafty