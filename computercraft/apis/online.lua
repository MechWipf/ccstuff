prefix = ""

local function loadfileOnline ( _sPath )
	--print("Fetching.. " .. _sPath)
	local _s = http.get( _sPath ).readAll()

	if not _s or #_s == 0 then
		return nil, "No such File: " .. _sPath
	end

	return loadstring( _s )
end

local tAPIsLoading = {}
function loadAPIOnline( _sPath, _sName )
	local _G = getfenv(0)
	if tAPIsLoading[_sName] == true then
		printError( "API "..sName.." is already being loaded" )
		return false
	end
	tAPIsLoading[_sName] = true

	local tEnv = {}
	setmetatable( tEnv, { __index = _G } )
	local fnAPI, err = loadfileOnline( (prefix or "") .. _sPath )
	if fnAPI then
		setfenv( fnAPI, tEnv )
		fnAPI()
	else
		printError( err )
        tAPIsLoading[_sName] = nil
		return false
	end

	local tAPI = {}
	for k,v in pairs( tEnv ) do
		tAPI[k] =  v
	end

	_G[_sName] = tAPI
	tAPIsLoading[_sName] = nil
	return true
end

function setPrefix( _sPrefix )
	if _sPrefix:sub(-1) ~= "/" then
		_sPrefix = _sPrefix .. "/"
	end
	prefix = _sPrefix
	--print("Changed prefix to \n\t" .. prefix)
end

function run( _tEnv, _sPath, ... )
	local _G = getfenv(0)
    local tArgs = { ... }
    local fnFile, err = loadfileOnline( (prefix or "") .. _sPath )
    if fnFile then
        local tEnv = _tEnv
		setmetatable( tEnv, { __index = _G } )
        setfenv( fnFile, tEnv )
        local ok, err = pcall( function()
        	fnFile( unpack( tArgs ) )
        end )
        if not ok then
        	if err and err ~= "" then
	        	printError( err )
	        end
        	return false
        end
        return true
    end
    if err and err ~= "" then
		printError( err )
	end
    return false
end
