local _dig  = {turtle.dig,		turtle.digUp,	 turtle.digDown	  }
local _move = {turtle.forward,	turtle.up,		 turtle.down   	  }
local _det  = {turtle.detect,	turtle.detectUp, turtle.detectDown}
local plugin = {}

function plugin.tryMove ( dir, n )
	local n = n or 1

	local dig  = _dig[dir]
	local move = _move[dir]
	local det  = _det[dir]
	
	while n > 0 do
		while det() and dig() do
			sleep( .5 )
			if not det() then break end
		end

		while not move() do sleep( .1 ) end
	
		n = n - 1
	end
end

function plugin.tryDig ( dir )
	local dig  = _dig[dir]
	local det  = _det[dir]
	
	while det() and dig() do
		sleep( .5 )
		if not det() then return true end
	end
	return false
end

return plugin