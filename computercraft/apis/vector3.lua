local vec3 = {}

function vec3.diff ( a, b )
  return {
    a[1]- b[1],
    a[2]- b[2],
    a[3]- b[3]
  }
end

function vec3.len ( a )
  return math.sqrt( a[1]^2 + a[2]^2 + a[3]^2 )
end

function vec3.normalize ( a )
  local len = vec3_len( a )
  return {
    a[1] / len,
    a[2] / len,
    a[3] / len
  }
end

function vec3.scalar ( a, b )
  return {
    a[1] * b,
    a[2] * b,
    a[3] * b
  }
end

function vec3.add ( a, b )
  return {
    a[1] + b[1],
    a[2] + b[2],
    a[3] + b[3]
  }
end

function vec3.min ( a, b )
  return {
    a[1] < b[1] and a[1] or b[1],
    a[2] < b[2] and a[2] or b[2],
    a[3] < b[3] and a[3] or b[3]
  }
end

function vec3.max ( a, b )
  return {
    a[1] > b[1] and a[1] or b[1],
    a[2] > b[2] and a[2] or b[2],
    a[3] > b[3] and a[3] or b[3]
  }
end

return vec3