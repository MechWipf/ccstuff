return function ( name, w, h )
    local vMonitor = {}

    vMonitor.type   = "monitor"
    vMonitor.name   = name

    local mon = {
        cursorX     = 1,
        cursorY     = 1,
        cursorBlink = true,
        width       = w,
        height      = h,
        textScale   = 1,

        mirrow     = {},
    }


    function vMonitor.write ( text ) -- ret: nil
        mon.cursorX = mon.cursorX + tostring(text):len()

        for _, mm in pairs( mon.mirrow ) do
            mm.write( text )
        end
    end

    function vMonitor.clear () -- ret: nil

        for _, mm in pairs( mon.mirrow ) do
            mm.clear()
        end
    end

    function vMonitor.clearLine () -- ret: nil

        for _, mm in pairs( mon.mirrow ) do
            mm.clearLine()
        end
    end

    function vMonitor.getCursorPos () -- ret: number x, number y
        return mon.cursorX, mon.cursorY
    end

    function vMonitor.setCursorPos ( x, y ) -- ret: nil
        mon.cursorX = x
        mon.cursorY = y

        for _, mm in pairs( mon.mirrow ) do
            mm.setCursorPos( x, y )
        end        
    end

    function vMonitor.setCursorBlink ( bool ) -- ret: nil
        cursorBlink = bool

        for _, mm in pairs( mon.mirrow ) do
            mm.setCursorBlink( bool )
        end
    end

    function vMonitor.isColour () -- ret: boolean bool
        return true
    end

    function vMonitor.getSize () -- ret: number w, number h
        return mon.width, mon.height
    end

    function vMonitor.scroll ( n ) -- ret: nil
        for _, mm in pairs( mon.mirrow ) do
            mm.scroll( n )
        end
    end

    function vMonitor.setTextColour ( color ) -- ret: nil
        
        for _, mm in pairs( mon.mirrow ) do
            if mm.isColor() then
                mm.setTextColor( color )
            end
        end
    end

    function vMonitor.setBackgroundColour ( color ) -- ret: nil

        for _, mm in pairs( mon.mirrow ) do
            if mm.isColor() then
                mm.setBackgroundColor( color )
            end
        end 
    end

    function vMonitor.setTextScale ( scale ) -- ret: nil
        mon.textScale = scale

        for _, mm in pairs( mon.mirrow ) do
            mm.setTextScale( scale )
        end
    end

    function vMonitor.addMirrow ( name, device ) -- ret: bool ok
        mon.mirrow[name] = device
    end

    function vMonitor.delMirrow ( name ) -- ret: bool ok
        mon.mirrow[name] = nil
    end

    function vMonitor.getAllMirrows() -- ret: table mirrows
        return mon.mirrow
    end

    vMonitor.isColor            = vMonitor.isColour
    vMonitor.setTextColor       = vMonitor.setTextColour
    vMonitor.setBackgroundColor = vMonitor.setBackgroundColour

    return vMonitor
end