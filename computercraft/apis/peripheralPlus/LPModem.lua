local CHANNEL_BROADCAST = 65535
local CHANNEL_REPEAT = 65533


return function ( name, pipe, repeater )
    if not pipe.sendMessage then
        error( "Peripheral is not a valid LogisticsPipe!" )
    end

    local modem = {}
    modem.name = name
    modem.type = "modem"

    function modem.open ()
        return true
    end

    function modem.close ()
        return true
    end

    function modem.isOpen ()
        return true
    end

    function modem.transmit ( nRecipient, nChannel, tMessage )
        local package = {
            nChannel = repeater and CHANNEL_REPEAT or nRecipient,
            nReplyChannel = nChannel,
            tMessage = tMessage,
        }

        if nRecipient == CHANNEL_BROADCAST then
            package = textutils.serialize( package )
            pipe.sendBroadcast( package )
        else
            pipe.sendMessage( repeater or nRecipient, package )
        end
    end

    return modem
end