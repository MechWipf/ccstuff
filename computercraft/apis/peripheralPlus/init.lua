
local old = {
    isPresent   = peripheral.isPresent,
    getType     = peripheral.getType,
    getMethods  = peripheral.getMethods,
    call        = peripheral.call,
    wrap        = peripheral.wrap,
    find        = peripheral.find, -- 1.6+
    getNames    = peripheral.getNames,
}

local peripheral = {}
local virtPeripheral = {}

function peripheral.isPresent ( side )
    if virtPeripheral[ side ] then
        return true
    else
        return old.isPresent( side )
    end
end

function peripheral.getType ( side )
    local v = virtPeripheral[ side ]

    if v then
        return v.type or "virtualPeripheral"
    else
        return old.getType( side )
    end
end

function peripheral.getMethods ( side )
    local v = virtPeripheral[ side ]

    if v then
        local funcList = {}

        for k,v in pairs(v) do
            if type(v) == "function" then
                funcList[ #funcList + 1 ] = k
            end
        end
    else
        return old.getMethods( side )
    end
end

function peripheral.call ( side, method, ... )
    local v = virtPeripheral[ side ]

    if v then
        local f = v[ method ]

        if f and type( f ) == "function" then
            return f( ... )
        else
            error( "No such method: " .. method )
        end
    else
        return old.call( side, method, ... )
    end
end

function peripheral.wrap ( side )
    local v = virtPeripheral[ side ]

    if v then
        return v
    else
        return old.wrap( side )
    end
end

function peripheral.getNames ()
    local list = old.getNames()

    for k, _ in pairs( virtPeripheral ) do
        list[ #list + 1 ] = k
    end

    return list
end

function peripheral.find ( ... )
    return old.find( ... )
end

function peripheral.addVirtual ( p )
    if not p.name then error "Peripheral needs a name attribut." end
    if virtPeripheral[ p.name ] then error "A peripheral with this name already exists." end

    virtPeripheral[ p.name ] = p

    os.queueEvent( "peripheral", p.name )
end

function peripheral.delVirtual ( name )
    if virtPeripheral[ name ] then
        virtPeripheral[ name ] = false
        os.queueEvent( "peripheral_detach", name )
    end
end

getfenv(0).peripheral = peripheral
