
return {
    name = "ME Monitor"
}, {
    -- Modular UI --
    "apis/modularUI/init.lua",
    "apis/modularUI/widgets/base.widget.lua",
    "apis/modularUI/widgets/button.widget.lua",
    "apis/modularUI/widgets/label.widget.lua",
    "apis/modularUI/widgets/panel.widget.lua",
    "apis/modularUI/widgets/progressbar.widget.lua",
    "apis/modularUI/widgets/tbutton.widget.lua",
    "apis/modularUI/widgets/textbox.widget.lua",
    "apis/modularUI/widgets/viewlist.widget.lua",

    -- Misc --
    "apis/event.lua",
    "apis/von.lua",
    "apis/config.lua",
    "apis/queue.lua",
    "apis/sha256.lua",
    "apis/legacy/easyFiles",
    "apis/legacy/utils",
    "apis/legacy/package",

    -- App --
    { true, { "programs/logistics/me_monitor.lua", "apps/me_monitor/me_monitor.lua" } },
    --{ "programs/logistics/me_monitor.lua", "apps/me_monitor/me_monitor.lua" },
    {
        function ( ... )
            os.loadAPI "apis/legacy/utils"
            os.loadAPI "apis/legacy/easyFiles"
            os.loadAPI "apis/legacy/package"

            _G.require = easyFiles.require
            rawset( getfenv(1), "sha256", require "vendor.sha256" )
            rawset( getfenv(1), "von", require "vendor.von" )

            os.run( _G, "apps/me_monitor/me_monitor.lua", ... )
        end,
        "apps/me_monitor/start"
    },
}
