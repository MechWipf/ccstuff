
return {
    name = "Node Monitor"
}, {
    -- Misc --
    "apis/legacy/easyFiles",
    "apis/legacy/utils",
    "apis/vendor/event.lua",

    -- Modular UI --
    "apis/modularUI/init.lua",
    "apis/modularUI/dom.lua",
    "apis/modularUI/helper.lua",
    "apis/modularUI/widgets/base.widget.lua",
    "apis/modularUI/widgets/panel.widget.lua",
    "apis/modularUI/widgets/label.widget.lua",
    "apis/modularUI/widgets/button.widget.lua",

    -- App --
    {  "programs/node_monitor/node_monitor.lua", "apps/node_monitor/node_monitor.lua"  },
    {  "programs/node_monitor/main.form.lua", "apps/node_monitor/main.form.lua"  },
    {  "programs/node_monitor/node.form.lua", "apps/node_monitor/node.form.lua"  },
    {  "programs/node_monitor/node-inner.form.lua", "apps/node_monitor/node-inner.form.lua"  },
    {
        function ( ... )
            local _R = getfenv()
            local shell = _R.shell

            _R.API_PATH = "/apis/"

            os.loadAPI "apis/legacy/utils"
            os.loadAPI "apis/legacy/easyFiles"

            _R.require = _R.easyFiles.require

            shell.run( "apps/node_monitor/node_monitor.lua", ... )
        end,
        "apps/node_monitor/start"
    }
}
