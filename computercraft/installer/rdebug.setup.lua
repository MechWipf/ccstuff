
return {
    name = "Debugger"
}, {
    -- Misc --
    "apis/von.lua",
    "apis/config.lua",
    "apis/json.lua",
    "apis/remoteDebug.lua",
    "apis/legacy/easyFiles",
    "apis/legacy/utils",

    -- App --
    {  "programs/rDebug.lua", "apps/rdebug/rDebug.lua"  },
    {
        function ( ... )
            if not _G.utils then os.loadAPI "apis/legacy/utils" end
            if not require then os.loadAPI "apis/legacy/easyFiles"; _G.require = _G.easyFiles.require end
            if not _G.von then _G.von = require "vendor.von" end
            if not _G.remoteDebug then _G.remoteDebug = require "remoteDebug" end

            os.run( _G, "apps/rdebug/rDebug.lua", ... )
        end,
        "./rdebug"
    },
    {
        "#help",
        function ()
            print( "run 'rdebug help' for more informations." )
        end,
    },
}
