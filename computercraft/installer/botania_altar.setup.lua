
return {
  name = "Botania Runic Altar automation"
}, {
  "apis/event.lua",
  "apis/von.lua",
  "apis/config.lua",
  "apis/rsbundle.lua",
  "apis/inventory.lua",
  "apis/legacy/easyFile",
  "apis/legacy/utils",

  -- App --
  { "programs/botania_altar/altar.lua", "apps/botania/altar.lua" },
  { "programs/botania_altar/runes_crafting_pattern.lua", "apps/botania/runes_crafting_pattern.lua" },
  {
    function ( ... )
      local _R = getfenv()
      local shell = _R.shell
      os.loadAPI "apis/legacy/utils"
      os.loadAPI "apis/legacy/easyFile"

      _G.require = _R.easyFile.require
      _G.von = require "vendor.von"

      shell.run( "apps/botania/altar.lua", ... )
    end,
    "apps/botania/start_altar"
  },
}
