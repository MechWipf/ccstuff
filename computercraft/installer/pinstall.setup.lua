local http, loadstring, select = http, loadstring, select
local dropbox_path = "https://dl.dropboxusercontent.com/u/17040958/ComputerCraft/installer/files/setup_"

local function loadfileOnline ( _sPath )
  local handler = http.get( _sPath )
  if not handler then error( "File not found: " .. _sPath ) end
  local _s = handler.readAll()

  if not _s or #_s == 0 then
    return nil, "File not found: " .. _sPath
  end

  return loadstring( _s )
end

local file = select( 1, ... )
local fn, err = loadfileOnline( dropbox_path .. file )
if err then
  error( err )
end

fn()