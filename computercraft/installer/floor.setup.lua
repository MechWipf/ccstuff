
return {
    name = "Disco Floor"
}, {
    -- App --
    -- { true, { "programs/floor.lua", "apps/disco_floor/floor.lua" } },
    { "programs/floor.lua", "apps/disco_floor/floor.lua" },
    {
        function ( ... )
            os.run( {}, "apps/disco_floor/floor.lua", ... )
        end,
        "apps/disco_floor/start"
    },
}
