const gulp = require('gulp')
const changed = require('gulp-changed')
const exec = require('child_process').execFile
const rename = require('gulp-rename')
const conf = require('./my.config.json')

const local_path = conf.install.local_path
const lua_exec = conf.lua_exec

var src = {}

src.files = [
    'computercraft/apis/**',
    'computercraft/programs/**',
    'computercraft/ressources/**',
    'computercraft/test/**',
    'computercraft/startup/**',
    'computercraft/installer/**',

    'opencomputers/apis/**',
    'opencomputers/programs/**',
    'opencomputers/ressources/**',
    'opencomputers/test/**',
    'opencomputers/startup/**',
    'opencomputers/installer/**'
]

gulp.task('deploy.files', function () {
    var dest = local_path

    return gulp.src(src.files, { base: './' })
        .pipe(changed(dest))
        .pipe(gulp.dest(dest))
})


gulp.task('deploy.legacyApis', function () {
    var src = ['computercraft/apis/utils.lua', 'computercraft/apis/vendor/package.lua', 'computercraft/apis/easyFiles.lua']
    var dest = local_path + 'computercraft/apis/legacy/'

    return gulp.src(src)
        .pipe(rename({ extname: "" }))
        .pipe(changed(dest))
        .pipe(gulp.dest(dest))
})



gulp.task('deploy.ccexplorer', function () {
    var src = 'computercraft/ccexplorer/**'
    var dest = local_path + 'computercraft/programs/ccexplorer/'

    return gulp.src(src)
        .pipe(changed(dest))
        .pipe(gulp.dest(dest))
})


gulp.task('deploy', gulp.series([
    'deploy.files',
    'deploy.ccexplorer',
    'deploy.legacyApis',
]))

gulp.task('index', gulp.series([
    'deploy',
    function IndexOpenComputers (cb) {
        exec(lua_exec, ["makeIndex.lua", local_path + "opencomputers/"], function (err, stdout, stderr) { cb(err) })
    },
    function IndexComputerCraft (cb) {
        exec(lua_exec, ["makeIndex.lua", local_path + "computercraft/"], function (err, stdout, stderr) { cb(err) })
    }
]))

gulp.task('build', gulp.series([
    'deploy',
    'index'
]))

/*
gulp.task('watch', gulp.series([
    'build',
    function () {
        var watcher = gulp.watch(['apis/**', 'ccexplorer/**', 'programs/**', 'installer/**', 'test/**', 'startup/**', 'ressources/**'], gulp.series(['build']))
        watcher.on('change', function (event) {
            console.log('File' + event.path + ' was ' + event.type)
        })

        return watcher
    }
]))
*/