package.path = package.path .. ";C:/Users/mechwipf/src/lua/?.lua"
local utils = require "libs.utils"

local t = {}
local first = true

for line in io.lines [[B:\mechwipf@hotmail.de\Google Drive\Minecraft\item.csv]] do
  if first then first = false
  else
    local name, id, block, mod, class = line:match( "(.+);(.+);(.+);(.+);(.+)" )
    local item = {
      name = name,
      id = id,
      isBlock = block,
      mod = mod,
      class = class
    }

    t[#t+1] = item
  end
end

local f = io.open( "./items.lua", "w" )
f:write( utils.serialize( t ) )
f:close()