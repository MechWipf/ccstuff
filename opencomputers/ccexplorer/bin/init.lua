local getfenv, setmetatable, colors, math, print, io, loadstring, type, setfenv, pcall, pairs, coroutine, os, unpack, parallel, tostring, fs, utils =
      getfenv, setmetatable, colors, math, print, io, loadstring, type, setfenv, pcall, pairs, coroutine, os, unpack, parallel, tostring, fs, utils
local term = setmetatable( {}, { __index = getfenv().term } )

local ceil  = math.ceil
local floor = math.floor
-- local max   = math.max
local min   = math.min

local ccexplorer
local app_id = math.random( 1111, 9999 )

local function class(className)
  return function(prototype)
    local metatable = { __index = prototype }
    prototype.__metatable = metatable
    ccexplorer.env[className] = function(...)
    local self = setmetatable({}, metatable)
    if self.__construct then self:__construct(...) end
      return self
    end
  end
end

local function prepareApps ( _self )
  local App = _self.env.App
  local modularUI = _self.env.modularUI

  local meta_apps = {}
  function meta_apps:add ( obj )
    if obj and obj.title then
      self[ obj.title ] = obj
    end
  end

  local apps = setmetatable( {}, { __index = meta_apps } )

  do
    local app = App( "MessageBox" )

    function app.load ( _, session )
      local cw, ch = term.getSize()
      local w, _ = min( 40, cw - 2 ), 0

      local msg = session.error[1]
      local h = min ( ceil( #msg / (w - 2) ) + 4, ch - 2 )

      local pnMain = modularUI.widgets.panel( floor( cw / 2 - w / 2 ), floor( ch / 2 - h / 2 ), w, h )
      pnMain:setColors( { background = colors.red } )

      for i = 1, h - 4 do
        local text = msg:sub( ( i - 1 ) * ( w - 2 ) + 1, i * ( w - 2 ) )

        text = text .. (" "):rep( ( w - 2 ) - text:len() )

        local lb = modularUI.widgets.label( 1, i, text )
        lb:setColors( { background = colors.cyan } )
        lb:setParent( pnMain )
      end

      if session.error[2] == true then
        debug.log( session.error )
      else
        local text = "Ok"
        local btnOk = modularUI.widgets.button( floor( pnMain.w / 2 - text:len() / 2 ), pnMain.h - 2, text:len(), 1, text )
        btnOk.focus = true
        btnOk:setParent( pnMain )
        function btnOk.onClickLeft ()
          session:close()
        end
      end

      session.form = pnMain
    end

    apps:add( app )
  end

  return apps
end



ccexplorer = {
  env = {},

  load = function ( self )
    local config = require "config"
    local von = require "vendor.von"
    local sha256 = require "vendor.sha256"
    local linq = require "vendor.lualinq"
    local events = require "vendor.event"
    local modularUI = require "modularUI"()

    modularUI.jobs = {}
    modularUI.clearWatchlist()

    modularUI.dom = require "modularUI.dom"
    modularUI.env = { sha256 = sha256 }
    modularUI.dom.widgets = modularUI.widgets

    local path = _G.API_PATH .. "modularUI/widgets/"
    modularUI.loadWidget( path .. "base" )
    modularUI.loadWidget( path .. "button" )
    modularUI.loadWidget( path .. "tbutton" )
    modularUI.loadWidget( path .. "label" )
    modularUI.loadWidget( path .. "panel" )
    modularUI.loadWidget( path .. "textbox" )
    modularUI.loadWidget( path .. "viewlist" )
    modularUI.loadWidget( path .. "progressbar" )

    self.config = config.load( _G.APPDATA_PATH .. "ccexplorer/app.von" )
    self.env.config = config
    self.env.von = von
    self.env.sha256 = sha256
    self.env.linq = linq
    self.env.events = events
    self.env.modularUI = modularUI
  end,

  loadProgram = function ( self )
    self.appdata = _G.APPDATA_PATH .. "ccexplorer/"
    if not fs.exists( self.appdata ) then
      fs.makeDir( self.appdata )
    end

    class "App" {
      __construct = function ( _self, title )
        _self.title = title
      end,
    }

      local _env = {
        config = self.env.config,
        von = self.env.von,
        sha256 = self.env.sha256,
        modularUI = self.env.modularUI
      }
      local load = loadfile( self.config.bin .. "account.lua" )
      setfenv( load, setmetatable( _env, { __index = _G } ) )
      local file_account = load()
      self.account = file_account( self.appdata )
  end,

  main = function ( self )
        debug.log( "--- Starting Program --- " )
        term.clear()
        term.setCursorPos( 1, 1 )

        local modularUI = self.env.modularUI

        -- load os apps
        self.apps = prepareApps( self )
        local apps = self.apps
        local path = self.config.apps[1] -- we are only supporting the main path atm
        self:loadApps( path, { "explorer/init.lua", "login/init.lua", "startmenu/init.lua", "settings/init.lua" } )

        local eventHandler = self.env.events()

        local session_meta

        local function close ( _ )
            debug.log( "Fired event close" )

            os.queueEvent( "_exit" .. app_id )
        end

        local function switchApp ( _self, appName )
            debug.log( utils.format("Switch to app %q", appName) )

            if apps[appName] then
                if appName == "Login" then
                    _self = setmetatable( {}, { __index = session_meta } )
                end

                apps[appName]:load( _self, self )
            else
                debug.log( "->App not found." )
            end

            modularUI.clearWatchlist()
            modularUI.startWatching( _self.form, term )
        end

        local function showMessageBox ( session, message, btnbool )
            debug.log( utils.format("Show messageBox with message %q", { message:gsub( "[\r\n]", " <br> " ) }) )

            debug.log( utils.format("Disable form %q", tostring( session )) )
            session.form._disabled = true

            local errSession = {}
            errSession.error = { message, btnbool }
            errSession.close = function ( _self )
                debug.log( "MessageBox closed" )
                _self.form:kill()

                debug.log( utils.format("Enable form %q", tostring( session )) )
                session.form._disabled = nil
                session.form:invalidate()
            end

            apps.MessageBox:load( errSession )
            modularUI.startWatching( errSession.form, term )
        end

        local function runLegacy ( session, appPath, ... )
            local args = { ... }

            local altSession = {}
            altSession.close = function ()
                debug.log( "Legacy App ended" )

                eventHandler:killCallback( "legacy" )

                modularUI.startWatching( session.form )
                session.form:invalidate()
            end

            local tEnv = {
                modularUI = modularUI,
                shell = getfenv(2).shell,
            }

            tEnv.term = term
            tEnv.term.write = oldWrite

            if args[1] == "/boot" then
                tEnv.fs = fs
                tEnv.fs.isReadOnly = function ()
                    return true
                end
            end

            local co = coroutine.create( function ()
                term.setBackgroundColor( colors.black )
                term.setTextColor( colors.white )
                term.clear()
                term.setCursorPos( 1, 1 )
                local fn = setfenv(function ()
                  local ok, err = utils.trace( tEnv.shell.run( appPath, unpack(args) ) )
                  if err then
                    print( err )
                  end
                end, tEnv)
                fn()
            end )

            local function legacyRun ( ... )
                coroutine.resume( co, ... )
                if coroutine.status( co ) == "dead" then
                    modularUI.clearWatchlist()
                    altSession:close()
                end
            end

            modularUI.killWidget( session.form )
            eventHandler:pushEvent( "legacy", legacyRun )
        end

        session_meta = {
            close = close,
            switchApp = switchApp,
            showMessageBox = showMessageBox,
            runLegacy = runLegacy,
        }

        local session = setmetatable( {}, { __index = session_meta } )
        apps["Login"]:load( session, self )
        modularUI.startWatching( session.form, term )

        parallel.waitForAny(
            function ()
                debug.log( "starting event mainloop" )
                eventHandler:mainLoop()
                debug.log( "event mainloop ended" )
            end,
            function ()
                debug.log( "starting exit listener" )
                os.pullEvent( "_exit" .. app_id )
                debug.log( "exit listener ended" )
            end,
            function ()
                while modularUI.inLoop do _G.sleep(10) end
                debug.log( "starting modularUI mainloop" )
                modularUI.mainLoop( eventHandler )
                debug.log( "modularUI mainloop ended" )
            end
        )

        modularUI.clearWatchlist()
  end,

  loadApp = function ( self, path, appName, env )
    local apps = self.apps

    if not env then
      local env_ = {
        App = self.env.App,
        modularUI = self.env.modularUI,
        sha256 = self.env.sha256,
        print = print,
      }

      env = setmetatable( {}, { __index = setmetatable( env_ ,{ __index = getfenv() } ) } )
    end

    local file = io.open( path .. appName, "r" )
    if not file then
      debug.log( utils.format("%q does not exist.", ( path .. appName )) )
      return false
    end

    local chunk, err = loadstring( file:read( "*a" ) )
    if type(chunk) == "function" then
      env.arg = { path .. appName }
      setfenv( chunk, env )
      local ok, ret = pcall( chunk )
      if ok then
        apps:add( ret )
      else
        debug.log( utils.format("Error while loading app %q\n->error: %s", { appName, ret }) )
      end
    else
      debug.log( utils.format("error: %q -> %s", { ( path .. appName ), err }) )
    end
  end,

  loadApps =  function ( self, path, list, env )
    for _, v in pairs( list ) do
      debug.log( utils.format("Loading app ... %q", v) )
      self:loadApp( path, v, env )
    end
  end,
}

local ok, err = utils.trace(
  function ()
    debug.log "-- ccexplorer load --"
    ccexplorer:load()
    ccexplorer:loadProgram()
    ccexplorer:main()
  end
)

if not ok then
    term.setBackgroundColor( colors.blue )
    term.setTextColor( colors.white )
    term.clear()
    term.setCursorPos( 1, 1 )

    local sErr = ("Error: %s"):format( err )
    print( sErr )
    debug.log( sErr )

    io.read "*l"
else
    debug.log "clean shutdown"
end

term.setBackgroundColor( colors.black )
term.setTextColor( colors.white )
term.clear()
term.setCursorPos( 1, 1 )

debug.log "-- ccexplorer close --"
