-- ****************************** --
-- User: MechWipf
-- Date: 03.06.2015
-- Time: 23:08
-- ****************************** --

local fs, io, sha256, von, content, setmetatable =
      fs, io, sha256, von, content, setmetatable

return function ( appdata )
    local accounts

    if not fs.exists( appdata .. "/accounts.von" ) then
        local f = io.open( appdata .. "/accounts.von", "w" )

        local data = {
            Admin = {
                Pass = sha256( "ILikeCookies@2012" ),
                Groups = { "admin", "default" },
            },

            Guest = {
                Pass = sha256( "guest" ),
                Groups = { "default" },
            },
        }

        content = von.serialize( data )
        f:write( content )
        f:close()

        accounts = data
    end

    if not accounts then
        local f = io.open( appdata .. "/accounts.von", "r" )
        local data = f:read( "*a" )
        accounts = von.deserialize( data )
    end

    local currentAccount
    local module = {}

    function module.login ( username, password )
        if not accounts[username] then return false end
        local success = accounts[username].Pass == password

        if success then
            currentAccount = accounts[username]
        end

        return success
    end

    function module.getCurrentAccount ()
        return currentAccount
    end

    return setmetatable( {}, { __index = module, __metatable = 'nope' } )
end
