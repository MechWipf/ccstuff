local von, fs, io, term, modularUI, colors, pairs, print, pcall, utils =
      von, fs, io, term, modularUI, colors, pairs, print, pcall, utils

local app = App( "Startmenu" )

local function getConfig ()
    local appdata = app.cce.appdata
    local f_startmenu = appdata .. "/startmenu.von"
    local links

    if not fs.exists( f_startmenu ) then
      local data = {
        {
            name = "Explorer",
            app = "Explorer"
        },
        {
            name = "Dashboard",
            app = "Dasboard"
        },
        {
            name = "Lua Console",
            app = "/rom/programs/lua",
            legacy = true
        }
      }

      local content = von.serialize( data )
      utils.fileWrite( f_startmenu, content )
      links = data
    end

    if not links then
        local ok, err = pcall( function ()
            local data = utils.fileRead( f_startmenu )
            links = von.deserialize( data )
        end )

        if not ok then
            print( err )
            fs.delete( f_startmenu )
            links = getConfig()
        end
    end

    return links
end

function app:loadUI ( session )
    local cw, ch = term.getSize()

    local pnBg = modularUI.widgets.panel( 1, 1, cw, ch )
    pnBg:setColors( { background = colors.orange } )

    local pnMain = modularUI.widgets.panel( 1, 1, cw - 2, ch - 2 )
    pnMain:setColors( { background = colors.gray } )
    pnMain:setParent( pnBg )

    local text = "Logout"
    local btnLogout = modularUI.widgets.button( pnMain.w - text:len() - 1, pnMain.h - 2, text:len(), 1, text )
    btnLogout:setParent( pnMain )
    function btnLogout.onClickLeft ()
      session:switchApp( "Login" )
    end

    local vlApps = modularUI.widgets.viewlist( 1, 1, pnMain.w - 2, pnMain.h - 4, "item" )
    vlApps.hideScrollbar = true
    vlApps:setParent( pnMain )

    for i, link in pairs( self.config ) do
      local offset = (i-1)*3

      local text = link.name
      local btn = modularUI.widgets.button( 1, 1, pnMain.w - 3, 1, text )
      vlApps:add( btn )

      function btn.onClickLeft ()
        if not link.legacy then
            session:switchApp( link.app )
        else
            session:runLegacy( link.app )
        end
      end
    end

    return pnBg
end

function app:load ( session, cce )
    app.cce = cce

    app.config = getConfig()

    if not self.form then
      self.form = self:loadUI( session )
    end

    self.form:invalidate()

    session.form = self.form
end

return app
