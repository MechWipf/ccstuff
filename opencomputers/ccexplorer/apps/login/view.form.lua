return panel {
  name = "pnBack",
  head = { 1, 1 },
  calcW = "@pnBack.calcW",
  calcH = "@pnBack.calcH",
  color = { background = colors.orange },
  {
    panel {
      name = "pnLogin",
      head = { 2, 2, 0, 9 },
      calcW = "=self.parent.w - 4",
      calcY = "=self.parent.h / 2 - self.h / 2",
      {
        label {
          head = { 0, 0, "Login" },
          calcX = "=self.parent.w / 2 - self.text:len() / 2",
        },
        label {
          head = { 1, 1, "Username:" },
        },
        textbox {
          name = "tbUser",
          head = { 1, 2, 1, 1 },
          calcW = "=self.parent.w - 2",
        },
        label {
          head = { 1, 4, "Password:" },
        },
        textbox {
          name = "tbPass",
          head = { 1, 5, 1, 1 },
          calcW = "=self.parent.w - 2",
          init = "=self:setProtected(true)"
        },
        button {
          name = "btnLogin",
          head = { 1, 7, 0, 1, "Accept" },
          calcW = "=self.text:len()",
          onClickLeft = "@btnLogin.onClickLeft",
        },
        button {
          name = "btnCancel",
          head = { 0, 7, 0, 1, "Shutdown" },
          calcW = "=self.text:len()",
          calcX = "=self.parent.w - self.text:len() - 2",
          onClickLeft = "@btnCancel.onClickLeft",
        },
      }
    }
  }
}