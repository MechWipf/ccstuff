local _R = getfenv()
local    modularUI,    term =
      _R.modularUI, _R.term
local app = _R.App( "Login" )

function app:loadUI ( session )
    local env
    env = {
        ["pnBack.calcW"] = function ( _ ) return ({term.getSize()})[1] end,
        ["pnBack.calcH"] = function ( _ ) return ({term.getSize()})[2] end,
        ["btnLogin.onClickLeft"] = function ( item )
            local username = env.tbUser:getText() or ""
            item:setFocus( env.tbUser )
            if self.account.login( username, env.tbPass:getText() ) then
                session:switchApp( "Startmenu" )
            else
                -- session does not hold the login form, so we have to use self
                session.showMessageBox( { form = self.form }, "Wrong username password combination." )
            end
        end,
        ["tbPass.onEnter"] = function ( _ )
            env.btnLogin:onClickLeft()
        end,
        ["btnCancel.onClickLeft"] = function ( _ )
            session:close()
        end,
    }

    modularUI.dom.buildFile( self.path .. "view.form.lua", env )

    env.tbUser.next = env.tbPass
    env.tbPass.next = env.btnLogin
    env.btnLogin.next = env.btnCancle
    env.btnCancel.next = env.tbUser

    self.tbPass = env.tbPass
    self.tbUser = env.tbUser

    env.tbUser.focus = true

    return env.pnBack
end

function app:load ( session, cce )
    self.path = arg[1]:match( "^(.*/).+$" )

    if not self.form then
        self.form = self:loadUI( session )
    end

    self.form:refresh()

    self.tbPass:clear()
    self.tbUser:clear()

    self.tbUser:setText( "Guest" )
    self.tbPass:addText( "guest" )

    if cce then self.account = cce.account end

    session.form = self.form
end

return app
