package.loaded.vfs = nil

local vfs = require "vfs"
local fs = require "filesystem"

if not _G.proxy then
  local proxy = vfs.getProxy(
    "http://mechwipf.dnshome.de/minecraft/opencomputers/",
    "http://mechwipf.dnshome.de/minecraft/opencomputers/base.von"
  )

  proxy:getIndex()
  fs.mount( proxy, "/mnt/dropbox" )

  package.path = package.path .. ";/mnt/dropbox/apis/?.lua;/mnt/dropbox/apis/?/init.lua"

  _G.proxy = proxy
else
  _G.proxy:getIndex()
end