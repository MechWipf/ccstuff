local computer = require "computer"

package.loaded.mui = nil
while true do
  local utils = require "utils"
  local mui = require "mui"
  local muiDom = require "mui.dom"
  local event = require "event"
  local term = require "term"
  local component = require "component"
  local Config = require "config".Config

  local widgetPath = "/mnt/dropbox/apis/mui/widgets/"

  mui.loadWidget( widgetPath .. "base" )
  mui.loadWidget( widgetPath .. "panel" )
  mui.loadWidget( widgetPath .. "label" )
  mui.loadWidget( widgetPath .. "button" )

  _G.sha256 = require "vendor.sha256"
  mui.loadWidget( widgetPath .. "textbox" )
  mui.jobs = {}

  muiDom.widgets = mui.widgets

  local panel = mui.widgets.panel{ x = 1, y = 1, w = 10, h = 10 }
  panel:refresh()
  -- panel:addChild( mui.widgets.panel{ x = 1, y = 1, w = 1, h = 1 } )
  mui.startWatching( panel )
  os.sleep(0.1)

  local mainLoop = mui.getMainLoop()
  for _ = 1, 10 do
    mainLoop()
  end

  os.sleep(0.2)
  panel:kill()
  print( computer.freeMemory() )
end