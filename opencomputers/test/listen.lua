
local modems = utils.getDevices "modem"
local channel = select( 1, ... )

for j, modem in pairs( modems ) do
  local channelRange = channel - 128 * j
  print( ("Listen on modem %d from channel %d to %d"):format( j, channelRange, channelRange + 128 ) )
  
  local l = {}
  for i = 1, 128 do
    modem.open( channelRange + i )
    l[#l+1] = channelRange + i
  end

  debug.log( ("Open [%s]"):format( table.concat( l, "," ) ) )
end

while true do
  debug.log( os.pullEvent "modem_message" )
end