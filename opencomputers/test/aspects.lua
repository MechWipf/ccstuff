--[[
    Logistics Pipes API
        .getCraftableItems
        .setTurtleConnect
        .getUnlocalizedName
        .getRouterId
        .getItemIdentifierIDFor
        .canAccess
        .getItemAmount
        .getAvailableItems
        .getItemDamage
        .makeRequest
        .getItemID
        .getType
        .getNBTTagCompound
]]

function getAspects ( nbt )
    if not nbt then return false end
    if not nbt.value then return false end
    if not nbt.value.Aspects then return false end

    local ret = {}

    for _, aspect in pairs( nbt.value.Aspects.value ) do
        ret[#ret + 1] = { aspect.value.key.value, aspect.value.amount.value }
    end

    return ret
end

function main ()
    local pipe = getDevices( "LogisticsPipes:Request" )[1]
    local items = pipe.getAvailableItems()

    for key, item in pairs( items ) do
        local NBTCompound = pipe.getNBTTagCompound( item[1] )
        local aspects = getAspects( NBTCompound )

        if aspects then
            print( string.format( "%s got %s", pipe.getUnlocalizedName( item[1] ), textutils.serialize( aspects ):gsub("[\n\t ]","") ) )
            sleep( .2 )
        end
    end
end


main()