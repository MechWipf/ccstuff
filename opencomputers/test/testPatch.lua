_G.sleep = function () end

package.path = "./apis/?.lua;./apis/?/init.lua;b:/mechwipf@hotmail.de/Dropbox/Public/ComputerCraft/apis/?.lua;b:/mechwipf@hotmail.de/Dropbox/Public/ComputerCraft/apis/?/init.lua;" .. package.path

local http
http = {
  curlPath = [["C:\Program Files\Git\mingw64\bin\curl.exe" -s -H 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7']],

  get = function ( url )
    local handler = {}

    local f = assert( io.popen( http.curlPath.." "..url , "r" ) )

    function handler.readAll ()
      return f:read "*a"
    end

    function handler.close ()
      return f:close()
    end

    return handler
  end,
}

_G.http = http