local rsbundle = require "rsbundle"
local app = {}

rsbundle.setSide "bottom"
rsbundle.toggle "yellow" -- we don't use yellow, but this reduces lag
pcall(
  function ()
    while true do
      app.rs = rs.getAnalogInput "back"

      if app.rs == 2 then
        sleep(1)
        rsbundle.pulse( "red", 1 )
        rsbundle.pulse( "green", 1 )
        sleep(2)
      elseif app.rs ~= 1 then
        rsbundle.pulse( "brown", 1 )
        app.ac = false
      end
    end

  end
)
