local computer = require "computer"

panel {
  name = "pnMain",
  x = 1, y = 1,
  calcW = "@window.w",
  calcH = "@window.h",
  color = { background = "WindowFrame" },
  {
    label {
      x = 1, y = 0, text = "Nanomachines Manager",
      calcX = "=math.floor(self.parent.w / 2 - self.text:len() / 2)",
      color = { background = "WindowFrame" },
    },
    label {
      x = 0, y = 0, text = "RAM",
      color = { background = "WindowFrame" },
    },
    label {
      x = 5, y = 0, text = "",
      color = { background = "WindowFrame" },
      update = function (self) self.text = tostring(computer.freeMemory()) self:invalidate() end
    },
    button {
      y = 0, w = 1, h = 1, text = "x",
      calcX = "=self.parent.w - 1",
      onClickLeft = "@window.exit",
      color = { button = "Red" }
    },
    panel {
      name = "pnContent",
      x = 0, y = 1,
      calcW = "=self.parent.w",
      calcH = "=self.parent.h - 1",
      color = { background = "WindowBackground" },
      {
        panel {
          x = 3, y = 17,
          calcH = "=self.parent.h - self.y - 1",
          calcW = "=self.parent.w - 6",
          {
            label {
              x = 1, y = 1,
              text = "Energy"
            },
            label {
              x = 14, y = 1,
              calcW = "=self.text:len()",
              text = "@data.energy",
            },
            label {
              x = 19, y = 1,
              text = "Max Inputs",
            },
            label {
              x = 31, y = 1,
              text = "@data.inputsMax",
            },
            label {
              x = 19, y = 3,
              text = "Save Inputs",
            },
            label {
              x = 31, y = 3,
              text = "@data.inputsSave",
            },
            button {
              text = "Disable All",
              calcW = "=self.text:len()",
              calcX = "=self.parent.w - self.w",
              onClickLeft = "@window.disableAll",
            },
            button {
              y = 2, w = 11,
              text = "Status",
              calcX = "=self.parent.w - self.w",
              onClickLeft = "@window.checkAll",
            },
            button {
              name = "btnDetection",
              y = 4, w = 11,
              text = "Detect",
              calcX = "=self.parent.w - self.w",
              onClickLeft = "@window.toggleDetect",
              color = { button = "Inactive" }
            },
            button {
              y = 4, w = 6,
              text = "Reset",
              calcX = "=self.parent.w - 13 - self.w",
            },
          }
        }
      }
    }
  }
}