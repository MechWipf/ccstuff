-- package.loaded.mui = nil
-- package.loaded["mui.dom"] = nil

local utils = require "utils"
local mui = require "mui"
local muiDom = require "mui.dom"
local event = require "event"
local term = require "term"
local component = require "component"
local Config = require "config".Config

local widgetPath = "/mnt/dropbox/apis/mui/widgets/"

mui.loadWidget( widgetPath .. "base" )
mui.loadWidget( widgetPath .. "panel" )
mui.loadWidget( widgetPath .. "label" )
mui.loadWidget( widgetPath .. "button" )

_G.sha256 = require "vendor.sha256"
mui.loadWidget( widgetPath .. "textbox" )

muiDom.widgets = mui.widgets

local appName = "test_" .. math.random( 10000,99999 )
local appFolder = os.getenv( "_" ):match( "^(.*/).+$" )
local appRunning = true
local app = {
  error = false,
  config = Config( "/home/nbm.von", { version = "1.0.0", effects = { active = {}, status = {} } } ),
}

app.effects = {
  active = app.config.effects.active.get(),
  status = app.config.effects.status.get(),
}

local function loadGUI ()
  local _, _, c_w, c_h = term.getGlobalArea()

  local form
  form = {
    ["window.w"] = function () return c_w end,
    ["window.h"] = function () return c_h end,
    ["window.exit"] = function () event.push( "exit_" .. appName ) end,
    ["window.onChanged"] = function ( self, enter )
      if enter then
        form.lbHello:setColor( "text", tonumber( self:getText(), 16 ) )
        form.lbHello:invalidate()
      end
    end,
    ["window.disableAll"] = function ()
      for i = 1, app.totalInputCount do
        app.sendMessage( "setInput", i, false )
      end
    end,
    ["window.checkAll"] = function ()
      for i = 1, app.totalInputCount do
        app.sendMessage( "getInput", i )
        app.effectItemsSetStatus( i, "pending" )
      end
    end,
    ["window.toggleDetect"] = function ()
      app.detectionEnable( not app.detectionMode )
    end,
    ["items"] = {},
    ["data.energy"] = "0%",
    ["data.inputsSave"] = "0",
    ["data.inputsMax"] = "0",
  }

  muiDom.loadFile( appFolder .. "main.form.lua", form )


  local ButtonPanel, err = muiDom.loadFileCached( appFolder .. "buttonPanel.form.lua" )
  if not ButtonPanel then
    error( err )
  end

  local effectItems = {}
  for i = 0, 19 do
    local x = i % 5
    local y = math.floor( i / 5 )

    local itemEnv = {
      ["title"] = "Effect " .. i + 1,
      ["effectTitle1"] = "unknown",
      ["effectTitle2"] = "",
      ["activate"] = function ( self )
        if not app.detectionMode then
          local active = not app.effects.active[i+1]
          self:setColor( "button", "Yellow" )

          app.sendMessage( "setInput", i + 1, active == true )
        end
      end,
    }

    local item = ButtonPanel( itemEnv )
    item.x = 3 + x * ( item.w + 1 )
    item.y = 1 + y * ( item.h + 1 )
    item:enable( false )

    effectItems[i+1] = itemEnv

    form.pnContent:addChild( item )
  end

  function app.effectItemsRedraw ( count )
    for i = 1, 20 do
      effectItems[i].pnEffectItem:enable( i <= count )
    end
  end

  function app.effectItemsSetStatus ( id, status )
    if status == true then
      effectItems[id].btnEffect:setColor( "button", "Button" )
    elseif status == "pending" then
      effectItems[id].btnEffect:setColor( "button", "Yellow" )
    else
      effectItems[id].btnEffect:setColor( "button", "Inactive" )
    end
  end

  local function setEffects ( id, effects )
    if not effects then return end

    if effects[1] == "" then
      effectItems[id]["effectTitle1"] = "None"
      effectItems[id]["effectTitle2"] = ""
      effectItems[id].lbEffectTitle1:setColor( "text", "Inactive" )
    else
      for i = 1, 2 do
        if effects[i] then
          local type, effect = effects[i]:match( "(.*)%.(.*)" )
          effectItems[id]["effectTitle" .. i] = effect
          effectItems[id]["lbEffectTitle" .. i]:setColor( "text", type == "effect" and "Yellow" or "Text" )
        end
      end
    end
  end

  function app.effectItemsSetEffects ( id, rawEffect )
    local effects = utils.explode( rawEffect:sub(2,-2), "," )
    app.effects.status[id] = effects
    setEffects( id, effects )
    app.config.save()
  end

  function app.effectItemsReload ( id )
    local effects = app.effects.status[id] or {}
    app.effectItemsSetStatus( id, app.effects.active[id] )
    setEffects( id, effects )
  end

  function app.detectionEnable ( status )
    app.detectionMode = status == true
    app.form.btnDetection:setColor( "button", status == true and "Button" or "Inactive" )
  end

  function app.reloadEffects ()
    for id, active in pairs( app.effects.active ) do
      app.effectItemsReload( id )
      app.effectItemsSetStatus( id, active == true )
    end
  end

  app.form = form
  form.pnMain:refresh()

  mui.startWatching( form.pnMain )
  print "Loaded GUI"
end

local function runEventHandler ()
  local function eventGUI ( eventName, ... )
    if not appRunning then return false end

    if eventName == "touch" then
      mui.handlerClick( select( 4, ... ), select( 2, ... ), select( 3, ... ), select( 1, ... ) )
    elseif eventName == "key_down" then
      mui.handlerKeyDown( select( 2, ... ), select( 3, ... ) )
    elseif eventName == "key_up" then
      mui.handlerKeyUp( select( 2, ... ), select( 3, ... ) )
    elseif eventName == "scroll" then
      mui.handlerScroll( select( 2, ... ), select( 3, ... ), select( 4, ... ), select( 5, ... ) )
    elseif eventName == "drag" then
      mui.handlerDrag( select( 4, ... ), select( 2, ... ), select( 3, ... ), select( 5, ... ) )
    elseif eventName == "drop" then
      mui.handlerDrop( select( 4, ... ), select( 2, ... ), select( 3, ... ), select( 5, ... ) )
    end

  end

  event.listen( "touch", eventGUI )
  event.listen( "key_down", eventGUI )
  event.listen( "key_up", eventGUI )
  event.listen( "scroll", eventGUI )
  event.listen( "drag", eventGUI )
  event.listen( "drop", eventGUI )

  event.listen( "modem_message", app.modemEvent )

  return event.pull( "exit_" .. appName )
end

function app.modemEvent ( _, _, _, _, _, header, command, ... )
  if not appRunning then return false end
  if header ~= "nanomachines" then return end
  if log then log{ command, ... } end

  if command == "totalInputCount" then
    app.effectItemsRedraw( select( 1, ... ) )
    app.totalInputCount = select( 1, ... )
  elseif command == "power" then
    app.form["data.energy"] = string.format( "%d%%", select( 1, ... ) / select( 2, ... ) * 100 )
  elseif command == "input" then
    app.effectItemsSetStatus( select( 1, ... ), select( 2, ... ) )
    app.effects.active[select( 1, ... )] = select( 2, ... )
  elseif command == "effects" then
    if app.detectionMode then
      for id, isActive in pairs( app.effects.active ) do
        if isActive then
          app.effectItemsSetEffects( id, select( 1, ... ) )
        end
      end
    end
  elseif command == "safeActiveInputs" then
    app.form["data.inputsSave"] = tostring(select( 1, ... ))
  elseif command == "maxActiveInputs" then
    app.form["data.inputsMax"] = tostring(select( 1, ... ))
  else
    if log then log{ command, ... } end
  end
end

app.delay = os.time()
app.messages = {}
function app.sendMessage ( ... )
  app.messages[#app.messages+1] = { ... }
end

function app.jobSendMessages ()
  if app.delay < os.time() and #app.messages > 0 then
    app.modem.broadcast( 1, "nanomachines", table.unpack( table.remove( app.messages, 1 ) ) )
    app.delay = os.time() + 100
  end
end

function app.main ()

  app.modem = component.modem
  app.modem.open( 1 )
  app.sendMessage( "setResponsePort", 1 )
  app.sendMessage( "getTotalInputCount" )

  while not app.totalInputCount do coroutine.yield() end

  app.reloadEffects()
  app.sendMessage( "getSafeActiveInputs" )
  app.sendMessage( "getMaxActiveInputs" )

  local statusDelay = 0
  while appRunning do
    coroutine.yield()
    if statusDelay < os.time() then
      app.sendMessage( "getPowerState" )
      statusDelay = os.time() + 1000
    end

    if app.detectionMode then
      for id = 1, app.totalInputCount do
        if app.effects.active[id] == true or app.effects.active[id] == nil then
          app.effectItemsSetStatus( id, "pending" )
          app.sendMessage( "setInput", id, false )
        end

        while app.effects.active[id] ~= false do coroutine.yield() end
      end

      for id, _ in pairs( app.effects.active ) do
        local effects = app.effects.status[id]
        if effects == nil then
          app.effectItemsSetStatus( id, "pending" )
          app.sendMessage( "setInput", id, true )
          app.sendMessage( "getActiveEffects" )
          while app.effects.status[id] == nil do coroutine.yield() end
          break
        end
      end

      if #app.effects.status == app.totalInputCount then
        for id, isActive in pairs( app.effects.active ) do
          if isActive then
            app.effectItemsSetStatus( id, "pending" )
            app.sendMessage( "setInput", id, false )
          end
        end

        app.detectionEnable( false )
      end
    end
  end
end

local function mainLoop ()
  loadGUI()
  local p = mui.flashPalette()
  mui.jobs = { app.jobSendMessages, muiDom.jobWatch }

  local _errorHandler = event.onError
  event.onError = function ( err )
    app.error = { err, debug.traceback() }
    event.push( "exit_" .. appName, true )
  end

  local pId
  local cancelProgram = function ()
    event.cancel( pId )
  end

  local mId
  local cancelMui = function ()
    event.cancel( mId )
  end

  local coMain = coroutine.create( app.main )

  pId = event.timer( 0.100, function ()
    local status = coroutine.status( coMain )

    if status == "suspended" then
      local ok, err = pcall( coroutine.resume, coMain )

      if not ok then
        app.error = { err, debug.traceback() }
        event.push( "exit_" .. appName, true )
      end
    else
      cancelProgram()
    end
  end, 1e999 )

  local muiMainLoop = mui.getMainLoop()
  mId = event.timer( 0.100, function ()
    local ok, err = pcall( muiMainLoop )

    if not ok then
      app.error = { err, debug.traceback() }
      event.push( "exit_" .. appName, true )
    end
  end, 1e999 )

  local _, err = pcall( function ()
    local _, err = runEventHandler()
    if err then return table.concat( app.error, "\n" ) end
  end )

  mui.jobs = {}
  mui.clearWatchlist()
  mui.inLoop = false
  appRunning = false
  cancelProgram()
  cancelMui()
  event.onError = _errorHandler

  p.unload()
  term.gpu().setBackground( 0x000000 )
  term.gpu().setForeground( 0xFFFFFF )
  term.clear()

  if err then
    print( err )
  end
end

mainLoop()