return panel {
  name = "pnEffectItem",
  w = 14, h = 3,
  {
    button {
      name = "btnEffect",
      text = "@title",
      calcW = "=self.parent.w",
      onClickLeft = "@activate",
      color = { button = "Inactive" }
    },
    label {
      name = "lbEffectTitle1",
      y = 1,
      text = "@effectTitle1",
    },
    label {
      name = "lbEffectTitle2",
      y = 2,
      text = "@effectTitle2",
    }
  }
}