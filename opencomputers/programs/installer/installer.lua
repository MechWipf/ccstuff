local von, utils, type, pairs, print, shell, dofile, unpack, string, fs, error, io, loadfile =
      von, utils, type, pairs, print, shell, dofile, unpack, string, fs, error, io, loadfile

if not package then package = require "vendor.package" end
local package = package
local programPath = shell.getRunningProgram():match("^(.*/).+$")

local function getData ( src, config )
  local data = {}

  for k, v in pairs( config ) do
      local compile = false
      if type(v) == "table" and v[1] == true then
          compile = true
          v = v[2]
      end

      if type(v) == "string" then
          print("Loading " .. src .. v)

          if compile then
              local func = string.dump( loadfile( src .. v ) )
              data[v] = { "func", utils.toHex( func ) }
          else
              data[v] = { "file", utils.fileRead( src .. v ) }
          end

      elseif type(v) == "table" then
          if type(v[1]) == "function" then
              print( "Compiling " .. v[2] )
              local func = string.dump( v[1] )
              data[v[2]] = { "func", utils.toHex( func ) }
          elseif type(v[1]) == "string" and v[1] == "#help" then
            print( "Compiling help" )
            local func = string.dump( v[2] )
            data["#help"] = { "help", utils.toHex( func ) }
          else
              print("Loading " .. src .. v[1])
              if compile then
                  local func = string.dump( loadfile( src .. v[1] ) )
                  data[v[2]] = { "func", utils.toHex( func ) }
              else
                  data[v[2]] = { "file", utils.fileRead( src .. v[1] ) }
              end
          end
      end
  end

  return data
end

local cmd = {
    help = function ()
        print "Usage: "
        print "installer create <config> <src> <install>"
    end,

    create = function ( args )
        local _, config, src, path = unpack( args )
        local src = src or "/"
        local path = path or "."
        local meta = ""
        local setup_name = "setup"

        if not fs.exists( config ) then
          print( ("Config file: %q not found"):format( config ) )
          io.read "*l"
          return
        end
        meta, config = dofile( config )

        io.write( "Setup file name: " )
        local _setup_name = io.read( "*l" )
        if _setup_name and _setup_name ~= "" then
            setup_name = _setup_name
        end

        local data = getData( src, config )
        -- Get content and encode it
        local content = von.serialize( data )
        content = content:gsub( "%[%[", "`[`" )
        content = content:gsub( "%]%]", "`]`" )

        -- Get von and encode it
        local s_von = utils.fileRead( src .. "apis/vendor/von.lua" )
        s_von = utils.toHex( s_von )

        --utils.fileWrite( shell.resolve( path .. "/data.von" ), content )
        --utils.fileWrite( shell.resolve( path .. "/von.lua" ), utils.fileRead( src .. "apis/von.lua" ) )

        -- get setup path
        local setup_path = ( programPath .. "setup_onefile.lua" ):gsub( "[/\\]+", "/" )

        -- get temporary setup file
        local tmp_setup = utils.fileRead( setup_path )
        -- write von and the content into the setup file
        --print( string.len(s_von), " ", string.len( content ), " ", tmp_setup )
        tmp_setup = string.format( tmp_setup, s_von, content, "%q" )
        -- write the temporary file to disk
        utils.fileWrite( shell.resolve( path .. "/tmp_setup" ), tmp_setup )
        --try to compile setup
        local setup, err = loadfile( shell.resolve( path .. "/tmp_setup" ) )
        if not setup then error( "Could not create setup: " .. err ) end
        -- clean up
        fs.delete( shell.resolve( path .. "/tmp_setup" ) )

        -- create binary
        setup = utils.createBinaryRunner( setup )
        utils.fileWrite( shell.resolve( path .. "/" .. setup_name ), setup )
    end,

    disk = function ( args )
      local _, config, src, path = unpack( args )
      local src = src or "/"
      local path = path or "."
      local meta = ""
      local setup_name = "setup"

      if not fs.exists( config ) then
        print( ("Config file: %q not found"):format( config ) )
        io.read "*l"
        return
      end
      meta, config = dofile( config )

      io.write( "Setup file name: " )
      local _setup_name = io.read( "*l" )
      if _setup_name and _setup_name ~= "" then
        setup_name = _setup_name
      end

      local data = getData( src, config )

      -- Get content and encode it
      local content = von.serialize( data )
      utils.binaryWrite( shell.resolve( path .. "/data.pack" ), package.compress( content ) )

      -- Get von
      utils.fileWrite( shell.resolve( path .. "/apis/von.lua" ), utils.fileRead( src .. "apis/von.lua" ) )
      -- Get package
      utils.fileWrite( shell.resolve( path .. "/apis/package" ), utils.fileRead( src .. "apis/legacy/package" ) )
      -- Get utils
      utils.fileWrite( shell.resolve( path .. "/apis/utils" ), utils.fileRead( src .. "apis/legacy/utils" ) )

      -- get setup path
      local setup_path = ( programPath .. "setup_disk.lua" ):gsub( "[/\\]+", "/" )
      --try to compile setup
      local setup, err = loadfile( setup_path )
      if not setup then error( "Could not create setup: " .. err ) end

      -- create binary
      setup = utils.createBinaryRunner( setup )
      utils.fileWrite( shell.resolve( path .. "/" .. setup_name ), setup )
    end
}

local function main ( args )
    if cmd[args[1]] then
        cmd[args[1]]( args )
    else
        cmd["help"]()
    end
end

local args = {...}
local ok, err = utils.trace( function() main(args) end )

if not ok then
    print( ("Error: %s"):format( err ) )
    io.read "*l"
end
