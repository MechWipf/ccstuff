local request = require "internet".request
local fs = require "filesystem"

local function dl ( url )
  return function ( path )
    local sb = {}
    for chunk in request( url ) do
      sb[#sb+1] = chunk
    end
    local file = table.concat( sb )

    local h = io.open( path, "w" )
    h:write( file ) h:close()
  end
end

fs.makeDirectory "/home/lib/vendor"
dl "http://mechwipf.dnshome.de/minecraft/opencomputers/apis/vfs.lua"        "/home/lib/vfs.lua"
dl "http://mechwipf.dnshome.de/minecraft/opencomputers/apis/vendor/von.lua" "/home/lib/vendor/von.lua"
dl "http://mechwipf.dnshome.de/minecraft/opencomputers/test/test_oc_vfs.lua" "./mount_dropbox.lua"
