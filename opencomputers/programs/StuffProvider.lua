print "loading StuffProvider"

local config     = dofile "/appdata/StuffProvider/config.lua"

local pInterface = peripheral.wrap( config.interface )
local pStorage   = peripheral.wrap( config.storage )
local pProvide   = config.provide

if not pInterface then error "No interface found." end
if not pStorage then error "No storage found." end

local function dumpItem ( slot )
	local item = pStorage.getStackInSlot( slot )
	local count = pStorage.pushItem( config.dumpDir, slot, 64 )
	
	if count > 0 then
		print( string.format( "Dumping %d %q", count, item.name ) )
	end

end

local function provideItem ( itemP, slot )
	local item = pStorage.getStackInSlot( slot )
	
	if item and item.id ~= itemP.id then
		dumpItem( slot )
		item = pStorage.getStackInSlot( slot )			
	end

	local c = itemP.qty - ( item ~= nil and item.qty or 0 )
	
	for iSlotI = 1, pInterface.getInventorySize() do
		local itemI = pInterface.getStackInSlot( iSlotI )
		
		if itemI and itemI.id == itemP.id and c > 0 then
			local count = pInterface.pushItemIntoSlot( config.pushDir, iSlotI, c, slot )
			print( string.format( "Providing %d %q", count, itemI.name ) )
			break
		end
	end
end

local function run ()
	while true do
		for slot, itemP in pairs( pProvide ) do
			if itemP.id ~= 0 then
				provideItem( itemP, slot )
			elseif pStorage.getStackInSlot( slot ) then
				dumpItem( slot )
			end
		end
		sleep( 0.01 )
	end
end

local function main ()
	while true do
		print "starting StuffProvider"

		ok, err = pcall( run )

		if not ok then
			if not string.find( err, "Too long without yielding" ) then
				print "An error occurred and StuffProvider had to be shut down."
				if config.debug then print( err ) end
				io.read()
				return false
			end
		end
		sleep(5)
	end
end

main()