local config = require "config"

local app = {}
app.configPath = "appdata/moveItem/config.von"
app.config = {
  inputInventory = "unknown",
  outputInventory = "unknown",
  movementDirection = "unknown"
}

local function askConfig ()

  -- get input inventory
  while app.config.inputInventory == "unknown" do
    term.write "Input Inventory> "
    local inInv = io.read "*l"
    local devices = utils.getDevices( inInv )
    print( ("Found %d %s."):format( #devices, inInv ) )

    if #devices == 1 then
      print "Set found device as Input"
      app.config.inputInventory = devices[1]
      config.write( app.configPath, app.config )
    end
  end

  -- get output inventory
  while app.config.outputInventory == "unknown" do
    term.write "Output Inventory> "
    local outInv = io.read "*l"
    local devices = utils.getDevices( outInv )
    print( ("Found %d %s."):format( #devices, outInv ) )

    if #devices == 1 then
      print "Set found device as Output"
      app.config.outputInventory = devices[1]
      config.write( app.configPath, app.config )
    end
  end


  -- get movement direction
  while app.config.movementDirection == "unknown" do
    local check = { north = 1, east = 1, west = 1, south = 1, up = 1, down = 1 }
    term.write "Movement direction> "
    local movDir = io.read "*l"

    if check[movDir:lower()] then
      print "Set as movement direction"
      app.config.movementDirection = movDir:upper()
      config.write( app.configPath, app.config )
    end
  end

end

local function main ()
  app.config = config.load( app.configPath, app.config )
  askConfig()


end

utils.pcallLog( "%s", main )