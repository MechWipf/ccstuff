local inv = require "inventory"

local app = {}
app.config = {
  inventories = {
    ["tileinscriber"] = {
      source = "ender_chest",
      method = "pull",
      direction = "east",
      items = {
        {
          -- Inscriber Calculation Press
          { fingerprint = { id = "appliedenergistics2:item.ItemMultiMaterial", dmg = 13 }, qty = 1, pullSlot = 4 },
          -- Pure Certus Quartz
          { fingerprint = { id = "appliedenergistics2:item.ItemMultiMaterial", dmg = 10, }, qty = 1, pullSlot = 3 },
        },
        {
          -- Inscriber Engineering Press
          { fingerprint = { id = "appliedenergistics2:item.ItemMultiMaterial", dmg = 14 }, qty = 1, pullSlot = 1 },
          -- Diamond
          { fingerprint = { id = "minecraft:diamond", dmg = 0 }, qty = 1, pullSlot = 1 },
        },
        {
          -- Inscriber Logic Press
          { fingerprint = { id = "appliedenergistics2:item.ItemMultiMaterial", dmg = 15 }, qty = 1, pullSlot = 1 },
          -- Gold Ingot
          { fingerprint = { id = "minecraft:gold_ingot", dmg = 0 }, qty = 1, pullSlot = 1 },
        },
        {
          -- Inscriber Silicon Press
          { fingerprint = { id = "appliedenergistics2:item.ItemMultiMaterial", dmg = 19 }, qty = 1, pullSlot = 1 },
          -- Silicon
          { fingerprint = { id = "EnderIO:itemMaterial", dmg = 0 }, qty = 1, pullSlot = 1 }
        }
      }
    }
  }
}

local machine_inscriber = {}
machine_inscriber.mt = {
  doWork = function ( self, msg )

    if self.state == 0 then
      self.state = 1

      if msg[2] == "pull" then
        local filteredItems = inv.filterItems( msg[1], msg[4] )


        inv.pullItems( self.machine, msg[3], msg[4], filteredItems )
      else
        inv.pushItems( self.machine, msg[3], msg[4] )
      end

    elseif self.state == 1 then
    end

  end,
}

function machine_inscriber.new ( machine )
  local o = {
    state = 0,
    machine = machine
  }

  setmetatable( o, { __index = machine_inscriber.mt } )
  return o
end


local function main ( args )

  local devices = utils.getDevices( "tileinscriber" )
  local inscribers = {}

  for i = 1, #devices do
    inscribers[i] = machine_inscriber.new( devices[i] )
  end


  local config = app.config.inventories.tileinscriber
  local chest = utils.getDevices( config.source )[1]


  local jobs = {}
  for k, items in pairs( config.items ) do
    local count = 1e99
    local available = false

    local cb = function ( item, tempQty )
      if not item and not item.qty then return end
      count = math.min( count, item.qty / tempQty  )
    end

    available = inv.hasItems( chest, items, cb )

    if available then
      jobs[#jobs+1] = { count, chest, config.method, config.direction, items }
    end
  end

  for _, inscriber in pairs( inscribers ) do
    local job
    if #jobs > 0 then
      job = { jobs[1][2], jobs[1][3], jobs[1][4], jobs[1][5] }
      jobs[1][1] = jobs[1][1] - 1
      if jobs[1][1] <= 0 then
        table.remove(jobs[1])
      end
    end

    inscriber:doWork( job )
  end
end

main( ... )


