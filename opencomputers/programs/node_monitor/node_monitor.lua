local _R = getfenv()
local    term,    parallel,    modularUI,    utils,    shell,    sleep,    colors =
      _R.term, _R.parallel, _R.modularUI, _R.utils, _R.shell, _R.sleep, _R.colors

local event_handler = require "vendor.event"()
local modularUIDom = require "modularUI.dom"
local shutdown = require "modularUI.helper".shutdown

local app = {}
local appID = "node_monitor_" .. math.random(100000, 999999)
local widgets = {}

app.dir = shell.getRunningProgram():match( "^(.*/).+$" )
app.primalAspects = {
  "aer", "terra", "ignis",
  "aqua", "ordo", "perditio"
}

do
  if not modularUI then
    modularUI = require "modularUI"

    local path = _G.API_PATH .. "modularUI/widgets/"
    modularUI.loadWidget( path .. "base" )
    modularUI.loadWidget( path .. "label" )
    modularUI.loadWidget( path .. "panel" )
    modularUI.loadWidget( path .. "button" )
  end

  modularUIDom.widgets = modularUI.widgets

  print( "Load node.form.lua" )
  widgets.node = modularUIDom.loadFile( app.dir .. "node.form.lua" )
  print( "Load node-inner.form.lua" )
  widgets.node_inner = modularUIDom.loadFile( app.dir .. "node-inner.form.lua" )
  print( "Load main.form.lua" )
  widgets.main = modularUIDom.loadFile( app.dir .. "main.form.lua" )
end

local function nodeWidget ( name )
  local nodeEnv = {}

  nodeEnv["init"] = function ( _ )
    nodeEnv.lbName:setText( name )

    for i, aspect in pairs( app.primalAspects ) do
      local text = aspect:gsub( "(.)(.*)", function ( s1, s2 ) return s1:upper() .. s2 end )
      nodeEnv[aspect] = {}

      local inner = widgets.node_inner( nodeEnv[aspect] )
      inner.y = i
      nodeEnv[aspect].lbAspectName:setText( text )

      inner:setParent( nodeEnv.main )
    end
  end

  local widget = widgets.node( nodeEnv )

  for _, aspect in pairs( app.primalAspects ) do
    widget[aspect] = {
      setCount = function ( count )
        nodeEnv[aspect].lbAspectCount:setText( tostring( count ) )
      end,
      setCountExtra = function ( count )
        nodeEnv[aspect].lbAspectCountExtra:setText( tostring( count ) )
      end
    }
  end

  function widget.updateColor ( _, clr )
    nodeEnv.pnMenu:setColor( "background", clr )
    nodeEnv.lbName:setColor( "background", clr )
  end

  return widget
end

function app.loadForm ()
  app.x, app.y = 1, 1
  app.w, app.h = term.getSize()

  app.form = {
    ["pnMain.calcW"] = function ( _ )
      return app.w
    end,
    ["pnMain.calcH"] = function ( _ )
      return app.h
    end,
    ["child.calcW"] = function ( self )
      return self.parent.w - ( self.offsetW or 0 )
    end,
    ["child.calcH"] = function ( self )
      return self.parent.h - ( self.offsetH or 0 )
    end,
    ["btnExit.onClickLeft"] = function ( _ )
      os.queueEvent( "exit_" .. appID )
    end,
  }

  widgets.main( app.form )

  app.form.pnMain:refresh()

  modularUI.startWatching( app.form.pnMain, term )
end

function app._newContainer ()
  local pnBack = app.form.pnBack

  local node = nodeWidget( "Node #" .. #app.container + 1 )
  node.y = #app.container * 8 + 1
  pnBack:addChild( node )
  node:refresh()

  return node
end

function app.main()
  app.container = {}

  while true do
    local aspectContainer = utils.getDevices "tt_aspectContainer" or {}

    for i, device in pairs ( aspectContainer ) do
      if not app.container[i] then app.container[i] = app:_newContainer() end
      local widget = app.container[i]

      local min = 1e99
      for _, aspect in pairs( app.primalAspects ) do
        local count = device.getAspectCount( aspect )
        widget[aspect].setCount( count )
        local countExtra = math.floor( math.sqrt( count ) )
        widget[aspect].setCountExtra( countExtra .. "cv" )
        if min > countExtra then min = countExtra end
      end

      if min < 30 then
        widget:updateColor( colors.brown )
      elseif min < 31 then
        widget:updateColor( colors.green )
      else
        widget:updateColor( colors.green )
      end
    end

    for i = #app.container, 1, -1 do
      if i > #aspectContainer then
        local parent = app.container[i].parent
        app.container[i]:delParent()
        app.container[i] = nil
        parent:invalidate()
      end
    end

    sleep(15)
  end
end

app:loadForm()

parallel.waitForAny(
  function ()
    utils.pcallLog(
      "EventHandler MainLoop exits with error: %s",
      event_handler.mainLoop,
      event_handler
    )
  end,
  function ()
    utils.pcallLog(
      "ModularUI MainLoop exits with error: %s",
      modularUI.mainLoop,
      event_handler
    )
  end,
  function ()
    utils.pcallLog(
      "Main exits with error: %s",
      app.main
    )
  end,
  function ()
    os.pullEvent( "exit_" .. appID )
    app.cleanShutdown = true
  end
)

shutdown( app.cleanShutdown )