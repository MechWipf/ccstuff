return panel {
  name = "main",
  head = { 2, 0, 10, 7 },
  calcW = "=self.parent.w - 4",
  color = { background = colors.gray },
  init = "@init",
  {
    panel {
      name = "pnMenu",
      head = { 0, 0, 1, 1 },
      calcW = "=self.parent.w",
      color = { background = colors.brown },
      {
        label {
          name = "lbName",
          head = { 0, 0, "Node #1" },
          color = { background = colors.brown },
        },
      }
    },
  }
}