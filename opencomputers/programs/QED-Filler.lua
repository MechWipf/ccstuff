-- ****************************** --
-- User: MechWipf
-- Date: 13.06.2015
-- Time: 18:12
-- ****************************** --

local inventories = {}
local config_path = '/appdata/config/qed-filler.config'
local config = [[ -- QED-Filler config --
return {
    coal = 'UNKNOWN',
    ore  = 'UNKNOWN'
}
]]

local function config ()
    if fs.exists(config_path) then
        local conf = dofile( 'config_path' )
        inventories = conf
    else
        local f = fs.open( config_path, 'w' )
        f.write( config )
        f.close()
        print( 'First run. Config files created.' )
        return false
    end

    if inventories.coal == 'UNKNOWN' or inventories.ore == 'UNKNOWN' then
        print( 'Please edit the config file first.' )
        return false
    end

end


local function main ()

    local qed = peripheral.wrap 'top'

    while true do

        qed.pullItem(  )

        sleep(1000)
    end

end

if config() then
    main()
end