--[[
            Config
    rsbSide     :   Redbundle Output side
    rsbClutch   :   Clutch Colorcode
    rsbCoil     :   Coil Colorcode
    energyCell  :   energyCell side or name
]]

local config = dofile "/appdata/RotaryEnergySaver/config.lua"
local energyCell = peripheral.wrap( config.energyCell )
local cursor = nil

rsbundle.setSide( config.rsbSide )

local function checkEnergy ()
    local f = energyCell.getEnergyStored( "" ) / energyCell.getMaxEnergyStored( "" ) * 100

    if f < 30 then
        rsbundle.set( config.rsbClutch, true )
        sleep(0.1)
        rsbundle.set( config.rsbCoil,   true )
    elseif f > 80 then
        rsbundle.set( config.rsbCoil,   false  )
        sleep(0.1)
        rsbundle.set( config.rsbClutch, false )
    end
end

function main ()
    if not cursor then
        cursor = { term.getCursorPos() }
    end

    while true do
        local status = checkEnergy()

        local f = energyCell.getEnergyStored( "" ) / energyCell.getMaxEnergyStored( "" )

        term.clearLine()
        term.write( string.format( "EnergyCell %d%% Coil: %s Clutch: %s", f * 100, tostring(rsbundle.getOut( config.rsbCoil )) , tostring(rsbundle.getOut( config.rsbClutch )) ) )
        term.setCursorPos( unpack( cursor ) )
        sleep(1)  
    end   
end

while true do
    pcall( main )
    sleep(2)
end