panel {
  name = "pnTabInfo",
  head = { 0, 1, 0, 0 },
  calcW = "=self.parent.w",
  calcH = "=self.parent.h-3",
  color = { background = colors.black },
  {
    label {
      head = { 1, 1, "Stored Energy" },
      color = { background = colors.black },
    },
    progressbar {
      head = { 2, 2, 1, "-" },
      update = "@pbStoredEnergy.update"
    }
  }
}