if not von then
	if not os.loadAPI "apis/von" then
		error "Failed while loading 'apis/von'"
	end
end

if not event then
	if not os.loadAPI "apis/event" then
		error "Failed while loading 'apis/event'"
	end
end

if not rsbundle then
	if not os.loadAPI "apis/rsbundle" then
		error "Failed while loading 'apis/rsbundle'"
	end
end

local sModemSide = nil

for n,sSide in pairs( rs.getSides() ) do
	if peripheral.getType( sSide ) == "modem" then
		sModemSide = sSide
	end
end

if not sModemSide then
	error( "No modem attached!" )
end

local rsb = rsbundle
local fs = fs
local ev = event.events()
local modem = peripheral.wrap( sModemSide )
local CHANNEL_RC = 65533

local register = {}
local motorClrs = {
	forward	= { "gray"		, "purple"	},
	back	= { "lightGray"	, "cyan"	},
	left	= { "green"		, "white"	},
	right	= { "black"		, "orange"	}}


local sCmds = {
	register 		= [[return { 'register', os.computerLabel(), os.computerID() }]],
	placeQuarry 	= [[turtle.place()]],
	removeQuarry	= [[turtle.dig()]],
	cleanInv		= [[for n = 4*4, 1, -1 do turtle.select(n) turtle.dropDown() end]],
}

local function getPosition()
	local vPos = vector.new( gps.locate() )
	vPos = vPos + vector.new( unpack( quarry.offset ) )
	return vPos
end

local function vectorEqual( v1, v2 )
	if v1.x == v2.x and v1.y == v2.y and v1.z == v2.z then
		return true
	end
	return false
end

local function send( sCmd )
	local _cmd = sCmds[sCmd]
	if _cmd and not sCmd == "register" then
		for _,id in ipairs( register ) do
			modem.transmit( id, os.getComputerID(), "run" .. _cmd )
		end
	else
		modem.transmit( CHANNEL_RC, os.getComputerID(), "run:" .. _cmd )
	end
end

local function move( sDir )
	rsb.pulse( motorClrs[sDir][1], 0.8 )
	sleep(1)
	rsb.pulse( motorClrs[sDir][2], 0.8 )
	sleep(1)
end

local function mwq_modem_message( _, sSide, sChannel, sReplyChannel, sMessage, nDistance )
	if sChannel == CHANNEL_RC or sChannel == os.getComputerID() and sSide == sModemSide then
		local status, data = pcall(von.deserialize, sMessage)
		if status and data[1] == "register" and data[2] == "qt" then
			register[#register + 1] = data[3]
		end
	end
end

local function moveTo( x, z )
	while true do
		local vCurPos = getPosition()

		if x < vCurPos.x then
			move( "back" )
		elseif x > vCurPos.x then
			move( "forward" )
		end

		if z < vCurPos.z then
			move( "left" )
		elseif z > vCurPos.z then
			move( "right" )
		end

		if vectorEqual( vector.new( x, vCurPos.y, z ), vCurPos ) then
			return true
		end
	end
end

local function posSave( vCurPos, vTarPos )
	local t = { { vCurPos.x, vCurPos.y, vCurPos.z }, { vTarPos.x, vTarPos.y, vTarPos.z } }
	local str = von.serialize( t )

	local file = fs.open( "appdata/mwquarry/save.von", "w" )
	file.write( str )
	file.close()
end

local function posLoad()
	local file = fs.open( "appdata/mwquarry/save.von", "r" )
	if not file then return getPosition(), getPosition() end

	local t = von.deserialize( file.readAll() )
	file.close()
	return vector.new( unpack( t[1] ) ), vector.new( unpack( t[2] ) )
end

local function main()
	local nPullEvent = os.pullEvent
	os.pullEvent = os.pullEventRaw

	if not fs.exists( "appdata" ) then
		fs.makeDir( "appdata" )
	end
	if not fs.exists( "appdata/mwquarry" ) then
		fs.makeDir( "appdata/mwquarry" )
	end

	if not modem.isOpen( os.getComputerID() ) then
		modem.open( os.getComputerID() )
	end

	rsb.setSide( "front" )

	print( "Get quarry turtles." )
	send( "register" )
	sleep(1)
	print( #register .. " turtles found." )

	--send( "cleanInv" )
	--sleep( 10 )
	--send( "removeQuarry" )

	local vCurMPos, vTarPos = posLoad()

	print( vCurMPos, ":", vTarPos )

	local bRunning = true
	while bRunning do
		local vCurPos = getPosition()

		if vectorEqual( vCurMPos, vTarPos ) then
			term.write( "Enter new Target offset X\n>" )
			local x = tonumber( io.read() )
			term.write( "Enter new Target offset Z\n>" )
			local z = tonumber( io.read() )
			vTarPos = vCurPos + vector.new( x, vCurPos.y, z )
			vCurMPos = vCurPos
			posSave( vCurMPos, vTarPos )
		end

		if not vectorEqual( vCurPos, vCurMPos ) then
			moveTo( vCurMPos.x, vCurMPos.z )
		end

		if vCurMPos.z ~= vTarPos.z then
			moveTo( vCurMPos.x, vTarPos.z )
		end

		send( "placeQuarry" )
		sleep( 40 )
		send( "cleanInv" )
		sleep( 15 )
		send( "removeQuarry" )
		sleep( 1 )

		if vCurMPos.x < vTarPos.x then
			vCurMPos = vCurMPos + vector.new( 1 )
		elseif vCurMPos.x > vTarPos.x then
			vCurMPos = vCurMPos - vector.new( 1 )
		end

		posSave( vCurMPos, vTarPos )
	end


	modem.close( os.getComputerID() )
	os.pullEvent = nPullEvent
end


ev:event( "modem_message", "mwq_modem_message", mwq_modem_message )
parallel.waitForAny(function() ev:mainLoop() end, function() local _, err = pcall(main) print(err) end)
