return {
	[1] = { 
		[1] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "Botania:manaResource",
			},
			["qty"] = 5,
		},
		[2] = { 
			["fingerprint"] = { 
				["dmg"] = "1",
				["id"] = "Botania:manaResource",
			},
			["qty"] = 1,
		},
	},
	[2] = { 
		[1] = { 
			["fingerprint"] = { 
				["dmg"] = "2",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
		[2] = { 
			["fingerprint"] = { 
				["dmg"] = "3",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
		[3] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:sand",
			},
			["qty"] = 2,
		},
		[4] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:slime_ball",
			},
			["qty"] = 1,
		},
		[5] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:melon",
			},
			["qty"] = 1,
		},
	},
	[3] = { 
		[1] = { 
			["fingerprint"] = { 
				["dmg"] = "1",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
		[2] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
		[3] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:sapling",
			},
			["qty"] = 3,
		},
		[4] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:wheat",
			},
			["qty"] = 1,
		},
	},
	[4] = { 
		[1] = { 
			["fingerprint"] = { 
				["dmg"] = "2",
				["id"] = "Botania:manaResource",
			},
			["qty"] = 2,
		},
		[2] = { 
			["fingerprint"] = { 
				["dmg"] = "2",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
		[3] = { 
			["fingerprint"] = { 
				["dmg"] = "7",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
	},
	[5] = { 
		[1] = { 
			["fingerprint"] = { 
				["dmg"] = "2",
				["id"] = "Botania:manaResource",
			},
			["qty"] = 2,
		},
		[2] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
		[3] = { 
			["fingerprint"] = { 
				["dmg"] = "7",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
	},
	[6] = { 
		[1] = { 
			["fingerprint"] = { 
				["dmg"] = "2",
				["id"] = "Botania:manaResource",
			},
			["qty"] = 2,
		},
		[2] = { 
			["fingerprint"] = { 
				["dmg"] = "3",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
		[3] = { 
			["fingerprint"] = { 
				["dmg"] = "5",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
	},
	[7] = { 
		[1] = { 
			["fingerprint"] = { 
				["dmg"] = "1",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
		[2] = { 
			["fingerprint"] = { 
				["dmg"] = "7",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
		[3] = { 
			["fingerprint"] = { 
				["dmg"] = "2",
				["id"] = "Botania:manaResource",
			},
			["qty"] = 2,
		},
	},
	[8] = { 
		[1] = { 
			["fingerprint"] = { 
				["dmg"] = "3",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
		[2] = { 
			["fingerprint"] = { 
				["dmg"] = "6",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
		[3] = { 
			["fingerprint"] = { 
				["dmg"] = "2",
				["id"] = "Botania:manaResource",
			},
			["qty"] = 2,
		},
	},
	[9] = { 
		[1] = { 
			["fingerprint"] = { 
				["dmg"] = "2",
				["id"] = "Botania:manaResource",
			},
			["qty"] = 2,
		},
		[2] = { 
			["fingerprint"] = { 
				["dmg"] = "5",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
		[3] = { 
			["fingerprint"] = { 
				["dmg"] = "1",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
	},
	[10] = { 
		[1] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
		[2] = { 
			["fingerprint"] = { 
				["dmg"] = "4",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
		[3] = { 
			["fingerprint"] = { 
				["dmg"] = "2",
				["id"] = "Botania:manaResource",
			},
			["qty"] = 2,
		},
	},
	[11] = { 
		[1] = { 
			["fingerprint"] = { 
				["dmg"] = "23",
				["id"] = "Botania:manaResource",
			},
			["qty"] = 1,
		},
		[2] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "Botania:manaResource",
			},
			["qty"] = 1,
		},
		[3] = { 
			["fingerprint"] = { 
				["dmg"] = "15",
				["id"] = "minecraft:dye",
			},
			["qty"] = 1,
		},
		[4] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:reeds",
			},
			["qty"] = 1,
		},
		[5] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:fishing_rod",
			},
			["qty"] = 1,
		},
	},
	[12] = { 
		[1] = { 
			["fingerprint"] = { 
				["dmg"] = "23",
				["id"] = "Botania:manaResource",
			},
			["qty"] = 1,
		},
		[2] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "Botania:manaResource",
			},
			["qty"] = 1,
		},
		[3] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:netherbrick",
			},
			["qty"] = 1,
		},
		[4] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:gunpowder",
			},
			["qty"] = 1,
		},
		[5] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:nether_wart",
			},
			["qty"] = 1,
		},
	},
	[13] = { 
		[1] = { 
			["fingerprint"] = { 
				["dmg"] = "23",
				["id"] = "Botania:manaResource",
			},
			["qty"] = 1,
		},
		[2] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "Botania:manaResource",
			},
			["qty"] = 1,
		},
		[3] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:stone",
			},
			["qty"] = 1,
		},
		[4] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:coal_block",
			},
			["qty"] = 1,
		},
		[5] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:brown_mushroom",
			},
			["qty"] = 1,
		},
	},
	[14] = { 
		[1] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "Botania:manaResource",
			},
			["qty"] = 1,
		},
		[2] = { 
			["fingerprint"] = { 
				["dmg"] = "23",
				["id"] = "Botania:manaResource",
			},
			["qty"] = 1,
		},
		[3] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:carpet",
			},
			["qty"] = 1,
		},
		[4] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:feather",
			},
			["qty"] = 1,
		},
		[5] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:string",
			},
			["qty"] = 1,
		},
	},
	[15] = { 
		[1] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:snow",
			},
			["qty"] = 2,
		},
		[2] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:wool",
			},
			["qty"] = 1,
		},
		[3] = { 
			["fingerprint"] = { 
				["dmg"] = "2",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
		[4] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:cake",
			},
			["qty"] = 1,
		},
		[5] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
	},
	[16] = { 
		[1] = { 
			["fingerprint"] = { 
				["dmg"] = "1",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
		[2] = { 
			["fingerprint"] = { 
				["dmg"] = "3",
				["id"] = "Botania:rune",
			},
			["qty"] = 1,
		},
		[3] = { 
			["fingerprint"] = { 
				["dmg"] = "1",
				["id"] = "minecraft:leaves",
			},
			["qty"] = 3,
		},
		[4] = { 
			["fingerprint"] = { 
				["dmg"] = "0",
				["id"] = "minecraft:spider_eye",
			},
			["qty"] = 1,
		},
	},
}