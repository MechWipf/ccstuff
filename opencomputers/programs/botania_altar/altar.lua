local _R = getfenv()
local    peripheral,    term,    utils,    shell,    parallel,    rs,    sleep =
      _R.peripheral, _R.term, _R.utils, _R.shell, _R.parallel, _R.rs, _R.sleep

local config = require "config"
local event_handler = require "vendor.event"()
local bundle = require "rsbundle"
local inv = require "inventory"

local app = {}
local appID = "runic_altar_" .. math.random( 1000000, 9999999 )
app.appdata = _G.APPDATA_PATH .. "botania_altar/"
app.dir = shell.getRunningProgram():match( "^(.*/).+$" )

local cmd = select( 1, ... )

if cmd == "config" then
  local _, data
  app.config = {}

  term.setCursorPos( 1, 1 )
  term.clear()

  print "Attach buffer container."
  _, data = os.pullEvent( "peripheral" )
  print( "Buffer: " .. data )
  app.config.buffer = data

  print "Attach dispenser."
  _, data = os.pullEvent( "peripheral" )
  print( "Dispenser: " .. data )
  app.config.dispenser = data

  print "Enter dispenser side (origin buffer chest)"
  app.config.bufferDispenser_side = io.read "*l"

  print "Enter Dispenser redstone:"
  app.config.dispenser_rs = io.read "*l"

  print "Enter Wand redstone:"
  app.config.wand_rs = io.read "*l"

  print "Enter Livingrock redstone:"
  app.config.rock_rs = io.read "*l"

  print "Enter runic altar redstone:"
  app.config.runic_altar_rs = io.read "*l"

  config.write( app.appdata .. "config.von", app.config )

  return
end

function app:prepare ()
  print "Preparing"
  self.config = config.load( self.appdata .. "config.von" )

  self.dispenser = peripheral.wrap( self.config.dispenser )
  if not self.dispenser then error "Dispenser not found." end
  print "Dispenser found."

  self.buffer = peripheral.wrap( self.config.buffer )
  if not self.buffer then error "Buffer not found." end
  print "Buffer found."

  local path = app.dir .. "runes_crafting_pattern.lua"
  print( "Loading recipes: " .. path )
  self.recipes = dofile( path )
  print( "Recipes found: " .. #self.recipes )

  self.status = "wait_for_recipe"
end

function app:run ()
  if self.running then return end
  self.running = true

  local conf = self.config

  if self.status == "wait_for_recipe" then
    -- go throuh recipes and check if we can actually craft one of them
    for _, recipe in pairs( self.recipes ) do
    -- if we can craft one, push all items out of the buffer chest into the dispenser
      if inv.hasItems( self.buffer, recipe ) then
        inv.pushItems( self.buffer, conf.bufferDispenser_side, recipe )
        -- and set step to "input_recipe"
        self.status = "input_recipe"
        print "Found recipe"
        break
      end
    end
    -- if not, repeat step
  elseif self.status == "input_recipe" then
    -- send pulses to the dispenser until it is empty
    if next( inv.getItems( self.dispenser ) ) ~= nil then
      bundle.pulse( conf.dispenser_rs )
    elseif rs.getAnalogInput( conf.runic_altar_rs ) > 0 then
    -- dispeners is empty and the altar sends a signal higher than 0 goto step "wait_for_mana"
      self.status = "wait_for_mana"
      print "Start crafting"
    end
  elseif self.status == "wait_for_mana" then
    -- wait until the altar sends a signal == 2
    if rs.getAnalogInput( conf.runic_altar_rs ) > 1 then
    -- then move on to "finish_crafting"
      self.status = "drop_rock"
      print "Apply finishing touch"
    end
  elseif self.status == "drop_rock" then
    -- drop a livingrock on the altar
    bundle.pulse( conf.rock_rs, 0.5 )
    -- backup, if dropping does not work
    self.drop_rock_interval = event_handler:interval( 8, function () bundle.pulse( conf.rock_rs, 0.5 ) end, 3 )
    self.status = "finish_crafting"
  elseif self.status == "finish_crafting" then
    -- tick the wand
    bundle.pulse( conf.wand_rs, 0.5 )
    -- go back to "wait_for_recipe" when altar signal is 0
    if rs.getAnalogInput( conf.runic_altar_rs ) == 0 then
      self.status = "wait_for_recipe"
      self.drop_rock_interval.cancel()
      print "Done"
      sleep( 5 )
    end
  end

  self.running = false
end

local function main ()
  local onChar = function ( _, char )
    if char == "q" then
      os.queueEvent( "exit_" .. appID )
    end
  end
  event_handler:event( "char", appID .. "_char", onChar )

  print "Starting up. Quit with Q."

  while true do
    app:run()
    sleep(0.5)
  end
end


if not utils.pcallLog( "prepare ended with error: %s", app.prepare, app ) then return end

parallel.waitForAny(
  function () event_handler:mainLoop() end,
  function () os.pullEvent( "exit_" .. appID ) end,
  function () utils.pcallLog( "Main ended with error: %s", main ) end
)