local btnGeneral = tbutton {
  head = { 1, 0, 7, 1, "General" },
  color = { button_active = colors.gray, button = colors.green },
  onClickLeft = function () env.configTabController:switchTab( "general" ) end,
}

local pnGeneral = panel {
  head = { 0, 0, 1, 1 },
  calcW = "=self.parent.w",
  calcH = "=self.parent.h",
  {
    viewlist {
      head = { 0, 0, 1, 1, "item" },
      calcW = "=self.parent.w",
      calcH = "=self.parent.h",
      color = { background = colors.black, scroll = colors.brown },
      {
        panel {
          head = { 0, 0, 20, 12 },
          calcW = "=self.parent.w - 1",
          color = { background = colors.black },
          {
            panel {
              head = { 0, 0, 0, 1 },
              calcW = "=self.parent.w",
              {
                label {
                  head = { 0, 0, "Peripherals" },
                  calcW = "=self.text:len()",
                  calcX = "=self.parent.w / 2 - self.text:len() / 2"
                }
              }
            },
            label {
              head = { 1, 2, "Local Interface" },
              color = { background = colors.black },
            },
            textbox {
              name = "tbLocalInterface",
              head = { 17, 2, 20, 1 },
              calcW = "=self.parent.w - self.x - 1",
              setText = "@localInterface",
            },
            label {
              head = { 1, 4, "Store Interface" },
              color = { background = colors.black },
            },
            textbox {
              name = "tbStoreInterface",
              head = { 17, 4, 20, 1 },
              calcW = "=self.parent.w - self.x - 1",
              setText = "@storeInterface",
            },
            label {
              head = { 1, 6, "Ticketmachine" },
              color = { background = colors.black },
            },
            textbox {
              name = "tbTicketmachine",
              head = { 17, 6, 20, 1 },
              calcW = "=self.parent.w - self.x - 1",
              setText = "@ticketmachine",
            },
            label {
              head = { 1, 8, "Sensor" },
              color = { background = colors.black },
            },
            textbox {
              name = "tbSensor",
              head = { 17, 8, 20, 1 },
              calcW = "=self.parent.w - self.x - 1",
              setText = "@sensor",
            },
            label {
              head = { 1, 10, "Door Redstone" },
              color = { background = colors.black },
            },
            textbox {
              name = "tbRedstone",
              head = { 17, 10, 20, 1 },
              calcW = "=self.parent.w - self.x - 1",
              setText = "@redstone",
            },
          }
        },
        panel {
          head = { 0, 0, 1, 7 },
          calcW = "=self.parent.w - 1",
          color = { background = colors.black },
          {
            panel {
              head = { 0, 0, 1, 1 },
              calcW = "=self.parent.w",
              {
                label {
                  head = { 0, 0, "Misc" },
                  calcW = "=self.text:len()",
                  calcX = "=self.parent.w / 2 - self.text:len() / 2"
                }
              }
            },
            label {
              head = { 1, 2, "Disable Exit Button" },
              color = { background = colors.black },
            },
            tbutton {
              name = "btnDisableExit",
              head = { 21, 2, 1, 1, "N" },
              isActive = "@disableExit",
              onClickLeft = "@btnCheckbox.onClickLeft",
              color = { button = colors.red, button_active = colors.green },
            },
            label {
              head = { 1, 4, "Restart on Error" },
              color = { background = colors.black },
            },
            tbutton {
              name = "btnRestart",
              head = { 21, 4, 1, 1, "N" },
              isActive = "@autoRestart",
              onClickLeft = "@btnCheckbox.onClickLeft",
              color = { button = colors.red, button_active = colors.green },
            },
            label {
              head = { 1, 6, "Master Server" },
              color = { background = colors.black },
            },
            textbox {
              name = "tbServer",
              head = { 21, 6, 6, 1 },
              setText = "@server",
              regex = "^[0-9]?[0-9]?[0-9]?[0-9]?[0-9]?$"
            },
          }
        },
      }
    }
  }
}

local btnItems = tbutton {
  head = { 9, 0, 5, 1, "Items" },
  color = { button_active = colors.black, button = colors.green },
  onClickLeft = function () env.configTabController:switchTab( "items" ) end
}

local pnItems = panel {
  head = { 0, 0, 1, 1 },
  calcW = "=self.parent.w",
  calcH = "=self.parent.h",
  {
    panel {
      head = { 0, 0, 1, 1 },
      calcW = "=self.parent.w",
      calcY = "=self.parent.h-1",
      {
        button {
          head = { 1, 0, 1, 1, "Add" },
          calcW = "=self.text:len()",
          onClickLeft = "@btnItemsAdd.onClickLeft",
          color = { button = colors.cyan },
        },
        button {
          head = { 1, 0, 8, 1, "Load URL" },
          calcW = "=self.text:len()",
          calcX = "=self.parent.w - self.text:len() - 2",
          onClickLeft = "@btnLoadURL.onClickLeft",
          color = { button = colors.cyan }
        },
      }
    },
    viewlist {
      name = "vlConfigItems",
      head = { 0, 0, 1, 1, "semi_item" },
      setSimpleWrapper = true,
      wrapper = "@configItemsWrapper",
      calcW = "=self.parent.w",
      calcH = "=self.parent.h - 1",
      color = { background = colors.black, scroll = colors.brown },
    }
  }
}

env.configTabController.tabList = {
  {
    name = "general",
    button = btnGeneral,
    widgets = { pnGeneral },
  },
  {
    name = "items",
    button = btnItems,
    widgets = { pnItems },
  },
}

panel {
  name = "pnConfig",
  head = { 1, 1 },
  calcW = "@getTermW",
  calcH = "@getTermH",
  color = { background = colors.red },
  {
    label {
      head = { 0, 0, "Shop Configuration" },
      calcX = "=self.parent.w / 2 - self.text:len() / 2",
      color = { background = colors.red }
    },
    panel {
      name = "pnConfigTabBar",
      head = { 0, 1, 1, 1 },
      calcW = "=self.parent.w",
      color = { background = colors.brown },
      {
        btnGeneral,
        btnItems,
      }
    },
    panel {
      name = "pnConfigContainer",
      head = { 0, 2 },
      calcW = "=self.parent.w",
      calcH = "=self.parent.h - 2",
      color = { background = colors.black },
    },
    button {
      head = { 1, 0, 1, 1, "X" },
      calcX = "=self.parent.w - 1",
      onClickLeft = "@btnExit.onClickLeft",
      color = { button = colors.gray },
    },
  }
}

panel {
  name = "pnConfigModalURL",
  head = { 0, 0, 30, 4 },
  calcX = "=math.floor(env.getTermW() / 2 - self.w / 2)",
  calcY = "=math.floor(env.getTermH() / 2 - self.h / 2)",
  color = { background = colors.red },
  {
    label {
      head = { 0, 0, "Enter URL" },
      calcX = "=math.floor(self.parent.w / 2 - self.w / 2)",
      color = { background = colors.red },
    },
    textbox {
      name = "tbLoadURL",
      head = { 1, 1, 28, 1 },
      calcY = "=self.parent.h - 3"
    },
    button {
      head = { 1, 0, 1, 1, "Close" },
      calcW = "=self.text:len()",
      calcY = "=self.parent.h - 1",
      onClickLeft = "@btnConfigModalClose.onClickLeft",
    },
    button {
      head = { 1, 0, 1, 1, "Accept" },
      calcW = "=self.text:len()",
      calcY = "=self.parent.h - 1",
      calcX = "=self.parent.w - self.text:len() - 1",
      onClickLeft = "@btnConfigModalAccept.onClickLeft",
    }
  }
}

panel {
  name = "pnCloseModal",
  head = { 0, 0, 23, 4 },
  calcX = "=math.floor(env.getTermW() / 2 - self.w / 2)",
  calcY = "=math.floor(env.getTermH() / 2 - self.h / 2)",
  color = { background = colors.red },
  {
    label {
      head = { 0, 0, "Save and exit?" },
      calcX = "=math.floor(self.parent.w / 2 - self.w / 2)",
      color = { background = colors.red },
    },
    button {
      head = { 1, 0, 1, 1, "Cancel" },
      calcW = "=self.text:len()",
      calcY = "=self.parent.h - 1",
      onClickLeft = "@btnCloseModalCancel.onClickLeft",
    },
    button {
      head = { 1, 0, 1, 1, "Discard" },
      calcW = "=self.text:len()",
      calcY = "=self.parent.h - 1",
      calcX = "=math.floor(self.parent.w / 2 - self.w / 2) + 1",
      onClickLeft = "@btnCloseModalDiscard.onClickLeft",
    },
    button {
      head = { 1, 0, 1, 1, "Save" },
      calcW = "=self.text:len()",
      calcY = "=self.parent.h - 1",
      calcX = "=self.parent.w - self.text:len() - 1",
      onClickLeft = "@btnCloseModalSave.onClickLeft",
    },
  }
}

panel {
  name = "pnConfigItemModal",
  head = { 0, 0, 30, 14 },
  calcX = "=math.floor(env.getTermW() / 2 - self.w / 2)",
  calcY = "=math.floor(env.getTermH() / 2 - self.h / 2) + 2",
  color = { background = colors.orange },
  {
    label {
      head = { 0, 0, "" },
      text = "@ItemModalLabel",
      calcW = "=self.text:len()",
      calcX = "=math.floor( self.parent.w / 2 - self.text:len() / 2 )",
      color = { background = colors.green },
    },
    panel {
      head = { 1, 1, 0, 0 },
      calcW = "=self.parent.w - 2",
      calcH = "=self.parent.h - 2",
      {
        label {
          head = { 1, 0, "Key" }
        },
        textbox {
          name = "tbItemKey",
          head = { 11, 0, 5, 1 },
          calcW = "=self.parent.w - self.x - 1",
        },
        label {
          head = { 1, 2, "Name" },
        },
        textbox {
          name = "tbItemName",
          head = { 11, 2, 5, 1 },
          calcW = "=self.parent.w - self.x - 1",
        },
        label {
          head = { 1, 4, "Min Value" },
        },
        textbox {
          name = "tbItemMinValue",
          head = { 11, 4, 5, 1 },
          calcW = "=self.parent.w - self.x - 1",
        },
        label {
          head = { 1, 6, "Max Value" }
        },
        textbox {
          name = "tbItemMaxValue",
          head = { 11, 6, 5, 1 },
          calcW = "=self.parent.w - self.x - 1",
        },
        label {
          head = { 1, 8, "Rate" }
        },
        textbox {
          name = "tbItemRate",
          head = { 11, 8, 5, 1 },
          calcW = "=self.parent.w - self.x - 1",
        },
        label {
          head = { 1, 10, "Target" }
        },
        textbox {
          name = "tbItemTarget",
          head = { 11, 10, 5, 1 },
          calcW = "=self.parent.w - self.x - 1",
        },
      }
    },
    button {
      head = { 2, 1, 6, 1, "Cancel" },
      calcY = "=self.parent.h - 1",
      onClickLeft = "@btnConfigItemModalClose.onClickLeft",
    },
    button {
      head = { 1, 1, 1, 1, "Save" },
      calcY = "=self.parent.h - 1",
      calcX = "=self.parent.w - self.text:len()",
      onClickLeft = "@btnConfigItemModalSave.onClickLeft",
    },
  }
}