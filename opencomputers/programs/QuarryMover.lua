if not getDevices 	then error "util.lua not installed." end
if not move 		then error "movement.lua not installed." end

local _TFW_	= 1
local _TUP_	= 2
local _TDW_	= 3

local tryMove 	= move.tryMove
local tryDig	= move.tryDig
local tLeft 	= turtle.turnLeft
local tRight 	= turtle.turnRight
local tSelect	= turtle.select
local tPlace	= turtle.place

local function construct ()
	tryMove( _TUP_, 2 )

	for i = 1, 2 do
		tryDig( _TFW_ )
		tSelect( i )
		tPlace()
		tryMove( _TDW_ )
	end

	tRight()
	tryMove( _TFW_ )
	tLeft()
	tryMove( _TUP_, 2 )

	tSelect( 3 )

	for i = 1, 2 do
		tryDig( _TFW_ )
		tPlace()
		tryMove( _TDW_ )
	end

	tryMove( _TUP_, 1 )
	tLeft()
	tryMove( _TFW_ )
	tRight()
end

local function deconstruct ()
	
end

local function main ()
	tryMove( _TUP_ )
	construct()

end

main()