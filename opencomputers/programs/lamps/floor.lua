
function onRenderStart ( )
end

function onRenderEnd ()
end

function onRender ( x, y )
  local idx = x + y * display.x
  local clr
  local clr_t

  local f      =      clock / 3 % 1
  local last_f = last_clock / 3 % 1

  if not colors[idx] then
    clr = { 255, 255, 255 }
    clr_t = { math.random()*255, math.random()*255, math.random()*255 }
    colors[idx] = clr
    colors_t[idx] = clr_t
  else
    clr = colors[idx]
    clr_t = colors_t[idx]
  end

  if f < last_f then
    clr = clr_t
    colors[idx] = clr
    clr_t = { math.random()*255, math.random()*255, math.random()*255 }
    colors_t[idx] = clr_t
  end

  local dir = vec3_diff( clr_t, clr )

  clr = vec3_add( clr, vec3_scalar( dir, f ) )
  
  p = (math.sin(y + x + clock) + 1) / 2
  clr = vec3_min( vec3_scalar( clr, 1 - 0.4 * p ), { 255, 255, 255 } )

  return math.floor( clr[1] % 256 ) * 0x010000 + math.floor( clr[2] % 256 ) * 0x000100 + math.floor( clr[3] % 256 ) * 0x000001
end