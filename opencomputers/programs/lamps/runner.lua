local utils = utils

local colors = {}
local colors_t = {}

local clock = os.clock()
local last_clock = os.clock()

function main ()
  for k, v in pairs( display.map ) do
    display.map[k] = peripheral.wrap( v )
  end

  local renderer = utils.fileRead()

  -- anti lag
  local s = 0
  while true do
    clock = os.clock()
    for i, tile in pairs( display.map ) do
      local x = (i-1) % display.x
      local y = math.floor( (i-1) / display.x )
      local color = render( x, y )

      display.map[i].setColor( color )

      if s > 20 then sleep() s = 0 else s = s + 1 end
    end
    last_clock = clock

    sleep(0.1)
  end
end