if not Devices then return end
if not Devices.meBridge then return end
if not Devices.me_interface then return end

local meBridge = Devices.meBridge[1]
local meInterface = Devices.me_interface[1]

local min = math.min
local max = math.max

getfenv(0).MeCondenser = {}
MeCondenser.MECondensList = {}
local MECondensList = MeCondenser.MECondensList
local MeOutSide
local Interval = 0.2


function MeCondenser.addItem(id,maxcount)
	MECondensList[id] = maxcount
end

function MeCondenser.delItem(id)
	MECondensList[id] = nil
end

function MeCondenser.setSide(side)
	MeOutSide = side
end

function MeCondenser.setInterval(num)
	Interval = max(num, 0.2)
end

local function getCount(id)
	return meBridge.listAll()[id] or 0
end

local function main()
	while true do
		for k,v in pairs(MECondensList) do
			local c = getCount(k)
			if c > v then
				if MeOutSide ~= nil then
					meBridge.retrieve(k, min(c-v,64),MeOutSide)
				end
			end
			sleep(Interval)
		end
	
		sleep(0.01)
	end
end

goroutine.spawnBackground("MECondenser",main)