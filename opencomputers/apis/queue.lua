local floor = math.floor
local setmetatable = setmetatable

local Queue = {}

function Queue.compare ( objA, objB )
  if not objA then return false end
  if not objB then return true end

  return objA.value < objB.value
end

function Queue:siftDown ( index )
  local index = index or 1

  while index < self._len do
    local leftChild = index * 2
    local rightChild = index * 2 + 1

    if not self.compare( self._q[index], self._q[leftChild] )
    or not self.compare( self._q[index], self._q[rightChild] )
    then
      if self.compare( self._q[leftChild], self._q[rightChild] ) then
        -- swap
        self._q[leftChild], self._q[index] = self._q[index], self._q[leftChild]
        self._q[leftChild].idx = leftChild
        self._q[index].idx = index
        -- new index
        index = leftChild
      else
        -- swap
        self._q[rightChild], self._q[index] = self._q[index], self._q[rightChild]
        self._q[leftChild].idx = leftChild
        self._q[index].idx = index
        -- new index
        index = rightChild
      end
    else
      break
    end
  end
end

function Queue:insert ( obj )
  self._len = self._len + 1
  local current = self._len
  local parent = floor( current / 2 )

  while current > 1 and self.compare( obj, self._q[parent] ) do
    self._q[current] = self._q[parent]
    self._q[current].idx = current
    current = parent
    parent = floor( current / 2 )
  end

  self._q[current] = obj
  self._q[current].idx = current
end

function Queue:decrease ( obj )
  local index, value = obj.idx, obj.value
  local element = self._q[index]
  self._q[index], self._q[self._len] = self._q[self._len], self._q[index]
  self._q[index].idx = index
  self._q[self._len] = nil
  self._len = self._len - 1
  self:siftDown( index )
  self:insert( obj )
end

function Queue:getTop ()
  return self._q[1]
end

function Queue:deleteTop ()
  local r = self:getTop()
  if not r then return end
  -- swap with last index
  self._q[1], self._q[self._len] = self._q[self._len], self._q[1]
  self._q[1].idx = 1
  self._q[self._len] = nil
  self._len = self._len - 1
  -- then sift down to keep integrity
  self:siftDown( 1 )
  -- don't forget to return the min
  r.idx = nil
  return r
end

return {
  new = function ()
    local instance = setmetatable( {}, { __index = Queue } )
    instance._q = {}
    instance._len = 0
    return instance
  end
}
