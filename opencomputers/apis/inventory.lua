
local module = {}

--- Converts string to fingerprint
-- @tparam string str
-- @treturn [table|fingerprint]
function module.fingerprintFromString ( str )
  local id, dmg, hash = str:match( "(.-):([0-9]+):*(.*)" )
  return { id = id, dmg = dmg, nbtHash = hash }
end

--- Convert fingerprint to string
-- @tparam [table|fingerprint] fingerprint
-- @treturn string
function module.fingerprintToString ( fingerprint )
  return ("%s:%s%s"):format( fingerprint.id, fingerprint.dmg, fingerprint.nbtHash and ":"..fingerprint.nbtHash or "" )
end

--- Checks for items in an inventory.
-- @name Inventory.hasItems
-- @param inv [Peripheral] Peripheral table with inventory functions.
-- @param items [table] Item list.
-- @param callback [function] Optional. Function params are (fingerprint, item).
-- @return true or false
-- @usage items should be a table list with tables that contain a fingerprint and qty for quantity.
function module.hasItems( inv, items, callback )
    local qtyList = {}

    for _, item in pairs(items) do
        local key = module.fingerprintToString(item.fingerprint)
        qtyList[key] = item.qty
    end

    local filter = function ( fingerprint, _ )
        local key = module.fingerprintToString(fingerprint)

        if qtyList[key] ~= nil then
            return true
        end

        return false
    end

    local invItems = module.getItems( inv, filter )

     for key, qty in pairs( qtyList ) do
        if not ( invItems[key] and invItems[key].qty >= qty ) then
            return false
        end

        if callback then
            callback( invItems[key], qty)
        end
    end

    return true
end

--- Get a table list of items in the inventory, can be filtered.
-- @name Inventory.getItems
-- @param inv [Peripheral] Peripheral table with inventory functions.
-- @param filter [function] Callback function for filtering. Gets (fingerprint, item) and should return true or false.
-- @return items [table] A list of items.
-- @usage filter should be a table list with tables that contain a fingerprint and qty for quantity.
function module.getItems ( inv, filter )
    local items = {}

    local tItems = inv.getAllStacks()

    for slot = 1, inv.getInventorySize() do
        local item = tItems[slot] and tItems[slot].all() or nil
        if item then
            item.fingerprint = {
                id = item.id,
                dmg = item.dmg,
                nbt_hash = item.nbt_hash
            }
            item.nbt_hash = nil
            item.dmg = nil
            item.id = nil

            local key = module.fingerprintToString( item.fingerprint )
            if ( filter and filter( item.fingerprint, item ) ) or not filter then
                if items[key] then
                    local tmpItem = items[key]
                    tmpItem.qty = tmpItem.qty + item.qty
                    tmpItem.slots[#tmpItem.slots+1] = slot
                else
                    item.slots = {slot}
                    items[key] = item
                end
            end
        end
    end

    return items
end

--- Interface for Inventory.getItems() to use an item list as filter
-- @see Inventory.getItems
-- @name Inventory.filterItems
-- @param inv [Peripheral] Peripheral table with inventory functions.
-- @param items [table] List of items to filter.
-- @return filtered items [table]
function module.filterItems ( inv, items )
    local itemFilterList = {}

    for _, item in pairs( items ) do
        if not item or not item.fingerprint then error "Incorrect item found." end

        local key = module.fingerprintToString( item.fingerprint )
        itemFilterList[key] = item
    end

    local filter = function ( fingerprint, item )
      local key = module.fingerprintToString( fingerprint )
      return itemFilterList[key]
    end

    return module.getItems( inv, filter )
end

--- Pulls items out of an inventory into another
-- @name Inventory.pullItems
-- @param inv [Peripheral] Peripheral table with inventory functions.
-- @param side [string] The side on which the other inventory is.
-- @param items [table] List of items that should be pulled.
-- @param filteredItems [table] List of items in the OTHER inventory.
-- @usage It uses the filteredItems to see on which slots the items to pull are.
-- Use pullSlot on the item in items to specify in which slot it should be pulled.
function module.pullItems ( inv, side, items, filteredItems )
    local itemReturnList = {}
    local itemFilterList = {}

    for _, item in pairs( items ) do
        local key = module.fingerprintToString( item.fingerprint )
        itemFilterList[key] = item
    end

    for key, item in pairs( filteredItems ) do
        local tItem = itemFilterList[key]

        local qty = tItem.qty
        local pullSlot = tItem.pullSlot
        for _, slot in pairs( item.slots ) do
            qty = qty - inv.pullItem( side, slot, qty, pullSlot )
            if qty <= 0 then break end
        end
        if qty > 0 then itemReturnList[key] = qty end
    end

    return itemReturnList
end

--- Pushes items into another inventory.
-- @name Inventory.pushItems
-- @param inv [Peripheral] Peripheral table with inventory functions.
-- @param side [string] The side on which the other inventory is.
-- @param items [table] List of items that should be pushed.
-- @return items [table] List of items that could not be moved.
-- @usage Use pushSlot to specify into which slot the item should be pushed.
function module.pushItems ( inv, side, items )
    local itemReturnList = {}
    local itemFilterList = {}

    local itemList = module.filterItems( inv, items )

    for _, item in pairs( items ) do
        local key = module.fingerprintToString( item.fingerprint )
        itemFilterList[key] = item
    end

    for key, item in pairs( itemList ) do
        local tItem = itemFilterList[key]

        local qty = tItem.qty
        local pushSlot = tItem.pushSlot
        for _, slot in pairs( item.slots ) do
            qty = qty - inv.pushItem( side, slot, qty, pushSlot )
            if qty <= 0 then break end
        end
        if qty > 0 then
            itemReturnList[key] = qty
        end
    end

    return itemReturnList
end

function module.pushItemsRepeat ( inv, side, items, rate )
    local itemFilterList = {}
    rate = rate or 0.2

    for _, item in pairs( items ) do
        local key = module.fingerprintToString( item.fingerprint )
        itemFilterList[key] = item
    end

    repeat
        local remaining = module.pushItems( inv, side, items )
        local _items = {}

        if not next(remaining) then break end

        for key, quantity in pairs(remaining) do
            local item = itemFilterList[key]

            _items[#_items+1] = {
                fingerprint = item.fingerprint,
                pushSlot = item.pushSlot,
                qty = quantity,
            }
        end

        items = _items
        sleep(rate)
    until false
end

module.ae = {}

--- Get a table list of items in the inventory, can be filtered.
-- @name Inventory.getItems
-- @param inv [Peripheral] Peripheral table with inventory functions.
-- @param filter [function] Callback function for filtering. Gets (fingerprint, item) and should return true or false.
-- @return items [table] A list of items.
-- @usage filter should be a table list with tables that contain a fingerprint and qty for quantity.
function module.ae.getItems( inv, filter )
    local items = {}

    local tItems = inv.getAvailableItems()

    for _, v in pairs( tItems ) do
        local key = module.fingerprintToString( v.fingerprint )

        if ( filter and filter( v.fingerprint, v ) ) or not filter then
            items[key] = v
        end
    end
    return items
end

function module.ae.pushItems ( inv, side, items )
    local itemReturnList = {}

    for _, item in pairs( items ) do
        local qty = item.qty
        local pushSlot = item.pushSlot
        local res = inv.exportItem( item.fingerprint, side, qty, pushSlot )

        qty = qty - res.size
        if qty > 0 then
            itemReturnList[module.fingerprintToString(item.fingerprint)] = qty
        end
    end

    return itemReturnList
end

return module