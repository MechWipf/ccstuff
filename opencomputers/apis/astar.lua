

--[[  ###                 Load Includes and Math functions                       ###  ]]--
if not vector then
	if not os.loadAPI( "apis/vector" ) then
		error( "Failed while loading apis/vector. File not found." )
	end
end

local floor, abs, sqrt = math.floor, math.abs, math.sqrt
local bxor, vec = bit.bxor, vector.new

local function inRange(n,min,max) return n >= min and n < max end



--[[  ###                           Map Handler                                  ###  ]]--
local map = {}
map.__index = map

function newMap( x, y )
	if type(x) == "table" then
		y = x.y
		x = x.x
	end

	local m = { x = x, y = y, data = {} }
	setmetatable( m, map )
	return m
end

function map:getNodeID( x, y )
	if type(x) == "table" then
		y = x.y
		x = x.x
	end

	if not inRange( x, 0, self.x ) then return nil end
	if not inRange( y, 0, self.y ) then return nil end

	return x + y * self.x
end

function map:getNodePos( id )
	if id < 0 then error( "Index out of range." ) end
	
	return id % self.x, floor( id / self.y )
end

function map:setNode( x, y, bool )
	if type(x) == "table" then
		y = x.y
		x = x.x
	end

	if not inRange( x, 0, self.x ) then error( "Index X out of range." ) end
	if not inRange( y, 0, self.y ) then error( "Index Y out of range." ) end
	
	self.data[ x + y * self.x ] = bool
end

function map:getNode( x, y )
	if type(x) == "table" then
		y = x.y
		x = x.x
	end
	
	if not inRange( x, 0, self.x ) then return false end
	if not inRange( y, 0, self.y ) then return false end

	return self.data[ x + y * self.x ]
end



--[[  ###                      A* Helper functions                               ###  ]]--

local function writePoint( X, Y, Clr, Char )
	term.setCursorPos( X, Y )
	term.setBackgroundColor( Clr )
	term.write(Char or " ")
	term.setBackgroundColor( colours.black )
end

local function dist( a, b )
	local dx = a.x - b.x
	local dy = a.y - b.y
	return sqrt( dx*dx + dy*dy )
end

--[[  ###                           A* Main                                      ###  ]]--
function calc( map, vS, vE )
	local open   = { { vS, 0, dist( vS, vE ) } }
	local closed = {}
	local Perf = 0
	local Steps = 0
		
	while #open > 0 do
		cPos = open[1]
		closed[map:getNodeID( cPos[1].x, cPos[1].y )] = true
		table.remove( open, 1 )

		if Perf > 100 then
			sleep(0)
			Perf = 0
		else
			Perf = Perf + 1
		end
		
		for _x = -1, 1 do
			for _y = -1, 1 do
				if bxor( abs(_x), abs(_y) ) == 1 then
					local pos = cPos[1] + vec( _x, _y )
					
					if map:getNode( pos ) == nil then
						
						if not closed[map:getNodeID( pos )] then
							Steps = Steps + 1
							local dis = dist( pos, vE )
							local node = { pos, cPos[2] + 1, dis, cPos }
							local sorted = false
							
							for i, oNode in ipairs(open) do
								if ( oNode[3] or 0 ) > dis then
									table.insert( open, i, node )
									sorted = true
									break
								end
							end
							
							if not sorted then open[#open+1] = node end
							closed[ map:getNodeID( pos ) ] = true
							if closed[ map:getNodeID( vE ) ] then
								local point = node
								local path = { point[1] }
								
								while point[4] do
									point = point[4]
									path[#path + 1] = point[1]
								end
								
								return path, Steps
							end
						end
					end
				end
			end
		end
	end
	
	return false
end
