--- Console module for interactive console

local utils = require "utils"

local Console = {}
local _mt = {}
Console._mt = _mt

function Console.NewConsole ( default_fn )
  local obj = {
    commands = {},
    before = {},
    after = {},
    default = default_fn,
  }

  setmetatable( obj, { __index = _mt } )
  return obj
end

function _mt:registerCommand ( prefix, fn )
  prefix = type(prefix) == "string" and { prefix } or prefix

  for _, _prefix in pairs( prefix ) do
    self.commands[_prefix] = fn
  end
end

function _mt:registerBefore ( fn )
  self.before[#self.before+1] = fn
end

function _mt:registerAfter ( fn )
  self.after[#self.after+1] = fn
end

function _mt:exit ()
  self._exit = true
end

function _mt:run ( inTbl )

  repeat
    local input = (inTbl and #inTbl > 0) and table.remove( inTbl, 1 ) or io.read "*l"
    local args = utils.explode( input, " " )
    local cmd = table.remove( args, 1 )

    local fn = self.commands[cmd]
    for _, f in pairs( self.before ) do f( self, unpack( args ) ) end
    if fn then
      fn( self, unpack( args ) )
    else
      self:default( unpack( args ) )
    end
    for _, f in pairs( self.after ) do f( self, unpack( args ) ) end
  until self._exit

end

return Console