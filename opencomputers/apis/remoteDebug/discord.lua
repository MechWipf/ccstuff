local _R = getfenv()
local http = _R.http

local json = require "vendor.json"

local module = {
  init = function (remoteDebug)
    remoteDebug.serviceGetDevice = function ( self )
      local handler = http.get( self.serviceGetDeviceUrl )
      if not handler then return {}, "Could not open handler." end
      if handler:getResponseCode() ~= 200 then return {}, "HTTP error." end

      local content = handler:readAll() handler:close()
      if not handler then return false, "Error accessing webservice." end
      local ok, ret = pcall( json.decode, content )
      if not ok then return {}, "Maleformed json data." end

      return { uuid = "none", phon = ret.name }
    end

    remoteDebug.serviceSendDebug = function ( self, ... )
      if not self.config then return false, "Load config first." end
      if not select( "#", ... ) then return false, "Cannot send nil data." end

      local data = {
        username = self.config.devicePhon,
        content = self:prepareData( ... )
      }

      local headers = {
        ["Content-Type"] = "application/json"
      }

      http.request( self.serviceSendDebugUrl, json.encode( data ), headers )
    end

    remoteDebug.serviceGetDeviceUrl = "https://discordapp.com/api/webhooks/278179654450282496/WAhuZu2UJplEf5YBgoTgt4-uogWvVHAnO577xzSWO45aQCpm01pbO2sZ1JQ7dQ89i6ir"
    remoteDebug.serviceSendDebugUrl = "https://discordapp.com/api/webhooks/278179654450282496/WAhuZu2UJplEf5YBgoTgt4-uogWvVHAnO577xzSWO45aQCpm01pbO2sZ1JQ7dQ89i6ir"
    remoteDebug:saveConfig()

    remoteDebug:registerDebugger()
  end
}

return module