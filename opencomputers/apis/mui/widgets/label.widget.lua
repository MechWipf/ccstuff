
widget( "label", widgets.base ) {
    _construct = function ( self, data )
        self.text = data.text or ""
        self.w = self.text:len()
        self.h = 1
    end,

    _draw = function ( self, g )
        local x, y = self:getPos()
        local w, h = self.w, self.h

        g.setTextColor( self:getColor( "text" ) )
        g.setBackgroundColor( self:getColor( "background" ) )
        g.set( x, y, self.text )

        if self.text:len() < self.w then
            g.fill( x + self.text:len(), y, w - self.text:len(), h, " " )
        end

        if self._w then
            g.fill( x + self.text:len(), y, w - self.text:len(), h, " " )
            g.write( (" "):rep( self.w - self.text:len() ) )
            self.w = self._w
            self._w = nil
        end
    end,

    setText = function ( self, text, w )
        if type(text) ~= "string" then
            if log then log{ "setText:label.widget.lua", "Type missmatch. [text] has to be a string." } end
            error "Type missmatch. [text] has to be a string."
        end
        self._w = w or text:len()

        if self.w < self._w then
            self.w = self._w
        end

        self.text = text
        self:invalidate()
    end,
}
