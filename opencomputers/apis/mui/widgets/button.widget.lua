
widget( "button", widgets.base ) {

    _construct = function ( self, data )
        self.text = data.text or ""
        self.w = self.text:len()
    end,

    _onClick = function ( self, btn, x, y )
        if btn == 0 then
            if self.onClickLeft then self:onClickLeft() end
        elseif btn == 1 then
            if self.onClickRight then self:onClickRight() end
        end

        self.focus = false
    end,

    _onKey = function ( self, key )
        if self.focus then
            if key == keys.enter then
                if self._onClick then self:_onClick( 1, 0, 0 ) end
            elseif key == keys.tab then
                if self.next then
                    self:setFocus( self.next )
                end
            end

            if self.onKey then self:onKey( key ) end
        end
    end,

    _update = function ( self )
        if self.hover then
            self:invalidate()
        end

        if self.focus and math.floor( ( os.clock() * 2 ) % 2 ) ~= self.clock then
            self.clock = math.floor( ( os.clock() * 2 ) % 2 )
            self.f = not self.f

            self:invalidate()
        end
    end,

    _draw = function ( self, g )
        local x, y = self:getPos()
        local w, h = self.w, self.h

        g.setBackgroundColor( self:getColor( "button" ) )
        g.setTextColor( self:getColor( "text" ) )

        g.fill( x, y, w, h, " " )

        local text = ( self.f and self.focus ) and "_" .. self.text:sub( 2 ) or self.text
        g.set( x + math.floor( w/2 - self.text:len()/2 ), y, text )
    end,
}
