local private = {}

if not sha256 then error "sha256 is missing!" end
local keys = require "keyboard".keys

widget( "textbox", widgets.base ) {

    _construct = function ( self, data )
        self.h = 1

        private[self] = {
            text = "",
            protected = false,
        }

        private[self].text = data.text or ""
        private[self].protected = false

        self.cursorX = 0
    end,

    _onKey = function ( self, char, code, down )
        if self.focus and down then
            if code == keys.back then
                private[self].text = private[self].text:sub( 1, self.cursorX - 1 ) .. private[self].text:sub( self.cursorX + 1 )
                self.cursorX = math.max( self.cursorX - 1, 0 )

                if self.onChanged then self:onChanged() end
                self:invalidate()
            elseif code == keys.left then
                self.cursorX = math.max( self.cursorX - 1, 0 )

                self:invalidate()
            elseif code == keys.right then
                self.cursorX = math.min( self.cursorX + 1, private[self].text:len() )

                self:invalidate()
            elseif code == keys.enter then
                    if self.onEnter then self:onEnter() end
                    if self.next then
                        self:setFocus( self.next )
                    end
                if self.onChanged then self:onChanged( true ) end
            elseif code == keys.tab then
                if self.next then
                    self:setFocus( self.next )
                end
            elseif char >= 0x20 and char <= 0x7E
            and ( not self.regex or string.char(char):match( self.regex ) )
            and ( not self.maxLen or private[self].text:len() < self.maxLen )
            then
                private[self].text = private[self].text:sub( 1, self.cursorX ) .. string.char(char) .. private[self].text:sub( self.cursorX + 1 )
                self.cursorX = self.cursorX + 1

                if self.onChanged then self:onChanged() end
                self:invalidate()
            end

            if self.onKey then self:onKey( private[self].protected and nil or char ) end
        end
    end,

    _update = function ( self )
        if self._focus ~= self.focus then
            self:setCursor( self.focus )
            self:invalidate()
        end
        self._focus = self.focus

        if math.floor( ( os.clock() * 2 ) % 2 ) ~= self.clock then
            self.clock = math.floor( ( os.clock() * 2 ) % 2 )
            self.f = not self.f

            self:invalidate()
        end
    end,

    _draw = function ( self, g )
        local x, y   = self:getPos()
        local w, h   = self.w, self.h
        local text   = private[self].text
        local scroll = 0

        if self.cursorX >= w then
            scroll = ( self.cursorX - w ) + 1
        end

        if private[self].protected then text = text:gsub( ".", "*" ) end

        if self.cursorBlink then
            if self.f then
                text = text:sub( 1, self.cursorX ) .. "_" .. text:sub( self.cursorX + 2 )
            end
        end

        text = text:sub( scroll + 1, scroll + w )

        g.setBackgroundColor( self:getColor( "textBackground" ) )
        g.setTextColor( self:getColor( "text" ) )

        g.set( x, y, text )
        g.fill( x + text:len(), y, w - text:len(), 1, " " )
    end,

    setCursor = function ( self, bool )
        self.cursorBlink = bool
    end,

    checkText = function ( self, textb )
        local texta = sha256( private[self].text )
        return texta == textb
    end,

    setProtected = function ( self, bool )
        private[self].protected = bool == true
        private[self].text = ""
    end,

    setText = function ( self, text )
        if not private[self].protected then
            private[self].text = text
            return
        end

        if type(text) ~= "string" then
            if log then log{ "setText:textbox.widget.lua", "Type missmatch. [text] has to be a string." } end
            error "Type missmatch. [text] has to be a string."
        end

        error( "Tried to access protected data." )
    end,

    addText = function ( self, text )
        if log then log{ "addText:textbox.widget.lua", "Type missmatch. [text] has to be a string." } end
        if type(text) ~= "string" then
            error "Type missmatch. [text] has to be a string."
        end

        private[self].text = string.format( "%s%s", private[self].text or "", text or "" )
    end,

    getText = function ( self )
        if not private[self].protected then
            return private[self].text
        end

        return sha256( private[self].text )
    end,

    clear = function ( self )
        private[self].text = ""
    end,
}
