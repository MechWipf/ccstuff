widget( "panel", widgets.base ) {

    _draw = function ( self, g )
        local x, y = self:getPos()
        local w, h = self.w, self.h

        g.setBackgroundColor( self:getColor( "background" ) )
        g.fill( x, y, w, h, " " )
    end,
}