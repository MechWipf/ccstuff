widget "base" {

    __construct = function ( self, data )
        self.x = data.x or 0
        self.y = data.y or 0
        self.w = data.w or 1
        self.h = data.h or 1

        self.enabled = true
        self.hidden = false
        self.__redraw = true

        self.child = setmetatable( {}, { __mode = "v" } )
        self.colors = {}

        if self._construct then
            self:_construct( data )
        end
    end,

    __onTouch = function ( self, x, y )
        if self.child then
            for _, child in pairs( self.child ) do
                if child.__onTouch then child:__onTouch( x, y ) end
            end
        end

        if self._onTouch then self:_onTouch( x, y ) end
    end,

    __onDrag = function ( self, btn, x1, y1, x2, y2 )
        if self.child then
            for _, child in pairs( self.child ) do
                if child.__onDrag then child:__onDrag( btn, x1, y1, x2, y2 ) end
            end
        end

        if self._onDrag then self:_onDrag( btn, x1, y1, x2, y2 ) end
    end,

    __onClick = function ( self, btn, x, y )
        self.focus = true

        if self.child then
            local canFocus = true

            for _, child in ipairs( self.child ) do
                local childX, childY = child:getPos()
                local childW, childH = child.w, child.h

                if child.__onClick and canFocus
                    and math.inRange( x, childX, childX + childW - 1 )
                    and math.inRange( y, childY, childY + childH - 1 )
                then
                    child:__onClick( btn, x, y )

                    canFocus = false
                else
                    child.focus = false
                end
            end
        end

        if self._onClick then self:_onClick( btn, x, y ) end
    end,

    __onScroll = function ( self, dir, x, y )
        if self.child then
            for _, child in ipairs( self.child ) do
                local childX, childY = child:getPos()
                local childW, childH = child.w, child.h

                if child.__onScroll
                    and math.inRange( x, childX, childX + childW - 1 )
                    and math.inRange( y, childY, childY + childH - 1 )
                then
                    child:__onScroll( dir, x, y )
                end
            end
        end

        if self._onScroll then self:_onScroll( dir, x, y ) end
    end,

    __onKey = function ( self, key, code, down )
        if self.child then
            for _, child in ipairs( self.child ) do
                if child.__onKey then child:__onKey( key, code, down ) end
            end
        end

        if self._onKey then self:_onKey( key, code, down ) end
    end,

    __close = function ( self, cancle )
        if self._close then
            cancle = self:_close( cancle )
        end

        if self.close then
            cancle = self:close( cancle )
        end

        if not cancle then
            killWidget( self )
        end
    end,

    __update = function ( self, dt )
        if self.child then
            for _, child in ipairs( self.child ) do
                child:__update( dt )
            end
        end

        if self._update then self:_update( dt ) end
        if self.update then self:update( dt ) end
    end,

    __draw = function ( self, g )
        if self._draw and self.__redraw then
            local x, y = g.getCursor()

            self.__redraw = false
            self:_draw( g )

            if self.draw then
                self:draw( g )
            end

            g.setCursor( x, y )
        end

        if self.child then
            if g.pushArea then
                local x, y = self:getPos()
                local w, h = self.w, self.h

                g.pushArea( x, y, w, h )
            end

            for _, child in ipairs( self.child ) do
                if child.__draw and not child._disabled then
                    child:__draw( g )
                end
            end

            if g.popArea then
                g.popArea()
            end
        end
    end,

    setPos = function ( self, x, y )
        self.x = x or self.x
        self.y = y or self.y

        if self.parent then
            self.parent:invalidate()
        else
            self:invalidate()
        end
    end,

    refresh = function ( self )
        if self.calcW then self.w = self:calcW() end
        if self.calcH then self.h = self:calcH() end
        if self.calcX then self.x = self:calcX() end
        if self.calcY then self.y = self:calcY() end

        if self.child then
            for _, child in ipairs( self.child ) do
                child:refresh()
            end
        end

        self:invalidate()
    end,

    setSize = function ( self, w, h )
        self.w = w or self.w
        self.h = h or self.h

        if self.parent then
            self.parent:invalidate()
        else
            self:invalidate()
        end
    end,

    setParent = function ( self, parent )
        if self.parent then
            self:delParent()
        end
        self.parent = parent
        parent.child[ #parent.child + 1 ] = self
    end,

    delParent = function ( self )
        if not self.parent then error( "Widget has no parent!", 2 ) end

        for k, child in ipairs( self.parent.child ) do
            if child == self then
                table.remove( self.parent.child, k )
                self.parent = nil
                break
            end
        end
    end,

    addChild = function ( self, child )
        child:setParent( self )
    end,

    clearChilds = function ( self )
        if self.child then
            while self.child[1] do
                self.child[1]:delParent()
            end
        end
    end,

    getPos = function ( self )
        local x, y = 0, 0

        if self.parent then
            x, y = self.parent:getPos()
        end

        return x + self.x, y + self.y
    end,

    setColors = function ( self, colorPack )
        self.colors = colorPack
        self:invalidate()
    end,

    getColor = function ( self, name )
        if self.colors and self.colors[name] then
            return self.colors[name]
        else
            return defaultColors[name]
        end
    end,

    setColor = function ( self, name, color )
        if not self.colors then self.colors = {} end
        self.colors[name] = color
        self:invalidate()
    end,

    invalidate = function ( self )
        if self.onInvalidate then
            self:onInvalidate()
        end

        if self.child then
            for _, child in ipairs( self.child ) do
                child:invalidate()
            end
        end

        self.__redraw = true
    end,

    setFocus = function ( self, target )
        setFocus( self, target )
    end,

    kill = function ( self )
        self:__close()
    end,

    enable = function ( self, b )
        if b == false ~= self._disabled then
            self:invalidate()
        end

        self._disabled = b == false
    end
}