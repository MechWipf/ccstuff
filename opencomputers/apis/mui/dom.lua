local module = {}

module._watches = {}
local _watches = module._watches

local function _build ( widget_name, opts, env )
  local children = opts[1] or setmetatable( {}, { __mode = "v" } )
  opts.children = nil

  local color = opts.color or setmetatable( {}, { __mode = "v" } )

  local name = opts.name

  local widget = module.widgets[widget_name]( opts )

  if name then
    env[name] = widget
  end

  for _, child in pairs( children ) do
    widget:addChild( child )
  end

  for k, v in pairs( opts ) do
    if type(v) == "string" and v:sub( 1, 1 ) == "@" then
      local fname = v:sub(2)
      if not env[fname] then error( "Missing link: " .. fname ) end

      if type(widget[k]) == "function" then
        widget[k]( widget, env[fname] )
      else
        widget[k] = env[fname]
      end

      _watches[#_watches+1] = { widget, env, k, fname, env[fname] }
    elseif type(v) == "string" and v:sub( 1, 1 ) == "#" then
      local fname = v:sub(2)
      if not env[fname] then error( "Missing link: " .. fname ) end

      widget[k] = env[fname]( widget )
    elseif type(v) == "string" and v:sub( 1, 1 ) == "=" then
      local fn = load( ("return function ( self ) return %s end"):format( v:sub(2) ) )
      widget[k] = fn()
    else
      widget[k] = v
    end
  end

  if color then
    widget:setColors( color )
  end

  if widget.init then
    widget:init()
    widget.init = nil
  end

  return widget
end

function module.getEnv ( env )
  local function __index ( _, tag_name )
    if _G[tag_name] then return _G[tag_name] end
    if env[tag_name] then return env[tag_name] end

    return function ( opts )
      return _build( tag_name, opts, env or {} )
    end
  end

  return setmetatable( {}, { __index = __index } )
end

function module.loadString ( str, env )
  local fn, err

  local lEnv = module.getEnv( env )
  fn, err = load( str, nil, nil, lEnv )
  if err then return nil, err end

  return fn()
end

function module.loadFile ( path, env )
  local h = io.open( path, "r" )
  if not h then return nil, "File not found." end
  local content = h:read "*a"
  h:close()

  return module.loadString( content, env )
end

function module.loadFileCached ( path )
  local h = io.open( path, "r" )
  if not h then return nil, "File not found: " .. path end
  local content = h:read "*a"
  h:close()

  return function ( env )
    return module.loadString( content, env )
  end
end

function module.jobWatch ()
  local i, c = 1, #_watches

  while i <= c do
    local watch = _watches[i]
    local widget, env, widgetKey, envKey, lastVal = table.unpack( watch )

    if widget.isDead then
      table.remove( _watches, i )
      c = #_watches
    else
      if env[envKey] ~= lastVal then
        if type(widget[widgetKey]) == "function" then
          widget[widgetKey]( widget, env[envKey] )
        else
          widget[widgetKey] = env[envKey]
        end

        widget:invalidate()
        watch[5] = env[envKey]
      end
      i = i + 1
    end
  end
end

function module.clearWatches ()
  _watches = {}
  module._watches = _watches
end

return module