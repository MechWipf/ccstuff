local internet = require "component".internet
local request = require "internet".request
local von = require "vendor.von"

local vfs = {}

local function gsplit ( s, sep )
	local lasti, done, g = 1, false, s:gmatch('(.-)'..sep..'()')
	return function()
		if done then return end
		local v,i = g()
		if s == '' or sep == '' then done = true return s end
		if v == nil then done = true return s:sub(lasti) end
		lasti = i
		return v
	end
end

local function getSegment ( self, path )
  local parent = { [self.index] = nil }
  local t = self.index

  for part in gsplit( path:gsub("^/", ""), "/" ) do
    if part == ".." then
      t = parent[t]
      if not t then return nil end
    elseif part ~= "." and part ~= "" then
      local newT = t[part]
      if not newT then return nil end
      parent[newT], t = t, newT
    end
  end

  return t
end

local function getRemotePath ( self, path )
  local seg = getSegment( self, path )

  if type(seg) == "string" then
    return self.base .. seg
  end

  return nil
end

function vfs.getProxy ( base, url )
  local meta = {
    handles = {},
    hCount = 0,
    base = base,
  }

  function meta:getIndex ()
    local sb = {}

    for chunk in request( url ) do
      sb[#sb+1] = chunk
    end

    self.index = von.deserialize( table.concat( sb ) )
  end

  function meta.spaceUsed ()
    return 0
  end

  function meta.makeDirectory ( path )
    return false
  end

  function meta.exists ( path )
    if path == "" then return true end

    local seg = getSegment( meta, path )
    if not seg then return false end

    return true
  end

  function meta.isReadOnly ()
    return true
  end

  function meta.spaceTotal ()
    return 0
  end

  function meta.isDirectory ( path )
    if path == "" then return true end

    local seg = getSegment( meta, path )
    if type(seg) == "table" then return true end

    return false
  end

  function meta.rename ( from, to )
    return false
  end

  function meta.list ( path )
    local seg = getSegment( meta, path )
    if not seg or type(seg) ~= "table" then return {} end

    local list = {}
    for k, _ in pairs( seg ) do
      list[#list+1] = k
    end

    return list
  end

  function meta.lastModified ( path )
  end

  function meta.getLabel ()
    return "vfs"
  end

  function meta.size ( path )
    return 0
  end

  function meta.setLabel ( value )
    return "vfs"
  end

  function meta.remove ( path )
    return false
  end

  function meta.open ( path, mode )
    if mode ~= "r" then
      return nil, "only read (r) is supported"
    end

    if ( meta.exists( path ) and not meta.isDirectory( path ) ) then
      if meta.hCount > 4 then
        local handle = next( meta.handles )
        meta.close( handle )
      end

      local handles = meta.handles

      local h = internet.request( getRemotePath( meta, path ) )
      handles[#handles+1] = h
      meta.hCount = meta.hCount + 1

      return #handles
    end

    return nil, "no file found"
  end

  function meta.close ( handle )
    meta.hCount = meta.hCount - 1
    meta.handles[handle].close()
    meta.handles[handle] = nil
  end

  function meta.read ( handle, count )
    if not meta.handles[handle] then return nil, "file closed" end
    return meta.handles[handle].read( count )
  end

  function meta.write ( handle, value )
    return nil, "file is readonly"
  end

  function meta.seek ( handle, whence, offset )
    return nil, "file does not support seek"
  end

  return meta
end

return vfs
