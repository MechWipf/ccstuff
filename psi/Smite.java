{
  spellName: "Smite",
  uuidMost: -5356331582719639336L,
  validSpell: 1b,
  spellList: [
    0: {
      data: {
        key: "errorSuppressor"
      },
      x: 0,
      y: 0
    },
    1: {
      data: {
        key: "selectorFocalPoint"
      },
      x: 0,
      y: 2
    },
    2: {
      data: {
        params: {
          _target: 2
        },
        key: "connector"
      },
      x: 1,
      y: 1
    },
    3: {
      data: {
        params: {
          _target: 3
        },
        key: "operatorEntityPosition"
      },
      x: 1,
      y: 2
    },
    4: {
      data: {
        key: "constantNumber",
        constantValue: "32"
      },
      x: 2,
      y: 0
    },
    5: {
      data: {
        params: {
          _position: 3,
          _radius: 1
        },
        key: "selectorNearbyLiving"
      },
      x: 2,
      y: 1
    },
    6: {
      data: {
        params: {
          _target: 1
        },
        key: "operatorRandomEntity"
      },
      x: 2,
      y: 2
    },
    7: {
      data: {
        params: {
          _position: 2
        },
        key: "trickSmite"
      },
      x: 3,
      y: 1
    },
    8: {
      data: {
        params: {
          _target: 3
        },
        key: "operatorEntityPosition"
      },
      x: 3,
      y: 2
    }
  ],
  uuidLeast: -8441325482134189569L
}