package.path = package.path .. ";C:/Users/mechwipf/src/lua/?.lua"

local fs = require "lfs"
local von = require "libs.von"
local utils = require "libs.utils"

local function set ( t, path, value )
  local parts = utils.explode( path, "/" )
  local key = table.remove( parts, #parts )

  for _, part in pairs( parts ) do
    if not t[part] or type(t[part]) ~= "table" then
      t[part] = {}
    end

    t = t[part]
  end

  t[key] = value
end

local function make ( basePath )
  local index = {}
  local tree = {}
  local lookup = { "." }

  while #lookup > 0 do
    local path = table.remove( lookup, 1 )

    local attr = fs.attributes( basePath .. path )
    local isDir = attr and attr.mode == "directory"

    if isDir then
      if path ~= "." then
        index[path] = "d"
        set( tree, path, {} )
      end

      for subPath in fs.dir( basePath .. path ) do
        if subPath ~= ".." and subPath ~= "." then
          lookup[#lookup+1] = ( path ~= "." and path .. "/" or "" ) .. subPath
        end
      end
    else
      index[path] = "f"
      set( tree, path, path )
    end
  end

  return index, tree
end

local basePath = select(1,...)
local paths = {
  {basePath                , basePath .. "base.von"     },
  {basePath .. "apis/"     , basePath .. "apis.von"     },
  {basePath .. "installer/", basePath .. "installer.von"},
  {basePath .. "programs/" , basePath .. "programs.von" },
  {basePath .. "startup/"  , basePath .. "startup.von"  },
}

local function writeFile ( path, content )
  local f = io.open( path, "w" )
  f:write( content )
  f:close()
end

for _, path in pairs( paths ) do
  local _, tree = make( path[1] )
  writeFile( path[2], von.serialize( tree ) )
end
